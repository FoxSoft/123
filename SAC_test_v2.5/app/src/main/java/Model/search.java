package Model;

/**
 * Created by eminem on 03.01.2018.
 */

public class search {

    private String total;
    //search
    //to
    private String codeTo;
    private String typeTo;
    private String popularTitleTo;
    private String titleTo;
    //from
    private String codeFrom;
    private String typeFrom;
    private String popularTitleFrom;
    private String titleFrom;

    //thread
    private String except_days;
    private String arrival;
    private String departure;
    private String duration;
    private String arrivalTerminal;
    private String arrivalPlatform;
    private String departurePlatform;
    private String days;
    private String startDate;
    private String transportType;
    private String uid;
    private String titleThread;
    private String vehicle;
    private String number;
    private String express_type;
    private String thread_method_link;
    private String transportSubtype;
    private String stops;

    //FromStation
    private String codeFromStation;
    private String stationTypeFromStation;
    private String titleFromStation;
    private String popularTitleFromStation;
    private String transportTypeFromStation;
    private String stationTypeNameFromStation;
    private String typeFromStation;

    //ToStation
    private String codeToStation;
    private String stationTypeToStation;
    private String titleToStation;
    private String popularTitleToStation;
    private String transportTypeToStation;
    private String stationTypeNameToStation;
    private String typeToStation;

    //carrier
    private String title;
    private String url;
    private String logo;
    private String contacts;
    private String phone;
    private String address;
    private String email;

    //transport_subtype
    private String colorTransport;
    private String codeTransport;
    private String titleTransport;
}
