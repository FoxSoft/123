package Model;

import com.example.eminem.sac.Yandex;

/**
 * Created by eminem on 25.12.2017.
 */

public class TypeTransport {
    private String title;
    private int img;
    private int id;
    private Yandex.TRANSPORT_TYPE transportType;

    public TypeTransport() {
    }

    public TypeTransport(String title, int img, Yandex.TRANSPORT_TYPE transportType, int id) {
        this.title = title;
        this.img = img;
        this.transportType = transportType;
        this.id = id;
    }

    public Yandex.TRANSPORT_TYPE getTransportType() {
        return transportType;
    }

    public void setTransportType(Yandex.TRANSPORT_TYPE transportType) {
        this.transportType = transportType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
