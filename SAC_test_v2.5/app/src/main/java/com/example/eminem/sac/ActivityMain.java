package com.example.eminem.sac;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

import com.bigkoo.svprogresshud.SVProgressHUD;
import com.bigkoo.svprogresshud.listener.OnDismissListener;
import com.polyak.iconswitch.IconSwitch;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilderFactory;

import Adapter.AdapterListSearch;
import Adapter.AdapterTypeTransport;
import Model.NearestStation;
import Model.Settlement;
import Model.StationDB;
import Model.TypeTransport;

public class ActivityMain extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public AdapterListView adapterBus;
    public List<Bus> busList;

    public DBAdapter dbAdapter;
    private AlertDialog.Builder dialog;
    private AutoCompleteTextView editTextFrom;
    private AutoCompleteTextView editTextTo;
    public Boolean internetConnect = false;
    public RecyclerView recycler;
    public String searchReys = null;
    private String from;
    private String to;
    public String searchReysReverse = null;
    private SVProgressHUD mSVProgressHUD;
    private IconSwitch iconSwitch;
    private AdapterHuntStationAutoComplete stationAutoComplete;
    private final String TAG = "ActivityMain";
    private StationDB stationDBFrom;
    private StationDB stationDBTo;
    private Download download;
    //--------------------------------
    private Yandex.TRANSPORT_TYPE transportType = null;
    private boolean isTransfers = false;

    private AdapterListSearch adapterListSearch;
    public Yandex.SearchAPI searchAPI;
    private Download d;
    private boolean isCancel = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        isOnline();

        dbAdapter = new DBAdapter(this);
        editTextFrom = (AutoCompleteTextView) findViewById(R.id.fromStationEdit);
        editTextTo = (AutoCompleteTextView) findViewById(R.id.toStationEdit);
        iconSwitch = (IconSwitch) findViewById(R.id.iconSwitch);
        setIconSwitch();

        creteDialog();

        stationDBFrom = new StationDB();
        stationDBTo = new StationDB();

        mSVProgressHUD = new SVProgressHUD(this);
        mSVProgressHUD.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(SVProgressHUD hud) {
                // todo something, like: finish current activity
                // Toast.makeText(getApplicationContext(), "dismiss", Toast.LENGTH_SHORT).show();
            }
        });

        ((FloatingActionButton) findViewById(R.id.fab)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                isOnline();

                boolean inputFrom = !editTextFrom.getText().toString().equals("") ? true : false;
                boolean inputTo = !editTextTo.getText().toString().equals("") ? true : false;

                if ((inputFrom & inputTo)) {
                    searchReys = editTextFrom.getText() + "_" + editTextTo.getText();
                    searchReysReverse = editTextTo.getText() + "_" + editTextFrom.getText();
                    if (dbAdapter.hashTable(searchReys)) {
                        dialog.show();
                    } else {
                        if (internetConnect) {
                            Log.d("SEARCHB", "=================");


                            String fromCode = stationDBFrom.getCodeStation();
                            String toCode = stationDBTo.getCodeStation();
                            Log.d("SEARCHB", fromCode);
                            Log.d("SEARCHB", toCode);

                            inputFrom = !fromCode.equals("") ? true : false;
                            inputTo = !toCode.equals("") ? true : false;
                            if ((inputFrom & inputTo)) {
                                dbAdapter.createTableV2(searchReys);
                                busList.clear();

                                download = new Download(getApplicationContext(), fromCode, toCode, transportType, false, Yandex.SEARCH_RASP.from_to);
                                download.execute();

                                RefrashTime();
                                dbAdapter.createTable(searchReysReverse);
                                Download dBack = new Download(getApplicationContext(), toCode, fromCode, transportType, false, Yandex.SEARCH_RASP.to_from);
                                dBack.execute();

                                // Log.d("search__", download.searchAPI.getSegments().get(0).toString());

                                mSVProgressHUD.showSuccessWithStatus("Загружено!");
                                dbAdapter.resetHistiryRasp(searchReys, editTextFrom.getText().toString(), stationDBFrom.getCodeStation(),
                                        editTextTo.getText().toString(), stationDBTo.getCodeStation());
                                Log.d("search", fromCode + "  " + toCode);


                            } else {

                                mSVProgressHUD.showInfoWithStatus("Упс... Возможно, вы допустили ошибку. Если все верно, тогда сорри. Такого города нет в базе...");
                                //Snackbar.make(view, "Упс... Возможно, вы допустили ошибку. Если все верно, тогда сорри. Такого города нет в базе...", 0).setAction("Action", null).show();


                            }
                        } else {
                            mSVProgressHUD.showInfoWithStatus("нет интернета...");
                        }
                    }
                } else {
                    mSVProgressHUD.showErrorWithStatus("Заполните все поля ...");
                }


            }

        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        recycler = (RecyclerView) findViewById(R.id.recycler);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        recycler.setLayoutManager(layoutManager);
        recycler.setNestedScrollingEnabled(false);

        createEdits();

        busList = new ArrayList();

        // preview();

        searchAPI = new Yandex.SearchAPI();
        adapterListSearch = new AdapterListSearch(getApplicationContext(), searchAPI);
        recycler.setAdapter(adapterListSearch);
        //  loadHistory();


        dbAdapter.createTableV2("Зарайск_Луховицы");
        d = new Download(getApplicationContext(), dbAdapter.getCodeCity("Луховицы"),
                "c10734", transportType, false, Yandex.SEARCH_RASP.from_to);
        d.execute();
        RefrashTimeV2();
        Date date = new Date();
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("'последнее обновление:' hh:mm', 'yyyy.MM.dd ");

        // Log.d("search__", dbAdapter.getCodeCity("Луховицы") + " " + dbAdapter.getCodeCity("Зарайск"));
        Log.d("search__", "проверка таблицы: " + timeToInt("12:35:00")[0] + " " + timeToInt("12:35:00")[1]);


    }

    public void RefrashTimeV2() {
        Timer t = new Timer();
        final Status status = new Status();
        t.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                ActivityMain.this.runOnUiThread(new Runnable() {
                    public void run() {
                        for (int i = 0; i < searchAPI.getSegments().size(); i++) {
                            searchAPI.getSegments().get(i).setStatus(status.statusBusV2(searchAPI.getSegments().get(i).getDeparture()));
                            searchAPI.getSegments().get(i).setIdImg(status.idImg);
                            searchAPI.getSegments().get(i).setPosition(status.trackTransport(searchAPI.getSegments().get(i).getDeparture(), searchAPI.getSegments().get(i).getArrival()));
                            Log.d("PROVERKA_POS", String.valueOf(status.trackTransport(searchAPI.getSegments().get(i).getDeparture(), searchAPI.getSegments().get(i).getArrival())));
                            searchAPI.getSegments().get(i).setTrack_max(status.max);
                            Log.d("PROVERKA", String.valueOf(status.max));
                            searchAPI.getSegments().get(i).setTrack_visible(status.send);
                        }
                        adapterListSearch.notifyDataSetChanged();
                        recycler.setAdapter(adapterListSearch);
                    }
                });
            }
        }, 0, 5000);
    }

    private void test2() {

        Timer t = new Timer();
        Date date = new Date();
        Log.d("+=+=+", date.toString());
        date.setTime(date.getTime() + 1000);
        final boolean[] bFlagForceExit = {true};
        final Handler timerHandler = new Handler();
        final Runnable timerRunnable = new Runnable() {
            @Override
            public void run() {
                if (d.isSet && dbAdapter.isRecordToRaspTable("Зарайск_Луховицы")) {
                    Log.d("search__", "1");
                    Log.d("search__", "record table" + dbAdapter.recordRaspTableV2("Зарайск_Луховицы").getSearch().toString());
                    isCancel = true;
                } else {
                    Log.d("search__", "0");
                }

                if (isCancel)
                    timerHandler.postDelayed(this, 1000);

            }
        };

        timerRunnable.run();
    }

    private void test() {
        String reys = "Рязань_Коломна";
        if (dbAdapter.isRecordToRaspTable(reys)) {
            Log.d("search__", "record yes");
            searchAPI.getSegments().clear();
            Yandex.SearchAPI lb = new Yandex.SearchAPI();
            lb = this.dbAdapter.recordRaspTableV2(reys);
            lb.getSegments().get(0).setArrival("4456");
            Log.d("search__", lb.getSegments().get(0).getArrival());
            for (int i = 0; i < lb.getSegments().size(); i++) {
                searchAPI.getSegments().add(lb.getSegments().get(i));
            }
            // RefrashTime();

            this.adapterListSearch.notifyDataSetChanged();
            this.searchReys = reys;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.action_search_op) {

//            CustomDialogFragment customDialogFragment = new CustomDialogFragment();
//            customDialogFragment.show(getSupportFragmentManager(), "d");
////            MyDialog myDialog = new MyDialog(this);
////            myDialog.createOneButtonAlertDialog("lol", "ddddd");
////
////            Intent intent = getIntent();
////
//            Bundle extras = getIntent().getExtras();
////
//           textView.setText(extras.getString("type")+"   "+ extras.getString("isCheck"));

            createDialogOptionSearch();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void createEdits() {
        stationAutoComplete = new AdapterHuntStationAutoComplete(this);
        editTextFrom.setAdapter(stationAutoComplete);
        editTextFrom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                stationDBFrom = (StationDB) adapterView.getItemAtPosition(position);
                editTextFrom.setText(stationDBFrom.getNameStation());
                Toast.makeText(getApplicationContext(), stationDBFrom.getCodeStation(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, stationDBFrom.getCodeStation() + " " + stationDBFrom.getCodeCity() + " " + stationDBFrom.getCodeRegion());
            }
        });

        editTextTo.setAdapter(stationAutoComplete);
        editTextTo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                stationDBTo = (StationDB) adapterView.getItemAtPosition(position);
                editTextTo.setText(stationDBTo.getNameStation());
                Toast.makeText(getApplicationContext(), stationDBTo.getCodeStation(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, stationDBTo.getCodeStation() + " " + stationDBTo.getCodeCity() + " " + stationDBTo.getCodeRegion());
            }
        });
    }

    private void preview() {
        busList.add(new Bus(1, "Зарайск", "Коломна", "23:00", "8:10", "31", "4200",
                "Зарайск - Коломна Зарайск - Коломна", "ежедневно", "впути", R.drawable.buses_yellow));
        busList.add(new Bus(2, "Зарайск", "Коломна", "21:00", "8:10", "31", "4200",
                "Зарайск - Коломна", "ежедневно", "впути", R.drawable.buses_red));
        busList.add(new Bus(3, "Зарайск", "Коломна", "14:00", "8:10", "31", "4200",
                "Зарайск - Коломна", " Января. 12. 25 Декабря. 12. 45 .47. 7 Февраля. 12.14.15.16.17.18.19. Сентября. 5. 6. 7. 8. 9" +
                " Января. 12. 25 Декабря. 12. 45 .47. 7 Февраля. 12.14.15.16.17.18.19. Сентября", "впути", R.drawable.buses_red));
        busList.add(new Bus(4, "Зарайск", "Коломна", "8:00", "8:10", "31", "4200",
                "Зарайск - Коломна", "ежедневно", "впути", R.drawable.buses_green));
        busList.add(new Bus(5, "Зарайск", "Коломна", "23:32", "8:10", "31", "4200",
                "Зарайск - Коломна", "5. 6. 7. 8. 9 Января. 12. 25 Декабря. 12. 45 .47. 7" +
                " Февраля. 12.14.15.16.17.18.19. Сентября", "впути", Integer.valueOf(R.drawable.buses_red)));
    }

    private void creteDialog() {
        dialog = new AlertDialog.Builder(this);
        dialog.setTitle("поиск расписания");
        dialog.setMessage("Такой маршрут уже был добавлен ранее, хотите обновить?");
        dialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                isOnline();
                if (internetConnect) {
                    dbAdapter.deleteTable(searchReys);
                    dbAdapter.createTable(searchReys);
                    busList.clear();
//                    new Download(getApplicationContext(), dbAdapter.findCode(editTextFrom.getText().toString()),
//                            dbAdapter.findCode(editTextTo.getText().toString()), null, false).execute();
                    RefrashTime();
                    dbAdapter.resetHistiryRasp(searchReys, editTextFrom.getText().toString(), stationDBFrom.getCodeStation(),
                            editTextTo.getText().toString(), stationDBTo.getCodeStation());

                    mSVProgressHUD.showSuccessWithStatus("Готово!");
                    Toast.makeText(getApplicationContext(), "Расписание " + editTextFrom.getText()
                            + "-" + editTextTo.getText() + " успешно обновлено...", Toast.LENGTH_LONG).show();

                } else {
                    mSVProgressHUD.showInfoWithStatus("нет интернета...");
                }
            }
        });
        dialog.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "Расписание используется из БД...", Toast.LENGTH_SHORT).show();
                busList.clear();
                List<Bus> lb = new ArrayList<>();
                lb = dbAdapter.recordRaspTable(searchReys);
                Log.d("OOOOP", lb.toString());
                Log.d("OOOOP", lb.toString());
                for (int i = 0; i < lb.size(); i++) {
                    busList.add(lb.get(i));
                }
//               RefrashTime();
                adapterBus.notifyDataSetChanged();
                recycler.setAdapter(adapterBus);
//                dbAdapter.resetHistiryRasp(searchReys);
                mSVProgressHUD.showSuccessWithStatus("Готово!");
            }
        });
    }

    private void setIconSwitch() {
        final IconSwitch.Checked checked = null;
        iconSwitch.setBackgroundColor(Color.parseColor("#FF1D1D1D"));

        iconSwitch.setThumbColorLeft(Color.parseColor("#00ff8a"));
        iconSwitch.setThumbColorRight(Color.parseColor("#ff0066"));

        iconSwitch.setActiveTintIconLeft(Color.parseColor("#ffffff"));
        iconSwitch.setInactiveTintIconLeft(Color.parseColor("#00ff7e"));
        iconSwitch.setActiveTintIconRight(Color.parseColor("#ffffff"));
        iconSwitch.setInactiveTintIconRight(Color.parseColor("#ff0066"));
        iconSwitch.setCheckedChangeListener(new IconSwitch.CheckedChangeListener() {
            @Override
            public void onCheckChanged(IconSwitch.Checked current) {
                switch (current.toggle()) {
                    case LEFT:
                        if (searchReys != null) {
                            busList.clear();
                            List<Bus> list = new ArrayList();
                            list = dbAdapter.recordRaspTable(searchReysReverse);

                            for (int i = 0; i < list.size(); i++) {
                                busList.add(list.get(i));
                            }

                            // RefrashTime();
                            adapterBus.notifyDataSetChanged();
                            recycler.setAdapter(adapterBus);
                            //   Toast.makeText(getApplicationContext(), "left", Toast.LENGTH_SHORT).show();
                            editTextTo.setText(from);
                            editTextFrom.setText(to);
                            mSVProgressHUD.showInfoWithStatus(editTextTo.getText() + "-" + editTextFrom.getText());
                        }

                        break;
                    case RIGHT:
                        if (searchReysReverse != null) {
                            busList.clear();
                            List<Bus> list2 = new ArrayList();
                            list2 = dbAdapter.recordRaspTable(searchReys);

                            for (int i = 0; i < list2.size(); i++) {
                                busList.add(list2.get(i));
                            }

                            //  RefrashTime();
                            adapterBus.notifyDataSetChanged();
                            recycler.setAdapter(adapterBus);
                            //     Toast.makeText(getApplicationContext(), "right", Toast.LENGTH_SHORT).show();
                            editTextTo.setText(to);
                            editTextFrom.setText(from);
                            mSVProgressHUD.showInfoWithStatus(editTextFrom.getText() + "-" + editTextTo.getText());

                        }
                        break;
                    default:
                }
            }
        });
    }


    public void showErrorWithStatus(View view) {
        mSVProgressHUD.showErrorWithStatus("заполните все поля", SVProgressHUD.SVProgressHUDMaskType.GradientCancel);
    }

    public boolean isNetworkOnline(Context context) {
        boolean status = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);
            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                status = true;
            } else {
                netInfo = cm.getNetworkInfo(1);
                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED)
                    status = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return status;

    }

    public class Status {
        public Integer idImg;
        public int max = 0;
        public boolean send = false;

        public Integer trackTransport(String fromTime, String toTime) {
            Time tFrom = new Time();
            Time tTo = new Time();
            Time time = new Time(Time.getCurrentTimezone());
            time.setToNow();
            int[] timeFrom = timeToInt(fromTime);
            int[] timeTo = timeToInt(toTime);
            tFrom.set(0, timeFrom[1], timeFrom[0], 0, 0, 0);
            tTo.set(0, timeTo[1], timeTo[0], 0, 0, 0);
            Log.d("TRACKING_TRANSPORT", String.valueOf(timeFrom[0]) + "  " + String.valueOf(timeFrom[1]));
            Log.d("TRACKING_TRANSPORT", String.valueOf(timeTo[0]) + "  " + String.valueOf(timeTo[1]));
            int hourFrom = tFrom.hour - time.hour;
            int minuteFrom = tFrom.minute - time.minute;
            int hourTo = tTo.hour - time.hour;
            int minuteTo = tTo.minute - time.minute;
            int minuteMinus = tTo.minute - tFrom.minute;
            int minuteSumTime = ((tTo.hour - tFrom.hour) * 60) + minuteMinus;
            this.max = ((tTo.hour * 60) + tTo.minute) - ((tFrom.hour * 60) + tFrom.minute);
            if ((tFrom.hour * 60) + tFrom.minute >= (time.hour * 60) + time.minute || (time.hour * 60) + time.minute >= (tTo.hour * 60) + tTo.minute) {
                this.send = false;
                return 0;
            }
            this.send = true;
            this.idImg = R.drawable.buses_yellow;
            Integer result = this.max - (((tTo.hour * 60) + tTo.minute) - ((time.hour * 60) + time.minute));
            Log.d("TIME_MINUTE", String.valueOf(result));
            if (result >= 0 && result <= this.max) {
                return result;
            }
            this.send = false;
            return result;
        }

        public String statusBus(String time) {
            Time t = new Time();
            int[] timeMas = timeToInt(time);
            t.set(0, timeMas[1], timeMas[0], 0, 0, 0);
            Time tNow = new Time(Time.getCurrentTimezone());
            tNow.setToNow();
            int hour = t.hour - tNow.hour;
            int minute = t.minute - tNow.minute;
            if ((hour < 0 || minute < 0) && (hour <= 0 || minute > 0)) {
                this.idImg = R.drawable.buses_red;
                if (hour != 0) {
                    return "ушел";
                }

                if (minute < 0) {
                    minute = -minute;
                }
                return String.valueOf(minute) + " мин. назад";
            }
            this.idImg = R.drawable.buses_green;
            String str = "отправка через " + (String.valueOf(hour) == "0" ? "" : String.valueOf(hour) + " ч. ");
            if (minute < 0) {
                minute = -minute;
            }
            return String.valueOf(minute) + " мин.";
        }

        public String statusBusV2(String time) {
            Time t = new Time();
            int[] timeMas = timeToInt(time);
            t.set(0, timeMas[1], timeMas[0], 0, 0, 0);
            Time tNow = new Time(Time.getCurrentTimezone());
            tNow.setToNow();
            int hour = t.hour - tNow.hour;
            int result = (hour * 60) + (t.minute - tNow.minute);
            Integer pHour = FormatSecToTime(result).get(0);
            Integer pMinute = FormatSecToTime(result).get(1);
            if (result > 0) {
                String str;
                this.idImg = R.drawable.buses_green;
                StringBuilder append = new StringBuilder().append("отправка через ").append(pHour == 0 ? "" : String.valueOf(pHour) + " ч. ");
                if (pMinute == 0) {
                    str = "";
                } else {
                    str = String.valueOf(pMinute) + " мин.";
                }
                return append.append(str).toString();
            } else if (hour == 0) {
                this.idImg = R.drawable.buses_yellow;
                return "ушел " + String.valueOf(pMinute < 0 ? -pMinute : pMinute) + " мин. назад";
            } else if (result > (-this.max)) {
                this.idImg = R.drawable.buses_yellow;
                return "ушел";
            } else {
                this.idImg = R.drawable.buses_red;
                return "ушел";
            }
        }
    }

    private void loadHistory() {
        if (dbAdapter.hashHistoryRasp()) {
            String reys = this.dbAdapter.recordhistoryRasp();
            if (dbAdapter.isRecordToRaspTable(reys)) {
                this.busList.clear();
                List<Bus> lb = new ArrayList();
                lb = this.dbAdapter.recordRaspTable(reys);
                for (int i = 0; i < lb.size(); i++) {
                    this.busList.add(lb.get(i));
                }
                RefrashTime();

                this.adapterBus.notifyDataSetChanged();
                this.searchReys = reys;

                String[] s = reys.split("_");
                this.from = s[0];
                this.to = s[1];
                this.searchReysReverse = s[1] + "_" + s[0];
                Log.d("STRINGF", this.searchReys + " " + this.searchReysReverse);
            } else Log.d("loadHistory", "пустая табл");
        } else {
            preview();
            adapterBus.notifyDataSetChanged();
            recycler.setAdapter(adapterBus);
            RefrashTime();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    private void getRasp() {

    }

    public void RefrashTime() {
        Timer t = new Timer();
        final Status status = new Status();
        t.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                ActivityMain.this.runOnUiThread(new Runnable() {
                    public void run() {
                        for (int i = 0; i < busList.size(); i++) {
                            busList.get(i).setStatus(status.statusBusV2(busList.get(i).getFromTime()));
                            busList.get(i).setIdImg(status.idImg);
                            busList.get(i).setPosition(status.trackTransport(busList.get(i).getFromTime(), busList.get(i).getToTime()));
                            Log.d("PROVERKA_POS", String.valueOf(status.trackTransport(busList.get(i).getFromTime(), busList.get(i).getToTime())));
                            busList.get(i).setTrack_max(status.max);
                            Log.d("PROVERKA", String.valueOf(status.max));
                            busList.get(i).setTrack_visible(status.send);
                        }
                        adapterBus.notifyDataSetChanged();
                        recycler.setAdapter(adapterBus);
                    }
                });
            }
        }, 0, 5000);
    }

    protected boolean isOnline3() {
        String cs = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(cs);
        if (cm.getActiveNetworkInfo() == null) {
            return false;
        } else {
            return true;
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_thread:
                startActivity(new Intent(this, ActivityThread.class));
                return true;
            case R.id.action_shudle:
                startActivity(new Intent(this, ActivityShudle.class));
                return true;
            case R.id.action_dataRasp:
                startActivity(new Intent(this, ActivityDataRasp.class));
                break;
            case R.id.action_bookmark:
                startActivity(new Intent(this, ActivityBookMark.class));
                break;
            case R.id.action_test:
                startActivity(new Intent(this, TestActivity.class));
                break;
            case R.id.action_test_sql:
                startActivity(new Intent(this, ActivityTestSQL.class));
                break;
            case R.id.action_nearest_settlement:
                startActivity(new Intent(this, ActivityNearestSettlement.class));
                break;
            case R.id.action_nearest_stations:
                startActivity(new Intent(this, ActivityNearestStations.class));
                break;
            case R.id.action_web:
                startActivity(new Intent(this, ActivityWebView.class));
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }

    public static boolean hasConnection(final Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getActiveNetworkInfo();
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public static boolean isOnline2(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    protected void isOnline() {
        String cs = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(cs);
        if (cm.getActiveNetworkInfo() == null) {
            internetConnect = false;
        } else {
            internetConnect = true;
        }
    }

    public List<Integer> FormatSecToTime(Integer time) {
        List<Integer> result = new ArrayList();
        Integer hour = time / 60;
        Integer minute = time - (hour * 60);
        result.add(hour);
        result.add(minute);
        return result;
    }

    public int[] timeToInt(String time) {
        int[] result = new int[0];
        String[] str = time.split(":");
        int chour = Integer.parseInt(str[0]);
        int minute = Integer.parseInt(str[1]);
        return new int[]{chour, minute};
    }

    private void createDialogOptionSearch() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityMain.this);
        View view2 = getLayoutInflater().inflate(R.layout.dialog, null); // Получаем layout по его ID

        List<TypeTransport> transportList = new ArrayList<>();
        transportList.add(new TypeTransport("автобус", R.drawable.buses_red, Yandex.TRANSPORT_TYPE.bus, 0));
        transportList.add(new TypeTransport("поезд", R.drawable.train_red, Yandex.TRANSPORT_TYPE.train, 1));
        transportList.add(new TypeTransport("электричка", R.drawable.surban_red, Yandex.TRANSPORT_TYPE.suburban, 2));
        transportList.add(new TypeTransport("самолет", R.drawable.fly_red, Yandex.TRANSPORT_TYPE.plane, 3));
        transportList.add(new TypeTransport("самолет", R.drawable.helicopter_red, Yandex.TRANSPORT_TYPE.helicopter, 4));
        transportList.add(new TypeTransport("водное судно", R.drawable.ship_red, Yandex.TRANSPORT_TYPE.water, 5));
        transportList.add(new TypeTransport("весь транспорт ", R.drawable.all, null, 6));

        final AdapterTypeTransport adapterTypeTransport = new AdapterTypeTransport(getApplicationContext(), transportList);
        final Spinner spinner = (Spinner) view2.findViewById(R.id.typeTransportSpinner);
        spinner.setAdapter(adapterTypeTransport);
        final CheckBox checkBox = (CheckBox) view2.findViewById(R.id.isPer);


        builder.setView(view2);
        builder.setPositiveButton("готово", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                transportType = ((TypeTransport) spinner.getSelectedItem()).getTransportType();
                isTransfers = checkBox.isChecked();
                Toast.makeText(getApplicationContext(), "тип транспорта: " + ((TypeTransport) spinner.getSelectedItem()).getTitle() + " (" +
                        transportType + ") " +
                        spinner.getSelectedItemPosition() + ". пересадки: " + isTransfers, Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        }).setNegativeButton("отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.dismiss();
            }
        });

        builder.create();
        builder.show();
    }

    private void refreshBusList() {

    }

    private class Download extends AsyncTask<String, Void, Void> {

        private ProgressDialog pr;
        private boolean isDownload = false;
        private String titleMessage;
        private Yandex.SEARCH_RASP searchRasp;
        private Context context;
        private String url = "";
        private Yandex.API api;
        private Document doc;
        public boolean isSet = false;
        private Yandex.SearchAPI searchapiD;

        //search
        public Download(Context context, String from, String to, Yandex.TRANSPORT_TYPE transportType, boolean transfers, Yandex.SEARCH_RASP searchRasp) {
            this.context = context;
            this.api = Yandex.API.search;
            this.searchRasp = searchRasp;
            url = "https://api.rasp.yandex.net/v3.0/search/?apikey=86973449-b547-417e-95da-e70f87ef8748&format=xml&limit=5000&from=" + from + "&to=" + to;
            if (transportType != null)
                url += "&transport_types=" + transportType;
            if (transfers)
                url += "&transfers=" + transfers;
            Log.d("url", url);
            searchapiD = new Yandex.SearchAPI();
            dbAdapter = new DBAdapter(context);
        }

        protected void onPreExecute() {
            pr = new ProgressDialog(ActivityMain.this);
            pr.setTitle("Загрузка рассписания");
            pr.setMessage("Соединение с сервером...");
            pr.setIndeterminate(false);
            pr.show();
            super.onPreExecute();
        }

        protected Void doInBackground(String... Url) {
            try {
                doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new URL(url).openStream()));
                doc.getDocumentElement().normalize();
                doc.getDocumentElement().getTagName();
                isDownload = true;
            } catch (Exception e) {
                Log.d("download", "error network");
                isDownload = false;
            }
            return null;
        }

        @SuppressLint("LongLogTag")
        protected void onPostExecute(Void aVoid) {

            switch (searchRasp) {
                case from_to:
                    if (isDownload) {

                        Log.d("search_parse", doc.getDocumentElement().getElementsByTagName("total").item(0).getTextContent());
                        searchapiD.getPagination().setTotal(doc.getDocumentElement().getElementsByTagName("total").item(0).getTextContent());

                        Element search = (Element) doc.getDocumentElement().getElementsByTagName("search").item(0);
                        Log.d("search_parse", "============to===============");
                        Log.d("search_parse", search.getElementsByTagName("code").item(0).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getToSearch().setCode(search.getElementsByTagName("code").item(0).getChildNodes().item(0).getNodeValue());
                        Log.d("search_parse", search.getElementsByTagName("type").item(0).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getToSearch().setType(Yandex.setType(search.getElementsByTagName("type").item(0).getChildNodes().item(0).getNodeValue()));
                        try {
                            Log.d("search_parse", search.getElementsByTagName("popular_title").item(0).getChildNodes().item(0).getTextContent());
                            searchapiD.getSearch().getToSearch().setPopularTitle(search.getElementsByTagName("popular_title").item(0).getChildNodes().item(0).getNodeValue());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.d("search_parse", search.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getToSearch().setTitle(search.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
                        try {
                            Log.d("search_parse", search.getElementsByTagName("short_title").item(0).getChildNodes().item(0).getNodeValue());
                            searchapiD.getSearch().getToSearch().setShortTitle(search.getElementsByTagName("short_title").item(0).getChildNodes().item(0).getNodeValue());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Log.d("search_parse", "============from===============");
                        Log.d("search_parse", search.getElementsByTagName("code").item(1).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getFromSearch().setCode(search.getElementsByTagName("code").item(1).getChildNodes().item(0).getNodeValue());
                        Log.d("search_parse", search.getElementsByTagName("type").item(1).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getFromSearch().setType(Yandex.setType(search.getElementsByTagName("type").item(1).getChildNodes().item(0).getNodeValue()));
                        Log.d("search_parse", search.getElementsByTagName("popular_title").item(1).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getFromSearch().setPopularTitle(search.getElementsByTagName("popular_title").item(1).getChildNodes().item(0).getNodeValue());
                        Log.d("search_parse", search.getElementsByTagName("title").item(1).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getFromSearch().setTitle(search.getElementsByTagName("title").item(1).getChildNodes().item(0).getNodeValue());
                        Log.d("search_parse", search.getElementsByTagName("short_title").item(1).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getFromSearch().setShortTitle(search.getElementsByTagName("short_title").item(1).getChildNodes().item(0).getNodeValue());

                        NodeList scheduleList = doc.getElementsByTagName("segment");
                        for (int i = 0; i < scheduleList.getLength(); i++) {
                            Yandex.SearchAPI.Segments segmen = new Yandex.SearchAPI.Segments();

                            Element segment = (Element) scheduleList.item(i);
                            Log.d("search_parse", segment.getElementsByTagName("except_days").item(0).getTextContent());
                            segmen.setExceptDays(segment.getElementsByTagName("except_days").item(0).getTextContent());
                            Log.d("search_parse", segment.getElementsByTagName("arrival").item(0).getTextContent());
                            segmen.setArrival(segment.getElementsByTagName("arrival").item(0).getTextContent());
                            Log.d("search_parse", segment.getElementsByTagName("departure").item(0).getTextContent());
                            segmen.setDeparture(segment.getElementsByTagName("departure").item(0).getTextContent());
                            Log.d("search_parse", segment.getElementsByTagName("duration").item(0).getTextContent());
                            segmen.setDuration(Format.FormatDurationTotime(segment.getElementsByTagName("duration").item(0).getTextContent()));
                            Log.d("search_parse", segment.getElementsByTagName("arrival_terminal").item(0).getTextContent());
                            segmen.setArrivalTerminal(segment.getElementsByTagName("arrival_terminal").item(0).getTextContent());
                            Log.d("search_parse", segment.getElementsByTagName("arrival_platform").item(0).getTextContent());
                            segmen.setArrivalPlatform(segment.getElementsByTagName("arrival_platform").item(0).getTextContent());
                            Log.d("search_parse", segment.getElementsByTagName("departure_platform").item(0).getTextContent());
                            segmen.setDeparturePlatform(segment.getElementsByTagName("departure_platform").item(0).getTextContent());
                            Log.d("search_parse", segment.getElementsByTagName("days").item(0).getTextContent());
                            segmen.setDays(segment.getElementsByTagName("days").item(0).getTextContent());
                            Log.d("search_parse", segment.getElementsByTagName("start_date").item(0).getTextContent());
                            segmen.setStartDate(segment.getElementsByTagName("start_date").item(0).getTextContent());
                            Log.d("search_parse", segment.getElementsByTagName("stops").item(0).getTextContent());
                            segmen.setStops(segment.getElementsByTagName("stops").item(0).getTextContent());

                            Element from = (Element) segment.getElementsByTagName("from").item(0);
                            Log.d("search_parse", from.getElementsByTagName("code").item(0).getTextContent());
                            segmen.getFrom().setCode(from.getElementsByTagName("code").item(0).getTextContent());
                            Log.d("search_parse", from.getElementsByTagName("station_type").item(0).getTextContent());
                            segmen.getFrom().setStationType(Yandex.setTypeStation(from.getElementsByTagName("station_type").item(0).getTextContent()));
                            Log.d("search_parse", from.getElementsByTagName("title").item(0).getTextContent());
                            segmen.getFrom().setTitle(from.getElementsByTagName("title").item(0).getTextContent());
                            Log.d("search_parse", from.getElementsByTagName("popular_title").item(0).getTextContent());
                            segmen.getFrom().setPopularTitle(from.getElementsByTagName("popular_title").item(0).getTextContent());
                            Log.d("search_parse", from.getElementsByTagName("transport_type").item(0).getTextContent());
                            segmen.getFrom().setTransportType(Yandex.setTransportType(from.getElementsByTagName("transport_type").item(0).getTextContent()));
                            Log.d("search_parse", from.getElementsByTagName("station_type_name").item(0).getTextContent());
                            segmen.getFrom().setStationTypeName(from.getElementsByTagName("station_type_name").item(0).getTextContent());
                            Log.d("search_parse", from.getElementsByTagName("type").item(0).getTextContent());
                            segmen.getFrom().setType(Yandex.setType(from.getElementsByTagName("type").item(0).getTextContent()));

                            Element thread = (Element) segment.getElementsByTagName("thread").item(0);

                            Log.d("search_parse_thread", thread.getElementsByTagName("transport_type").item(0).getTextContent());
                            segmen.getThread().setTransportType(Yandex.setTransportType(thread.getElementsByTagName("transport_type").item(0).getTextContent()));
                            Log.d("search_parse_thread", thread.getElementsByTagName("uid").item(0).getTextContent());
                            segmen.getThread().setUid(thread.getElementsByTagName("uid").item(0).getTextContent());
                            Log.d("search_parse_thread", "#" + i + " non     " + thread.getChildNodes().item(4).getTextContent());
                            segmen.getThread().setTitle(thread.getChildNodes().item(4).getTextContent());
                            Log.d("search_parse_thread", thread.getElementsByTagName("vehicle").item(0).getTextContent());
                            segmen.getThread().setVehicle(thread.getElementsByTagName("vehicle").item(0).getTextContent());
                            Log.d("search_parse_thread", thread.getElementsByTagName("number").item(0).getTextContent());
                            segmen.getThread().setNumber(thread.getElementsByTagName("number").item(0).getTextContent());
                            Log.d("search_parse_thread", thread.getElementsByTagName("express_type").item(0).getTextContent());
                            segmen.getThread().setExpressType(Yandex.setExpressType(thread.getElementsByTagName("express_type").item(0).getTextContent()));
                            Log.d("search_parse_thread", thread.getElementsByTagName("thread_method_link").item(0).getTextContent());
                            segmen.getThread().setThreadMethodLink(thread.getElementsByTagName("thread_method_link").item(0).getTextContent());

                            Log.d("search_parse_transport_subtype", thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(0).getTextContent());
                            segmen.getThread().getTransportSubtype().setColor(thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(0).getTextContent());
                            Log.d("search_parse_transport_subtype", thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(1).getTextContent());
                            segmen.getThread().getTransportSubtype().setCode(thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(1).getTextContent());
                            Log.d("search_parse_transport_subtype", thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(2).getTextContent());
                            segmen.getThread().getTransportSubtype().setTitle(thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(2).getTextContent());

                            try {
                                Element carrier = (Element) thread.getElementsByTagName("carrier").item(0);
                                Log.d("search_parse", carrier.getElementsByTagName("code").item(0).getTextContent());
                                segmen.getThread().getCarrier().setCode(carrier.getElementsByTagName("code").item(0).getTextContent());
                                Log.d("search_parse_title_carrier", carrier.getElementsByTagName("title").item(0).getChildNodes().item(0).getTextContent());
                                segmen.getThread().getCarrier().setTitle(carrier.getElementsByTagName("title").item(0).getTextContent());
                                Log.d("search_parse", carrier.getElementsByTagName("url").item(0).getTextContent());
                                segmen.getThread().getCarrier().setUrl(carrier.getElementsByTagName("url").item(0).getTextContent());
                                Log.d("search_parse", carrier.getElementsByTagName("phone").item(0).getTextContent());
                                segmen.getThread().getCarrier().setPhone(carrier.getElementsByTagName("phone").item(0).getTextContent());
                                Log.d("search_parse", carrier.getElementsByTagName("address").item(0).getTextContent());
                                segmen.getThread().getCarrier().setAddress(carrier.getElementsByTagName("address").item(0).getTextContent());
                                Log.d("search_parse", carrier.getElementsByTagName("email").item(0).getTextContent());
                                segmen.getThread().getCarrier().setEmail(carrier.getElementsByTagName("email").item(0).getTextContent());
                                Log.d("search_parse", carrier.getElementsByTagName("logo").item(0).getTextContent());
                                segmen.getThread().getCarrier().setLogo(carrier.getElementsByTagName("logo").item(0).getTextContent());
                                Log.d("search_parse", carrier.getElementsByTagName("contacts").item(0).getTextContent());
                                segmen.getThread().getCarrier().setContacts(carrier.getElementsByTagName("contacts").item(0).getTextContent());
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("search_parse", "carrier пустое поле");
                            }

                            Element to = (Element) segment.getElementsByTagName("to").item(0);
                            Log.d("search_parse", to.getElementsByTagName("code").item(0).getTextContent());
                            segmen.getTo().setCode(to.getElementsByTagName("code").item(0).getTextContent());
                            Log.d("search_parse", to.getElementsByTagName("station_type").item(0).getTextContent());
                            segmen.getTo().setStationType(Yandex.setTypeStation(to.getElementsByTagName("station_type").item(0).getTextContent()));
                            Log.d("search_parse", to.getElementsByTagName("title").item(0).getTextContent());
                            segmen.getTo().setTitle(to.getElementsByTagName("title").item(0).getTextContent());
                            Log.d("search_parse", to.getElementsByTagName("popular_title").item(0).getTextContent());
                            segmen.getTo().setPopularTitle(to.getElementsByTagName("popular_title").item(0).getTextContent());
                            Log.d("search_parse", to.getElementsByTagName("transport_type").item(0).getTextContent());
                            segmen.getTo().setTransportType(Yandex.setTransportType(to.getElementsByTagName("transport_type").item(0).getTextContent()));
                            Log.d("search_parse", to.getElementsByTagName("station_type_name").item(0).getTextContent());
                            segmen.getTo().setStationTypeName(to.getElementsByTagName("station_type_name").item(0).getTextContent());
                            Log.d("search_parse", to.getElementsByTagName("type").item(0).getTextContent());
                            segmen.getTo().setType(Yandex.setType(to.getElementsByTagName("type").item(0).getTextContent()));

                            segmen.setId(i);
                            segmen.setStatus("в пути");
                            segmen.setIdImg(R.drawable.buses_green);
                            searchapiD.getSegments().add(segmen);
                        }

                        searchAPI.setSegments(searchapiD.getSegments());
                        adapterListSearch.notifyDataSetChanged();
                        ;
                        dbAdapter.insertRaspTableV2("Зарайск_Луховицы", searchapiD);
                        titleMessage = "готово";
                        Log.d("SearchAPI", searchapiD.getSegments().toString());
                    } else
                        titleMessage = "оишибка...";

                    break;
                case to_from:
                    if (isDownload) {

                        pr.dismiss();

                        Log.d("search_parse", doc.getDocumentElement().getElementsByTagName("total").item(0).getTextContent());
                        searchapiD.getPagination().setTotal(doc.getDocumentElement().getElementsByTagName("total").item(0).getTextContent());

                        Element search = (Element) doc.getDocumentElement().getElementsByTagName("search").item(0);
                        Log.d("search_parse", "============to===============");
                        Log.d("search_parse", search.getElementsByTagName("code").item(0).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getToSearch().setCode(search.getElementsByTagName("code").item(0).getChildNodes().item(0).getNodeValue());
                        Log.d("search_parse", search.getElementsByTagName("type").item(0).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getToSearch().setType(Yandex.setType(search.getElementsByTagName("type").item(0).getChildNodes().item(0).getNodeValue()));
                        try {
                            Log.d("search_parse", search.getElementsByTagName("popular_title").item(0).getChildNodes().item(0).getTextContent());
                            searchapiD.getSearch().getToSearch().setPopularTitle(search.getElementsByTagName("popular_title").item(0).getChildNodes().item(0).getNodeValue());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.d("search_parse", search.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getToSearch().setTitle(search.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
                        try {
                            Log.d("search_parse", search.getElementsByTagName("short_title").item(0).getChildNodes().item(0).getNodeValue());
                            searchapiD.getSearch().getToSearch().setShortTitle(search.getElementsByTagName("short_title").item(0).getChildNodes().item(0).getNodeValue());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Log.d("search_parse", "============from===============");
                        Log.d("search_parse", search.getElementsByTagName("code").item(1).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getFromSearch().setCode(search.getElementsByTagName("code").item(1).getChildNodes().item(0).getNodeValue());
                        Log.d("search_parse", search.getElementsByTagName("type").item(1).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getFromSearch().setType(Yandex.setType(search.getElementsByTagName("type").item(1).getChildNodes().item(0).getNodeValue()));
                        Log.d("search_parse", search.getElementsByTagName("popular_title").item(1).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getFromSearch().setPopularTitle(search.getElementsByTagName("popular_title").item(1).getChildNodes().item(0).getNodeValue());
                        Log.d("search_parse", search.getElementsByTagName("title").item(1).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getFromSearch().setTitle(search.getElementsByTagName("title").item(1).getChildNodes().item(0).getNodeValue());
                        Log.d("search_parse", search.getElementsByTagName("short_title").item(1).getChildNodes().item(0).getNodeValue());
                        searchapiD.getSearch().getFromSearch().setShortTitle(search.getElementsByTagName("short_title").item(1).getChildNodes().item(0).getNodeValue());

                        NodeList scheduleList = doc.getElementsByTagName("segment");
                        for (int i = 0; i < scheduleList.getLength(); i++) {
                            Yandex.SearchAPI.Segments segmen = new Yandex.SearchAPI.Segments();

                            Element segment = (Element) scheduleList.item(i);
                            Log.d("search_parse", segment.getElementsByTagName("except_days").item(0).getTextContent());
                            segmen.setExceptDays(segment.getElementsByTagName("except_days").item(0).getTextContent());
                            Log.d("search_parse", segment.getElementsByTagName("arrival").item(0).getTextContent());
                            segmen.setArrival(segment.getElementsByTagName("arrival").item(0).getTextContent());
                            Log.d("search_parse", segment.getElementsByTagName("departure").item(0).getTextContent());
                            segmen.setDeparture(segment.getElementsByTagName("departure").item(0).getTextContent());
                            Log.d("search_parse", segment.getElementsByTagName("duration").item(0).getTextContent());
                            segmen.setDuration(segment.getElementsByTagName("duration").item(0).getTextContent());
                            Log.d("search_parse", segment.getElementsByTagName("arrival_terminal").item(0).getTextContent());
                            segmen.setArrivalTerminal(segment.getElementsByTagName("arrival_terminal").item(0).getTextContent());
                            Log.d("search_parse", segment.getElementsByTagName("arrival_platform").item(0).getTextContent());
                            segmen.setArrivalPlatform(segment.getElementsByTagName("arrival_platform").item(0).getTextContent());
                            Log.d("search_parse", segment.getElementsByTagName("departure_platform").item(0).getTextContent());
                            segmen.setDeparturePlatform(segment.getElementsByTagName("departure_platform").item(0).getTextContent());
                            Log.d("search_parse", segment.getElementsByTagName("days").item(0).getTextContent());
                            segmen.setDays(segment.getElementsByTagName("days").item(0).getTextContent());
                            Log.d("search_parse", segment.getElementsByTagName("start_date").item(0).getTextContent());
                            segmen.setStartDate(segment.getElementsByTagName("start_date").item(0).getTextContent());
                            Log.d("search_parse", segment.getElementsByTagName("stops").item(0).getTextContent());
                            segmen.setStops(segment.getElementsByTagName("stops").item(0).getTextContent());

                            Element from = (Element) segment.getElementsByTagName("from").item(0);
                            Log.d("search_parse", from.getElementsByTagName("code").item(0).getTextContent());
                            segmen.getFrom().setCode(from.getElementsByTagName("code").item(0).getTextContent());
                            Log.d("search_parse", from.getElementsByTagName("station_type").item(0).getTextContent());
                            segmen.getFrom().setStationType(Yandex.setTypeStation(from.getElementsByTagName("station_type").item(0).getTextContent()));
                            Log.d("search_parse", from.getElementsByTagName("title").item(0).getTextContent());
                            segmen.getFrom().setTitle(from.getElementsByTagName("title").item(0).getTextContent());
                            Log.d("search_parse", from.getElementsByTagName("popular_title").item(0).getTextContent());
                            segmen.getFrom().setPopularTitle(from.getElementsByTagName("popular_title").item(0).getTextContent());
                            Log.d("search_parse", from.getElementsByTagName("transport_type").item(0).getTextContent());
                            segmen.getFrom().setTransportType(Yandex.setTransportType(from.getElementsByTagName("transport_type").item(0).getTextContent()));
                            Log.d("search_parse", from.getElementsByTagName("station_type_name").item(0).getTextContent());
                            segmen.getFrom().setStationTypeName(from.getElementsByTagName("station_type_name").item(0).getTextContent());
                            Log.d("search_parse", from.getElementsByTagName("type").item(0).getTextContent());
                            segmen.getFrom().setType(Yandex.setType(from.getElementsByTagName("type").item(0).getTextContent()));

                            Element thread = (Element) segment.getElementsByTagName("thread").item(0);

                            Log.d("search_parse_thread", thread.getElementsByTagName("transport_type").item(0).getTextContent());
                            segmen.getThread().setTransportType(Yandex.setTransportType(thread.getElementsByTagName("transport_type").item(0).getTextContent()));
                            Log.d("search_parse_thread", thread.getElementsByTagName("uid").item(0).getTextContent());
                            segmen.getThread().setUid(thread.getElementsByTagName("uid").item(0).getTextContent());
                            Log.d("search_parse_thread", "#" + i + " non     " + thread.getChildNodes().item(4).getTextContent());
                            segmen.getThread().setTitle(thread.getChildNodes().item(4).getTextContent());
                            Log.d("search_parse_thread", thread.getElementsByTagName("vehicle").item(0).getTextContent());
                            segmen.getThread().setVehicle(thread.getElementsByTagName("vehicle").item(0).getTextContent());
                            Log.d("search_parse_thread", thread.getElementsByTagName("number").item(0).getTextContent());
                            segmen.getThread().setNumber(thread.getElementsByTagName("number").item(0).getTextContent());
                            Log.d("search_parse_thread", thread.getElementsByTagName("express_type").item(0).getTextContent());
                            segmen.getThread().setExpressType(Yandex.setExpressType(thread.getElementsByTagName("express_type").item(0).getTextContent()));
                            Log.d("search_parse_thread", thread.getElementsByTagName("thread_method_link").item(0).getTextContent());
                            segmen.getThread().setThreadMethodLink(thread.getElementsByTagName("thread_method_link").item(0).getTextContent());

                            Log.d("search_parse_transport_subtype", thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(0).getTextContent());
                            segmen.getThread().getTransportSubtype().setColor(thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(0).getTextContent());
                            Log.d("search_parse_transport_subtype", thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(1).getTextContent());
                            segmen.getThread().getTransportSubtype().setCode(thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(1).getTextContent());
                            Log.d("search_parse_transport_subtype", thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(2).getTextContent());
                            segmen.getThread().getTransportSubtype().setTitle(thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(2).getTextContent());

                            try {
                                Element carrier = (Element) thread.getElementsByTagName("carrier").item(0);
                                Log.d("search_parse", carrier.getElementsByTagName("code").item(0).getTextContent());
                                segmen.getThread().getCarrier().setCode(carrier.getElementsByTagName("code").item(0).getTextContent());
                                Log.d("search_parse_title_carrier", carrier.getElementsByTagName("title").item(0).getChildNodes().item(0).getTextContent());
                                segmen.getThread().getCarrier().setTitle(carrier.getElementsByTagName("title").item(0).getTextContent());
                                Log.d("search_parse", carrier.getElementsByTagName("url").item(0).getTextContent());
                                segmen.getThread().getCarrier().setUrl(carrier.getElementsByTagName("url").item(0).getTextContent());
                                Log.d("search_parse", carrier.getElementsByTagName("phone").item(0).getTextContent());
                                segmen.getThread().getCarrier().setPhone(carrier.getElementsByTagName("phone").item(0).getTextContent());
                                Log.d("search_parse", carrier.getElementsByTagName("address").item(0).getTextContent());
                                segmen.getThread().getCarrier().setAddress(carrier.getElementsByTagName("address").item(0).getTextContent());
                                Log.d("search_parse", carrier.getElementsByTagName("email").item(0).getTextContent());
                                segmen.getThread().getCarrier().setEmail(carrier.getElementsByTagName("email").item(0).getTextContent());
                                Log.d("search_parse", carrier.getElementsByTagName("logo").item(0).getTextContent());
                                segmen.getThread().getCarrier().setLogo(carrier.getElementsByTagName("logo").item(0).getTextContent());
                                Log.d("search_parse", carrier.getElementsByTagName("contacts").item(0).getTextContent());
                                segmen.getThread().getCarrier().setContacts(carrier.getElementsByTagName("contacts").item(0).getTextContent());
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("search_parse", "carrier пустое поле");
                            }

                            Element to = (Element) segment.getElementsByTagName("to").item(0);
                            Log.d("search_parse", to.getElementsByTagName("code").item(0).getTextContent());
                            segmen.getTo().setCode(to.getElementsByTagName("code").item(0).getTextContent());
                            Log.d("search_parse", to.getElementsByTagName("station_type").item(0).getTextContent());
                            segmen.getTo().setStationType(Yandex.setTypeStation(to.getElementsByTagName("station_type").item(0).getTextContent()));
                            Log.d("search_parse", to.getElementsByTagName("title").item(0).getTextContent());
                            segmen.getTo().setTitle(to.getElementsByTagName("title").item(0).getTextContent());
                            Log.d("search_parse", to.getElementsByTagName("popular_title").item(0).getTextContent());
                            segmen.getTo().setPopularTitle(to.getElementsByTagName("popular_title").item(0).getTextContent());
                            Log.d("search_parse", to.getElementsByTagName("transport_type").item(0).getTextContent());
                            segmen.getTo().setTransportType(Yandex.setTransportType(to.getElementsByTagName("transport_type").item(0).getTextContent()));
                            Log.d("search_parse", to.getElementsByTagName("station_type_name").item(0).getTextContent());
                            segmen.getTo().setStationTypeName(to.getElementsByTagName("station_type_name").item(0).getTextContent());
                            Log.d("search_parse", to.getElementsByTagName("type").item(0).getTextContent());
                            segmen.getTo().setType(Yandex.setType(to.getElementsByTagName("type").item(0).getTextContent()));

                            searchapiD.getSegments().add(segmen);
                        }

                        titleMessage = "готово";
                        Log.d("SearchAPI", searchapiD.getSegments().toString());
                    } else
                        titleMessage = "оишибка...";
                    break;
            }

            pr.dismiss();

            Toast.makeText(context, titleMessage, Toast.LENGTH_SHORT).show();
            isSet = true;
            super.onPostExecute(aVoid);
        }
    }

}