package com.example.eminem.sac;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.xw.repo.BubbleSeekBar;
import com.yalantis.phoenix.PullToRefreshView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import Model.Settlement;

public class ActivityNearestSettlement extends AppCompatActivity implements LocationListener {

    private TextView lan;
    private TextView lng;
    private TextView provider;
    private ListView listView;
    private List<Settlement> settlements;
    private AdapterSettlement adapterSettlement;
    private LocationManager locationManager;
    private long tim;
    private double longitude = 0;
    private double latitude = 0;
    private String nameProvider;
    private PullToRefreshView mPullToRefreshView;
    private BubbleSeekBar bubbleSeekBar;
    private Download download;
    private boolean isCancel = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearest_settlement);

        lan = (TextView) findViewById(R.id.myLan);
        lng = (TextView) findViewById(R.id.myLng);
        provider = (TextView) findViewById(R.id.provider);
        bubbleSeekBar = (BubbleSeekBar) findViewById(R.id.seekBar);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        mPullToRefreshView = (PullToRefreshView) findViewById(R.id.pull_to_refresh);
        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {

                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        settlements.clear();
                        if (isOnline3() & (longitude != 0) & (latitude != 0)) {
                            download = new Download(getApplicationContext(), latitude,
                                    longitude, bubbleSeekBar.getProgress());
                            download.execute();
                            refresh();
                        }
                        mPullToRefreshView.setRefreshing(false);
                    }
                }, 3000);
            }
        });

        listView = (ListView) findViewById(R.id.listview);
        settlements = new ArrayList<>();
        settlements.add(new Settlement("5.23", "c4235", 45.13174, 32.7832113, "поселение", "Рязань", "", "", 1));
        settlements.add(new Settlement("9.43", "c7635", 32.6456, 23.435552, "поселение", "Владимир", "", "", 2));
        settlements.add(new Settlement("3.56", "c9035", 54.5323, 16.45245, "поселение", "Питер", "", "", 3));
        adapterSettlement = new AdapterSettlement(getApplicationContext(), settlements);
        listView.setAdapter(adapterSettlement);
    }

    private void refresh() {
        Timer t = new Timer();
        Date date = new Date();
        Log.d("+=+=+", date.toString());
        date.setTime(date.getTime()+ 2000);
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (download.isSet & settlements.isEmpty()) {
                    settlements.add(download.settlement);
                    adapterSettlement.notifyDataSetChanged();
                    Helper.getListViewSize(listView);
                    mPullToRefreshView.setRefreshing(false);
                    isCancel = true;
                }
                Log.d("+=+=+", "1");
            }
        };
            t.schedule(new TimerTask() {
                public void run() {
                     runOnUiThread(runnable);
                }
            }, date);
    }

    private void refresh2() {
        Runnable doBackgroundThreadProcessing = new Runnable() {
            public void run() {
                if (download.isSet & settlements.isEmpty()) {
                    settlements.add(download.settlement);
                    adapterSettlement.notifyDataSetChanged();
                    Helper.getListViewSize(listView);
                    mPullToRefreshView.setRefreshing(false);
                }
            }
        };
        Thread thread = new Thread(null, doBackgroundThreadProcessing,
                "Background");
        thread.start();
        try {
            thread.join(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    protected boolean isOnline3() {
        String cs = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(cs);
        if (cm.getActiveNetworkInfo() == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
    }

    @Override
    public void onLocationChanged(Location location) {

        getLocation(location);
    }

    void getLocation(Location location) {
        if (location == null)
            return;
        if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
            formatLocation(location);
        } else if (location.getProvider().equals(LocationManager.NETWORK_PROVIDER)) {
            formatLocation(location);
        }

    }

    private void formatLocation(Location location) {
        longitude = location.getLongitude();
        Log.d("locale_lon", String.valueOf(location.getLongitude()));
        lng.setText(String.valueOf(location.getLongitude()));
        latitude = location.getLatitude();
        Log.d("locale_lan", String.valueOf(location.getLatitude()));
        lan.setText(String.valueOf(location.getLatitude()));
        nameProvider = location.getProvider();
        provider.setText(nameProvider);
        tim = location.getTime();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        provider.setText(nameProvider);
    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
