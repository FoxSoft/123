package com.example.eminem.sac;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Model.StationDB;

public class ActivityTestSQL extends AppCompatActivity {
    private final String TAG = "ActivityTestSQL";
    private AutoCompleteTextView autoCompleteTextView;
    private List<String> hint = new ArrayList<>();
    private DBAdapter dbAdapter;
    private ArrayAdapter<String> arrayAdapter;
    private EditText editText;

    private AdapterHuntStationAutoComplete stationAutoComplete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_sql);
        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.multiTextView);
        stationAutoComplete = new AdapterHuntStationAutoComplete(this);
        autoCompleteTextView.setAdapter(stationAutoComplete);
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                StationDB stationDB = (StationDB) adapterView.getItemAtPosition(position);
                autoCompleteTextView.setText(stationDB.getNameStation());
                Toast.makeText(getApplicationContext(), stationDB.getCodeStation(),Toast.LENGTH_SHORT).show();
                Log.d(TAG, stationDB.getCodeStation()+" "+ stationDB.getCodeCity()+" "+ stationDB.getCodeRegion());
            }
        });

//        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, hint);
//        autoCompleteTextView.setAdapter(arrayAdapter);
//        dbAdapter = new DBAdapter(getApplicationContext());
//
//        List<String> list = new ArrayList<>();
//                hint.clear();
//                list = dbAdapter.getStationOfInput(autoCompleteTextView.getText().toString());
//                for (int i = 0; i < list.size(); i++)
//                {
//                    hint.add(list.get(i));
//                }
//                Log.d("countSearch", String.valueOf(hint));
//                autoCompleteTextView.setAdapter(arrayAdapter);
//                arrayAdapter.notifyDataSetChanged();

//        multiAutoCompleteTextView.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                List<String> list = new ArrayList<>();
//                hint.clear();
//                list = dbAdapter.getStationOfInput(multiAutoCompleteTextView.getText().toString());
//                for (int i = 0; i < list.size(); i++)
//                {
//                    hint.add(list.get(i));
//                }
//                Log.d("countSearch", String.valueOf(hint));
//                multiAutoCompleteTextView.setAdapter(arrayAdapter);
//                arrayAdapter.notifyDataSetChanged();
//
//
//
//            }
//        });
    }
}
