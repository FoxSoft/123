package com.example.eminem.sac;

/**
 * Created by eminem on 22.10.2017.
 */

public class Format {
    public static String FormatDurationTotime(String duration) {
        int sec = (int)Float.parseFloat(duration);
        int hour = sec / 3600;
        int minute = (int)(0.016666666667D * (double)(sec - hour * 3600));
        return hour == 0 ? minute + " мин.":hour + " ч. " + minute + " мин.";
    }
}