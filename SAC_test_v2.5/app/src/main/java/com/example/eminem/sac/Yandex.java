package com.example.eminem.sac;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eminem on 24.12.2017.
 */

public class Yandex {
    public static enum API {nearest_settlement, nearest_stations, search, schedule, thread}

    public static enum STATION_TYPE {
        station, platform, stop, checkpoint, post,
        crossing, overtaking_point, train_station, airport, bus_station, bus_stop,
        unknown, port, wharf, river_port, marine_station
    }

    //    station — станция;
//    platform — платформа;
//    stop — остановочный пункт;
//    checkpoint — блок-пост;
//    post — пост;
//    crossing — разъезд;
//    overtaking_point — обгонный пункт;
//    train_station — вокзал;
//    airport — аэропорт;
//    bus_station — автовокзал;
//    bus_stop — автобусная остановка;
//    unknown — станция без типа;
//    port — порт;
//    port_point — портпункт;
//    wharf — пристань;
//    river_port — речной вокзал;
//    marine_station — морской вокзал.
    public static enum TYPE_CHOICES {
        schedule, tablo, train, suburban, aeroex
    }

    //    schedule — вид расписания по умолчанию;
//    tablo — табло аэропорта;
//    train — расписание железнодорожного вокзала;
//    suburban — расписание электричек;
//    aeroex — расписание аэроэкспрессов.
    public static enum TRANSPORT_TYPE {
        plane, train, suburban, bus, water, helicopter
    }

//    plane — самолет;
//    train — поезд;
//    suburban — электричка;
//    bus — автобус;
//    water — водный транспорт;
//    helicopter — вертолет.

    public static enum EVENT {
        departure, arrival
    }
    // departure — включить в ответ только отправляющиеся со станции нитки (по умолчанию);
    // arrival — включить в ответ только прибывающие на станцию нитки.

    //признак запроса маршрутов с пересадками
    public static boolean TRANSFERS;

    //    Признак экспресса или аэроэкспресса. Значение по умолчанию — xsi:nil=“true“.
//    Если тип транспорта — электричка (элемент transport_type возвращен со значением suburban), принимает одно из значений:
//    express — экспресс-рейс;
//    aeroexpress — рейс, курсирующий между городом и аэропортом.
    public static enum EXPRESS_TYPE {
        express, aeroexpress
    }

    //    Вид пункта отправления.
//    Возможные значения:
//    station — станция;
//    settlement — поселение.
    public static enum TYPE {
        station, settlement
    }

    public static enum SEARCH_RASP{
        from_to, to_from
    }
    //--------------------------Запросы к API--------------------------------//
    //Расписание рейсов между станциями
    public static class SearchAPI {

        /**
         * interval_segments : []
         * pagination : {"total":160,"limit":100,"offset":0}
         * segments : [{"except_days":"","arrival":"02:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6154_7_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6154","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-24&uid=SU-6154_7_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"00:01:00","stops":"","days":"чт с 24.05 по 27.09","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9240,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-24","arrival_platform":""},{"except_days":"","arrival":"04:15:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6152_6_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6152","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-21&uid=SU-6152_6_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"01:40:00","stops":"","days":"пн с 21.05 по 01.10","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-21","arrival_platform":""},{"except_days":"","arrival":"05:00:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1627_1_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1627","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-01&uid=SU-1627_1_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"02:25:00","stops":"","days":"ежедневно с 01.05 по 01.06","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-01","arrival_platform":""},{"except_days":"","arrival":"05:00:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1627_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1627","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-06-02&uid=SU-1627_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"02:25:00","stops":"","days":"ежедневно с 02.06 по 19.10","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-06-02","arrival_platform":""},{"except_days":"","arrival":"05:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"S7-268_0_c260_547","title":"Симферополь \u2014 Москва","number":"S7 268","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-06-02&uid=S7-268_0_c260_547","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"03:05:00","stops":"","days":"ежедневно с 02.06 по 24.09","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-06-02","arrival_platform":""},{"except_days":"","arrival":"05:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"GH-268_0_c260_12","title":"Симферополь \u2014 Москва","number":"GH 268","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-06-02&uid=GH-268_0_c260_12","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"03:05:00","stops":"","days":"ежедневно с 02.06 по 24.09","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-06-02","arrival_platform":""},{"except_days":"","arrival":"06:10:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6158_0_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6158","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-21&uid=SU-6158_0_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Airbus A319","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"03:35:00","stops":"","days":"пн с 21.05 по 24.09","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-21","arrival_platform":""},{"except_days":"","arrival":"07:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1633_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1633","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-06-02&uid=SU-1633_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"05:05:00","stops":"","days":"ежедневно с 02.06 по 19.10","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-06-02","arrival_platform":""},{"except_days":"","arrival":"11:15:00","from":{"code":"s9654608","title":"Симферополь, автостанция-1 «Центральный»","station_type":"bus_station","popular_title":"Автовокзал Центральный","short_title":"","transport_type":"bus","station_type_name":"автовокзал","type":"station"},"thread":{"uid":"empty_1_f9692613t9744862_413","title":"Ялта \u2014 Москва, м. Щёлковская","number":"","short_title":"Ялта \u2014 Москва, м. Щёлковская","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=empty_1_f9692613t9744862_413","carrier":null,"transport_type":"bus","vehicle":null,"transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"06:10:00","stops":"","days":"ежедневно","to":{"code":"s9744862","title":"Москва, м. Щёлковская","station_type":"","popular_title":"м. Щёлковская","short_title":"","transport_type":"bus","station_type_name":"автостанция","type":"station"},"duration":104700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"09:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6152_8_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6152","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-26&uid=SU-6152_8_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"06:30:00","stops":"","days":"сб с 26.05 по 29.09","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-26","arrival_platform":""},{"except_days":"","arrival":"11:15:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"S7-264_0_c260_547","title":"Симферополь \u2014 Москва","number":"S7 264","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-26&uid=S7-264_0_c260_547","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"08:40:00","stops":"","days":"ежедневно с 26.05 по 24.09","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-26","arrival_platform":""},{"except_days":"","arrival":"11:15:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"GH-264_0_c260_12","title":"Симферополь \u2014 Москва","number":"GH 264","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-26&uid=GH-264_0_c260_12","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"08:40:00","stops":"","days":"ежедневно с 26.05 по 24.09","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-26","arrival_platform":""},{"except_days":"","arrival":"12:10:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6154_0_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6154","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-09-11&uid=SU-6154_0_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"09:35:00","stops":"","days":"только 11, 18, 25 сентября","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-09-11","arrival_platform":""},{"except_days":"","arrival":"12:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1637_1_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1637","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-04-30&uid=SU-1637_1_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"09:50:00","stops":"","days":"ежедневно с 30.04 по 27.09","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-04-30","arrival_platform":""},{"except_days":"","arrival":"12:30:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-308_14_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-31&uid=WZ-308_14_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:00:00","stops":"","days":"только 31 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-31","arrival_platform":""},{"except_days":"","arrival":"12:30:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6308_5_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-31&uid=5N-6308_5_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:00:00","stops":"","days":"только 31 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-31","arrival_platform":""},{"except_days":"","arrival":"12:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"S7-270_1_c260_547","title":"Симферополь \u2014 Москва","number":"S7 270","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=S7-270_1_c260_547","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:05:00","stops":"","days":"ежедневно, кроме ср с 25.03 по 21.08","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"12:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"GH-270_0_c260_12","title":"Симферополь \u2014 Москва","number":"GH 270","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=GH-270_0_c260_12","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:05:00","stops":"","days":"ежедневно, кроме ср с 25.03 по 21.08","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"12:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-526_6_c30_547","title":"Симферополь \u2014 Москва","number":"U6 526","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-04&uid=U6-526_6_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus A319","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:10:00","stops":"","days":"только 4 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8100,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-04","arrival_platform":""},{"except_days":"","arrival":"12:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-526_2_c30_547","title":"Симферополь \u2014 Москва","number":"U6 526","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=U6-526_2_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:10:00","stops":"","days":"только 26, 29, 30 декабря, 5, 8 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8100,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"12:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-526_1_c30_547","title":"Симферополь \u2014 Москва","number":"U6 526","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-06&uid=U6-526_1_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:10:00","stops":"","days":"6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 января, \u2026","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8100,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-06","arrival_platform":""},{"except_days":"","arrival":"12:50:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6150_3_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6150","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-21&uid=SU-6150_3_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 777-300","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:15:00","stops":"","days":"пн, вт, чт, сб с 21.05 по 29.09","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-21","arrival_platform":""},{"except_days":"","arrival":"13:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-36_1_c30_547","title":"Симферополь \u2014 Москва","number":"U6 36","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-29&uid=U6-36_1_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:40:00","stops":"","days":"только 29 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-29","arrival_platform":""},{"except_days":"","arrival":"13:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"N4-534_1_c2543_547","title":"Симферополь \u2014 Москва","number":"N4 534","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=N4-534_1_c2543_547","carrier":{"code":2543,"contacts":"Телефон: +7 (495) 730-43-30; факс: +7 (495) 730-25-93.","url":"http://www.nordwindairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordwind_airlines.svg","title":"Nordwind Airlines","phone":"","codes":{"icao":"NWS","sirena":"КЛ","iata":"N4"},"address":"141426, Московская обл., Химкинский р-н, аэропорт \"Шереметьево-1\", а/я 44","logo":null,"email":"nws@nordwindairlines.ru"},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:50:00","stops":"","days":"только 26, 29 декабря, 5 января","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9900,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"13:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"N4-534_0_c2543_547","title":"Симферополь \u2014 Москва","number":"N4 534","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-09&uid=N4-534_0_c2543_547","carrier":{"code":2543,"contacts":"Телефон: +7 (495) 730-43-30; факс: +7 (495) 730-25-93.","url":"http://www.nordwindairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordwind_airlines.svg","title":"Nordwind Airlines","phone":"","codes":{"icao":"NWS","sirena":"КЛ","iata":"N4"},"address":"141426, Московская обл., Химкинский р-н, аэропорт \"Шереметьево-1\", а/я 44","logo":null,"email":"nws@nordwindairlines.ru"},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:50:00","stops":"","days":"вт, пт с 02.01 по 23.03, кроме 05.01","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9900,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-09","arrival_platform":""},{"except_days":"","arrival":"13:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1645_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1645","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=SU-1645_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:50:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"13:20:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-306_2_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-07&uid=WZ-306_2_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:50:00","stops":"","days":"только 7 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-07","arrival_platform":""},{"except_days":"","arrival":"13:20:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6306_8_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-07&uid=5N-6306_8_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"10:50:00","stops":"","days":"только 7 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-07","arrival_platform":""},{"except_days":"","arrival":"13:55:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"S7-262_0_c260_547","title":"Симферополь \u2014 Москва","number":"S7 262","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=S7-262_0_c260_547","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"11:15:00","stops":"","days":"5, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19, 21, 22, 23, 24, 25, 26, 27 января, \u2026","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"13:55:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"GH-262_0_c260_12","title":"Симферополь \u2014 Москва","number":"GH 262","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=GH-262_0_c260_12","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"11:15:00","stops":"","days":"5, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19, 21, 22, 23, 24, 25, 26, 27 января, \u2026","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"14:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1637_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1637","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=SU-1637_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"11:25:00","stops":"","days":"пн, ср, пт, вс по 23.03","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"14:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1637_2_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1637","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-06&uid=SU-1637_2_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"11:25:00","stops":"","days":"вт, чт, сб по 24.03","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-06","arrival_platform":""},{"except_days":"","arrival":"14:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-34_3_c30_547","title":"Симферополь \u2014 Москва","number":"U6 34","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-31&uid=U6-34_3_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"11:40:00","stops":"","days":"только 31 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-31","arrival_platform":""},{"except_days":"","arrival":"14:45:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"N4-124_1_c2543_547","title":"Симферополь \u2014 Москва","number":"N4 124","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=N4-124_1_c2543_547","carrier":{"code":2543,"contacts":"Телефон: +7 (495) 730-43-30; факс: +7 (495) 730-25-93.","url":"http://www.nordwindairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordwind_airlines.svg","title":"Nordwind Airlines","phone":"","codes":{"icao":"NWS","sirena":"КЛ","iata":"N4"},"address":"141426, Московская обл., Химкинский р-н, аэропорт \"Шереметьево-1\", а/я 44","logo":null,"email":"nws@nordwindairlines.ru"},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"11:55:00","stops":"","days":"ежедневно по 24.03, кроме 01.02","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":10200,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"14:45:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"N4-124_0_c2543_547","title":"Симферополь \u2014 Москва","number":"N4 124","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-02-01&uid=N4-124_0_c2543_547","carrier":{"code":2543,"contacts":"Телефон: +7 (495) 730-43-30; факс: +7 (495) 730-25-93.","url":"http://www.nordwindairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordwind_airlines.svg","title":"Nordwind Airlines","phone":"","codes":{"icao":"NWS","sirena":"КЛ","iata":"N4"},"address":"141426, Московская обл., Химкинский р-н, аэропорт \"Шереметьево-1\", а/я 44","logo":null,"email":"nws@nordwindairlines.ru"},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"11:55:00","stops":"","days":"только 1 февраля","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":10200,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-02-01","arrival_platform":""},{"except_days":"","arrival":"14:55:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"N4-924_1_c2543_547","title":"Симферополь \u2014 Москва","number":"N4 924","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-31&uid=N4-924_1_c2543_547","carrier":{"code":2543,"contacts":"Телефон: +7 (495) 730-43-30; факс: +7 (495) 730-25-93.","url":"http://www.nordwindairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordwind_airlines.svg","title":"Nordwind Airlines","phone":"","codes":{"icao":"NWS","sirena":"КЛ","iata":"N4"},"address":"141426, Московская обл., Химкинский р-н, аэропорт \"Шереметьево-1\", а/я 44","logo":null,"email":"nws@nordwindairlines.ru"},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:00:00","stops":"","days":"только 31 декабря","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":10500,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-31","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-2951_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 2951","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-01&uid=SU-2951_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:00:00","stops":"","days":"только 1 января","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-01","arrival_platform":""},{"except_days":"","arrival":"14:30:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-306_7_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-27&uid=WZ-306_7_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:00:00","stops":"","days":"только 27 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-27","arrival_platform":""},{"except_days":"","arrival":"14:30:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6306_4_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-27&uid=5N-6306_4_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:00:00","stops":"","days":"только 27 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-27","arrival_platform":""},{"except_days":"","arrival":"14:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-440_1_c30_547","title":"Симферополь \u2014 Москва","number":"U6 440","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=U6-440_1_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:05:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6308_13_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-28&uid=5N-6308_13_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"ТУ-204","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:10:00","stops":"","days":"только 28 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-28","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-308_4_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-28&uid=WZ-308_4_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"ТУ-204","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:10:00","stops":"","days":"только 28 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-28","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6308_2_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-29&uid=5N-6308_2_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:10:00","stops":"","days":"только 29 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-29","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-308_1_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-29&uid=WZ-308_1_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:10:00","stops":"","days":"только 29 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-29","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-306_5_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=WZ-306_5_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:10:00","stops":"","days":"только 2, 5, 8 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6306_7_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=5N-6306_7_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:10:00","stops":"","days":"только 2, 5, 8 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"15:10:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"N4-934_0_c2543_547","title":"Симферополь \u2014 Москва","number":"N4 934","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-01&uid=N4-934_0_c2543_547","carrier":{"code":2543,"contacts":"Телефон: +7 (495) 730-43-30; факс: +7 (495) 730-25-93.","url":"http://www.nordwindairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordwind_airlines.svg","title":"Nordwind Airlines","phone":"","codes":{"icao":"NWS","sirena":"КЛ","iata":"N4"},"address":"141426, Московская обл., Химкинский р-н, аэропорт \"Шереметьево-1\", а/я 44","logo":null,"email":"nws@nordwindairlines.ru"},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:15:00","stops":"","days":"только 1 января","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":10500,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-01","arrival_platform":""},{"except_days":"","arrival":"14:50:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-308_15_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-13&uid=WZ-308_15_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:15:00","stops":"","days":"только 13 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-13","arrival_platform":""},{"except_days":"","arrival":"14:50:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6308_12_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-13&uid=5N-6308_12_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:15:00","stops":"","days":"только 13 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-13","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-36_3_c30_547","title":"Симферополь \u2014 Москва","number":"U6 36","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-06&uid=U6-36_3_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:15:00","stops":"","days":"только 6, 7 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-06","arrival_platform":""},{"except_days":"","arrival":"14:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-36_4_c30_547","title":"Симферополь \u2014 Москва","number":"U6 36","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-08&uid=U6-36_4_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus A319","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:15:00","stops":"","days":"только 8 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-08","arrival_platform":""},{"except_days":"","arrival":"14:55:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"S7-164_1_c260_547","title":"Симферополь \u2014 Москва","number":"S7 164","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=S7-164_1_c260_547","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:20:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"14:55:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"GH-164_1_c260_12","title":"Симферополь \u2014 Москва","number":"GH 164","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=GH-164_1_c260_12","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:20:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"15:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6308_14_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-04&uid=5N-6308_14_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:35:00","stops":"","days":"только 4 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-04","arrival_platform":""},{"except_days":"","arrival":"15:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-308_11_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-04&uid=WZ-308_11_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:35:00","stops":"","days":"только 4 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-04","arrival_platform":""},{"except_days":"","arrival":"15:30:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"N4-524_2_c2543_547","title":"Симферополь \u2014 Москва","number":"N4 524","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=N4-524_2_c2543_547","carrier":{"code":2543,"contacts":"Телефон: +7 (495) 730-43-30; факс: +7 (495) 730-25-93.","url":"http://www.nordwindairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordwind_airlines.svg","title":"Nordwind Airlines","phone":"","codes":{"icao":"NWS","sirena":"КЛ","iata":"N4"},"address":"141426, Московская обл., Химкинский р-н, аэропорт \"Шереметьево-1\", а/я 44","logo":null,"email":"nws@nordwindairlines.ru"},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:40:00","stops":"","days":"только 29 декабря, 5 января, 8, 9 февраля","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":10200,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"15:30:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"N4-524_0_c2543_547","title":"Симферополь \u2014 Москва","number":"N4 524","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-11&uid=N4-524_0_c2543_547","carrier":{"code":2543,"contacts":"Телефон: +7 (495) 730-43-30; факс: +7 (495) 730-25-93.","url":"http://www.nordwindairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordwind_airlines.svg","title":"Nordwind Airlines","phone":"","codes":{"icao":"NWS","sirena":"КЛ","iata":"N4"},"address":"141426, Московская обл., Химкинский р-н, аэропорт \"Шереметьево-1\", а/я 44","logo":null,"email":"nws@nordwindairlines.ru"},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:40:00","stops":"","days":"только 11, 12, 18, 19, 25, 26 января, 1, 2, 15, 16, 22, 23 февраля, 1, 2, 8, 9, 15, 16, 22, 23 марта","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":10200,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-11","arrival_platform":""},{"except_days":"","arrival":"15:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"S7-164_0_c260_547","title":"Симферополь \u2014 Москва","number":"S7 164","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=S7-164_0_c260_547","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:45:00","stops":"","days":"ежедневно по 24.03","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"15:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"GH-164_0_c260_12","title":"Симферополь \u2014 Москва","number":"GH 164","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=GH-164_0_c260_12","carrier":{"code":260,"contacts":"Телефоны: 8-800-200-000-7 (звонок по России бесплатный)<\/br>\r\n+7 (495) 777-99-99<\/br>\r\ne-mail: globus@s7.ru","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/globus.svg","title":"Глобус","phone":"","codes":{"icao":null,"sirena":"ГЛ","iata":"GH"},"address":"","logo":"//yastatic.net/rasp/media/data/company/logo/globus.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:45:00","stops":"","days":"ежедневно по 24.03","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"15:30:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1621_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1621","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=SU-1621_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:55:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"15:20:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-34_0_c30_547","title":"Симферополь \u2014 Москва","number":"U6 34","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-28&uid=U6-34_0_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"12:55:00","stops":"","days":"только 28 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-28","arrival_platform":""},{"except_days":"","arrival":"15:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6144_1_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6144","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=SU-6144_1_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"13:05:00","stops":"","days":"ежедневно с 25.03 по 19.05","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"15:45:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-2832_3_c30_547","title":"Симферополь \u2014 Москва","number":"U6 2832","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=U6-2832_3_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"13:20:00","stops":"","days":"5, 6, 7, 9, 10, 11, 13, 14, 16, 17, 19, 20, 22, 23, 24, 26, 27, 28, 29, 30 января, \u2026","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"15:45:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-2832_0_c30_547","title":"Симферополь \u2014 Москва","number":"U6 2832","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-08&uid=U6-2832_0_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"13:20:00","stops":"","days":"только 1, 8, 12, 15, 18, 21, 25 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-08","arrival_platform":""},{"except_days":"","arrival":"16:00:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1621_1_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1621","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=SU-1621_1_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"13:25:00","stops":"","days":"ежедневно по 24.03","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"15:50:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-2832_1_c30_547","title":"Симферополь \u2014 Москва","number":"U6 2832","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=U6-2832_1_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"13:25:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"16:15:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6144_6_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6144","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-31&uid=SU-6144_6_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"13:40:00","stops":"","days":"только 31 декабря","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-31","arrival_platform":""},{"except_days":"","arrival":"20:30:00","from":{"code":"s9654608","title":"Симферополь, автостанция-1 «Центральный»","station_type":"bus_station","popular_title":"Автовокзал Центральный","short_title":"","transport_type":"bus","station_type_name":"автовокзал","type":"station"},"thread":{"uid":"empty_1_f9654609t9744862_413","title":"Севастополь \u2014 Москва, м. Щёлковская","number":"","short_title":"Севастополь \u2014 Москва, м. Щёлковская","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=empty_1_f9654609t9744862_413","carrier":null,"transport_type":"bus","vehicle":null,"transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"14:05:00","stops":"","days":"ежедневно","to":{"code":"s9744862","title":"Москва, м. Щёлковская","station_type":"","popular_title":"м. Щёлковская","short_title":"","transport_type":"bus","station_type_name":"автостанция","type":"station"},"duration":109500,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"16:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6144_0_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6144","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-01&uid=SU-6144_0_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"14:05:00","stops":"","days":"только 1 января","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-01","arrival_platform":""},{"except_days":"","arrival":"16:40:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6144_3_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6144","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=SU-6144_3_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"14:05:00","stops":"","days":"ежедневно по 24.03, кроме 31.12, 01.01","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"16:45:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6144_2_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6144","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-21&uid=SU-6144_2_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 777-300","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"14:10:00","stops":"","days":"пн, вт, чт, сб с 21.05 по 29.09","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-21","arrival_platform":""},{"except_days":"","arrival":"16:45:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6144_4_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6144","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-23&uid=SU-6144_4_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 747-400","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"14:10:00","stops":"","days":"ср, пт, вс с 23.05 по 30.09","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-23","arrival_platform":""},{"except_days":"","arrival":"17:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-558_0_c80_547","title":"Симферополь \u2014 Москва","number":"5N 558","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-29&uid=5N-558_0_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-500","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"14:25:00","stops":"","days":"только 29 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-29","arrival_platform":""},{"except_days":"","arrival":"17:10:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-306_3_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-09&uid=WZ-306_3_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"14:40:00","stops":"","days":"только 9 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-09","arrival_platform":""},{"except_days":"","arrival":"17:10:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6306_9_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-09&uid=5N-6306_9_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"14:40:00","stops":"","days":"только 9 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-09","arrival_platform":""},{"except_days":"","arrival":"17:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1787_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1787","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-01&uid=SU-1787_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:00:00","stops":"","days":"только 1 января","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-01","arrival_platform":""},{"except_days":"","arrival":"17:20:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-522_3_c30_547","title":"Симферополь \u2014 Москва","number":"U6 522","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=U6-522_3_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:05:00","stops":"","days":"5, 9, 10, 11, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 25, 27, 30, 31 января, 1, 3 февраля, \u2026","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8100,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"17:20:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-522_2_c30_547","title":"Симферополь \u2014 Москва","number":"U6 522","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-06&uid=U6-522_2_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus A319","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:05:00","stops":"","days":"только 31 декабря, 6 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8100,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-06","arrival_platform":""},{"except_days":"","arrival":"17:20:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-522_5_c30_547","title":"Симферополь \u2014 Москва","number":"U6 522","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-08&uid=U6-522_5_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:05:00","stops":"","days":"только 8, 12, 13, 19, 26, 28, 29 января, 2, 9, 16, 23 февраля, 2, 9, 16, 23 марта","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8100,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-08","arrival_platform":""},{"except_days":"","arrival":"17:30:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"6R-540_6_c156_547","title":"Симферополь \u2014 Москва","number":"6R 540","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-02&uid=6R-540_6_c156_547","carrier":{"code":156,"contacts":"678170, Саха (Якутия), г. Мирный, Аэропорт","url":"http://www.alrosa.aero/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/alrosa_avia.svg","title":"Алроса","phone":"+7 (411) 364-71-70","codes":{"icao":"DRU","sirena":"ЯМ","iata":"6R"},"address":"респ. Саха, г. Мирный, Аэропорт","logo":null,"email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:15:00","stops":"","days":"только 2 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8100,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-02","arrival_platform":""},{"except_days":"","arrival":"17:50:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-308_5_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-27&uid=WZ-308_5_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:20:00","stops":"","days":"только 24, 27 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-27","arrival_platform":""},{"except_days":"","arrival":"17:50:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6308_0_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-27&uid=5N-6308_0_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:20:00","stops":"","days":"только 24, 27 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-27","arrival_platform":""},{"except_days":"","arrival":"17:50:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-308_3_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-10&uid=WZ-308_3_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:20:00","stops":"","days":"10, 11, 12, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 января, \u2026","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-10","arrival_platform":""},{"except_days":"","arrival":"17:50:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6308_1_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6308","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-10&uid=5N-6308_1_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:20:00","stops":"","days":"10, 11, 12, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 января, \u2026","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9000,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-10","arrival_platform":""},{"except_days":"","arrival":"18:15:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1623_2_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1623","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=SU-1623_2_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:35:00","stops":"","days":"ежедневно с 25.03 по 31.05","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"18:15:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1623_3_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1623","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-06-01&uid=SU-1623_3_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"15:35:00","stops":"","days":"ежедневно с 01.06 по 19.10","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-06-01","arrival_platform":""},{"except_days":"","arrival":"19:10:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-6152_7_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6152","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-24&uid=SU-6152_7_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"16:35:00","stops":"","days":"чт с 24.05 по 27.09","to":{"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-05-24","arrival_platform":""},{"except_days":"","arrival":"19:10:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1649_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1649","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-06-01&uid=SU-1649_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"16:35:00","stops":"","days":"ежедневно с 01.06 по 19.10","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-06-01","arrival_platform":""},{"except_days":"","arrival":"19:00:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-522_9_c30_547","title":"Симферополь \u2014 Москва","number":"U6 522","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-07&uid=U6-522_9_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"16:35:00","stops":"","days":"только 7 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-07","arrival_platform":""},{"except_days":"","arrival":"19:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-522_0_c30_547","title":"Симферополь \u2014 Москва","number":"U6 522","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=U6-522_0_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"16:40:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"19:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"S7-166_1_c23_547","title":"Симферополь \u2014 Москва","number":"S7 166","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=S7-166_1_c23_547","carrier":{"code":23,"contacts":"Телефон: 8-800-200-000-7 (звонок по России бесплатный)","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/S7.svg","title":"S7 Airlines","phone":"","codes":{"icao":null,"sirena":"С7","iata":"S7"},"address":"г. Москва, Новая пл., 3/4, Политехнический музей, подъезд 4, м. Китай-город","logo":"//yastatic.net/rasp/media/data/company/logo/logo_2_Yandex.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"16:45:00","stops":"","days":"ежедневно по 24.03","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9600,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"19:15:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-34_1_c30_547","title":"Симферополь \u2014 Москва","number":"U6 34","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-05&uid=U6-34_1_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"16:50:00","stops":"","days":"только 5 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-05","arrival_platform":""},{"except_days":"","arrival":"19:15:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-34_4_c30_547","title":"Симферополь \u2014 Москва","number":"U6 34","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-06&uid=U6-34_4_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"16:50:00","stops":"","days":"только 6 января","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-06","arrival_platform":""},{"except_days":"","arrival":"19:45:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1789_1_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1789","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-01-01&uid=SU-1789_1_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Сухой Суперджет 100","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"17:00:00","stops":"","days":"только 1 января","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9900,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-01-01","arrival_platform":""},{"except_days":"","arrival":"19:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-306_4_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-31&uid=WZ-306_4_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"17:00:00","stops":"","days":"только 31 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-31","arrival_platform":""},{"except_days":"","arrival":"19:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"5N-6306_6_c80_547","title":"Симферополь \u2014 Москва","number":"5N 6306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-31&uid=5N-6306_6_c80_547","carrier":{"code":80,"contacts":"Телефон:(+7 8182) 218800.<br>\r\ne-mail:office@aeroflot-nord.ru","url":"http://www.nordavia.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/nordavia.svg","title":"Нордавиа","phone":"","codes":{"icao":null,"sirena":"5Н","iata":"5N"},"address":"г. Архангельск, Аэропорт Талаги","logo":"//yastatic.net/rasp/media/data/company/logo/nordavia.jpg","email":""},"transport_type":"plane","vehicle":"Airbus А321","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"17:00:00","stops":"","days":"только 31 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-31","arrival_platform":""},{"except_days":"","arrival":"19:35:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"S7-166_0_c23_547","title":"Симферополь \u2014 Москва","number":"S7 166","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=S7-166_0_c23_547","carrier":{"code":23,"contacts":"Телефон: 8-800-200-000-7 (звонок по России бесплатный)","url":"http://www.s7.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/S7.svg","title":"S7 Airlines","phone":"","codes":{"icao":null,"sirena":"С7","iata":"S7"},"address":"г. Москва, Новая пл., 3/4, Политехнический музей, подъезд 4, м. Китай-город","logo":"//yastatic.net/rasp/media/data/company/logo/logo_2_Yandex.gif","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"17:00:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"19:25:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"U6-522_8_c30_547","title":"Симферополь \u2014 Москва","number":"U6 522","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-30&uid=U6-522_8_c30_547","carrier":{"code":30,"contacts":"Круглосуточная служба поддержки пассажиров: <\/br>\r\n+7 (800) 770-02-62 (бесплатно для звонков из России),<\/br>\r\n+7 (499) 920-22-52 (для звонков из других стран).","url":"http://www.uralairlines.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/ural_airlines_1.svg","title":"Уральские авиалинии","phone":"","codes":{"icao":null,"sirena":"У6","iata":"U6"},"address":"г. Екатеринбург, пер. Утренний, 1г","logo":"//yastatic.net/rasp/media/data/company/logo/ural_airlines.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"17:00:00","stops":"","days":"только 30 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":8700,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-30","arrival_platform":""},{"except_days":"","arrival":"19:45:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"SU-1629_0_c26_547","title":"Симферополь \u2014 Москва","number":"SU 1629","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-03-25&uid=SU-1629_0_c26_547","carrier":{"code":26,"contacts":"Центр информации и бронирования: +7 (495) 223-55-55,  +7 (800) 444-55-55. <br>\r\ne-mail: callcenter@aeroflot.ru","url":"http://www.aeroflot.ru/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/Aeroflot_1.svg","title":"Аэрофлот","phone":"","codes":{"icao":"AFL","sirena":"СУ","iata":"SU"},"address":"Москва, Ленинградский пр., д.37, корп.9","logo":"//yastatic.net/rasp/media/data/company/logo/aeroflot_logo_ru.gif","email":""},"transport_type":"plane","vehicle":"Airbus A320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"17:10:00","stops":"","days":"ежедневно с 25.03 по 22.08","to":{"code":"s9600213","title":"Шереметьево","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2018-03-25","arrival_platform":""},{"except_days":"","arrival":"20:05:00","from":{"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"thread":{"uid":"WZ-306_9_c617_547","title":"Симферополь \u2014 Москва","number":"WZ 306","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2017-12-26&uid=WZ-306_9_c617_547","carrier":{"code":617,"contacts":"+7 (495) 212-12-51 - Информационная служба","url":"http://www.flyredwings.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/red_wings_airlines.svg","title":"Red Wings","phone":"+7 (495) 212-12-51","codes":{"icao":"RWZ","sirena":"ИН","iata":"WZ"},"address":"г. Москва, Заводское шоссе, д. 19","logo":"//yastatic.net/rasp/media/data/company/logo/RedWings_logo2.png","email":""},"transport_type":"plane","vehicle":"Airbus А320","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"departure_platform":"","departure":"17:30:00","stops":"","days":"только 23, 24, 25, 26 декабря","to":{"code":"s9600216","title":"Домодедово","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"},"duration":9300,"departure_terminal":null,"arrival_terminal":null,"start_date":"2017-12-26","arrival_platform":""}]
         * search : {"date":null,"to":{"code":"c213","type":"settlement","popular_title":"Москва","short_title":"Москва","title":"Москва"},"from":{"code":"c146","type":"settlement","popular_title":"Симферополь","short_title":"Симферополь","title":"Симферополь"}}
         */

        private Pagination pagination;
        private Search search;
        private List<Segments> intervalSegments;
        private List<Segments> segments;

        public SearchAPI(Pagination pagination, Search search, List<Segments> intervalSegments, List<Segments> segments) {
            this.pagination = pagination;
            this.search = search;
            this.intervalSegments = intervalSegments;
            this.segments = segments;
        }

        public SearchAPI() {
            this.pagination = new Pagination();
            this.search = new Search();
            this.intervalSegments = new ArrayList<>();
            this.segments = new ArrayList<>();
        }

        public Pagination getPagination() {
            return pagination;
        }

        public void setPagination(Pagination pagination) {
            this.pagination = pagination;
        }

        public Search getSearch() {
            return search;
        }

        public void setSearch(Search search) {
            this.search = search;
        }

        public List<Segments> getIntervalSegments() {
            return intervalSegments;
        }

        public void setIntervalSegments(List<Segments> intervalSegments) {
            this.intervalSegments = intervalSegments;
        }

        public List<Segments> getSegments() {
            return segments;
        }

        public void setSegments(List<Segments> segments) {
            this.segments = segments;
        }

        public static class Search {
            /**
             * date : null
             * to : {"code":"c213","type":"settlement","popular_title":"Москва","short_title":"Москва","title":"Москва"}
             * from : {"code":"c146","type":"settlement","popular_title":"Симферополь","short_title":"Симферополь","title":"Симферополь"}
             */

            private String date = null;
            private ToSearch toSearch = new ToSearch();
            private FromSearch fromSearch = new FromSearch();

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public ToSearch getToSearch() {
                return toSearch;
            }

            public void setToSearch(ToSearch toSearch) {
                this.toSearch = toSearch;
            }

            public FromSearch getFromSearch() {
                return fromSearch;
            }

            public void setFromSearch(FromSearch fromSearch) {
                this.fromSearch = fromSearch;
            }

            @Override
            public String toString() {
                return "Search{" +
                        "date='" + date + '\'' +
                        ", toSearch=" + toSearch +
                        ", fromSearch=" + fromSearch +
                        '}';
            }

            public static class ToSearch {
                /**
                 * code : c213
                 * type : settlement
                 * popular_title : Москва
                 * short_title : Москва
                 * title : Москва
                 */

                private String code = null;
                private TYPE type = null;
                private String popularTitle = null;
                private String shortTitle = null;
                private String title = null;

                public String getCode() {
                    return code;
                }

                public void setCode(String code) {
                    this.code = code;
                }

                public TYPE getType() {
                    return type;
                }

                public void setType(TYPE type) {
                    this.type = type;
                }

                public String getPopularTitle() {
                    return popularTitle;
                }

                public void setPopularTitle(String popularTitle) {
                    this.popularTitle = popularTitle;
                }

                public String getShortTitle() {
                    return shortTitle;
                }

                public void setShortTitle(String shortTitle) {
                    this.shortTitle = shortTitle;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                @Override
                public String toString() {
                    return "ToSearch{" +
                            "code='" + code + '\'' +
                            ", type=" + type +
                            ", popularTitle='" + popularTitle + '\'' +
                            ", shortTitle='" + shortTitle + '\'' +
                            ", title='" + title + '\'' +
                            '}';
                }
            }

            public static class FromSearch {
                /**
                 * code : c146
                 * type : settlement
                 * popular_title : Симферополь
                 * short_title : Симферополь
                 * title : Симферополь
                 */

                private String code = null;
                private TYPE type = null;
                private String popularTitle = null;
                private String shortTitle = null;
                private String title = null;

                public String getCode() {
                    return code;
                }

                public void setCode(String code) {
                    this.code = code;
                }

                public TYPE getType() {
                    return type;
                }

                public void setType(TYPE type) {
                    this.type = type;
                }

                public String getPopularTitle() {
                    return popularTitle;
                }

                public void setPopularTitle(String popularTitle) {
                    this.popularTitle = popularTitle;
                }

                public String getShortTitle() {
                    return shortTitle;
                }

                public void setShortTitle(String shortTitle) {
                    this.shortTitle = shortTitle;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                @Override
                public String toString() {
                    return "FromSearch{" +
                            "code='" + code + '\'' +
                            ", type=" + type +
                            ", popularTitle='" + popularTitle + '\'' +
                            ", shortTitle='" + shortTitle + '\'' +
                            ", title='" + title + '\'' +
                            '}';
                }
            }
        }

        public static class Segments {
            /**
             * except_days :
             * arrival : 02:35:00
             * from : {"code":"s9600396","title":"Симферополь","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"}
             * thread : {"uid":"SU-6154_7_c8565_547","title":"Симферополь \u2014 Москва","number":"SU 6154","short_title":"Симферополь \u2014 Москва","thread_method_link":"api.rasp.yandex.net/v3/thread/?date=2018-05-24&uid=SU-6154_7_c8565_547","carrier":{"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""},"transport_type":"plane","vehicle":"Boeing 737-800","transport_subtype":{"color":null,"code":null,"title":null},"express_type":null}
             * departure_platform :
             * departure : 00:01:00
             * stops :
             * days : чт с 24.05 по 27.09
             * to : {"code":"s9600215","title":"Внуково","station_type":"airport","popular_title":"","short_title":"","transport_type":"plane","station_type_name":"аэропорт","type":"station"}
             * duration : 9240
             * departure_terminal : null
             * arrival_terminal : null
             * start_date : 2018-05-24
             * arrival_platform :
             */

            private String exceptDays = null;
            private String arrival = null;
            private From from = new From();
            private Thread thread = new Thread();
            private String departurePlatform = null;
            private String departure = null;
            private String stops = null;
            private String days = null;
            private To to = new To();
            private String duration = null;
            private String departureTerminal = null;
            private String arrivalTerminal = null;
            private String startDate = null;
            private String arrivalPlatform = null;

            //------------------------
            private Integer id;
            private Integer track_max = 0;
            private boolean track_visible ;
            private Integer position = 1;
            private String status;
            private Integer idImg;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getTrack_max() {
                return track_max;
            }

            public void setTrack_max(Integer track_max) {
                this.track_max = track_max;
            }

            public boolean isTrack_visible() {
                return track_visible;
            }

            public void setTrack_visible(boolean track_visible) {
                this.track_visible = track_visible;
            }

            public Integer getPosition() {
                return position;
            }

            public void setPosition(Integer position) {
                this.position = position;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public Integer getIdImg() {
                return idImg;
            }

            public void setIdImg(Integer idImg) {
                this.idImg = idImg;
            }

            @Override
            public String toString() {
                return "Segments{" +
                        "exceptDays='" + exceptDays + '\'' +
                        ", arrival='" + arrival + '\'' +
                        ", from=" + from +
                        ", thread=" + thread +
                        ", departurePlatform='" + departurePlatform + '\'' +
                        ", departure='" + departure + '\'' +
                        ", stops='" + stops + '\'' +
                        ", days='" + days + '\'' +
                        ", to=" + to +
                        ", duration='" + duration + '\'' +
                        ", departureTerminal='" + departureTerminal + '\'' +
                        ", arrivalTerminal='" + arrivalTerminal + '\'' +
                        ", startDate='" + startDate + '\'' +
                        ", arrivalPlatform='" + arrivalPlatform + '\'' +
                        '}';
            }

            public String getExceptDays() {
                return exceptDays;
            }

            public void setExceptDays(String exceptDays) {
                this.exceptDays = exceptDays;
            }

            public String getArrival() {
                return arrival;
            }

            public void setArrival(String arrival) {
                this.arrival = arrival;
            }

            public From getFrom() {
                return from;
            }

            public void setFrom(From from) {
                this.from = from;
            }

            public Thread getThread() {
                return thread;
            }

            public void setThread(Thread thread) {
                this.thread = thread;
            }

            public String getDeparturePlatform() {
                return departurePlatform;
            }

            public void setDeparturePlatform(String departurePlatform) {
                this.departurePlatform = departurePlatform;
            }

            public String getDeparture() {
                return departure;
            }

            public void setDeparture(String departure) {
                this.departure = departure;
            }

            public String getStops() {
                return stops;
            }

            public void setStops(String stops) {
                this.stops = stops;
            }

            public String getDays() {
                return days;
            }

            public void setDays(String days) {
                this.days = days;
            }

            public To getTo() {
                return to;
            }

            public void setTo(To to) {
                this.to = to;
            }

            public String getDuration() {
                return duration;
            }

            public void setDuration(String duration) {
                this.duration = duration;
            }

            public String getDepartureTerminal() {
                return departureTerminal;
            }

            public void setDepartureTerminal(String departureTerminal) {
                this.departureTerminal = departureTerminal;
            }

            public String getArrivalTerminal() {
                return arrivalTerminal;
            }

            public void setArrivalTerminal(String arrivalTerminal) {
                this.arrivalTerminal = arrivalTerminal;
            }

            public String getStartDate() {
                return startDate;
            }

            public void setStartDate(String startDate) {
                this.startDate = startDate;
            }

            public String getArrivalPlatform() {
                return arrivalPlatform;
            }

            public void setArrivalPlatform(String arrivalPlatform) {
                this.arrivalPlatform = arrivalPlatform;
            }

            public static class From {
                /**
                 * code : s9600396
                 * title : Симферополь
                 * station_type : airport
                 * popular_title :
                 * short_title :
                 * transport_type : plane
                 * station_type_name : аэропорт
                 * type : station
                 */

                private String code = null;
                private String title = null;
                private STATION_TYPE stationType = null;
                private String popularTitle = null;
                private String shortTitle = null;
                private TRANSPORT_TYPE transportType = null;
                private String stationTypeName = null;
                private TYPE type = null;

                public String getCode() {
                    return code;
                }

                public void setCode(String code) {
                    this.code = code;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public STATION_TYPE getStationType() {
                    return stationType;
                }

                public void setStationType(STATION_TYPE stationType) {
                    this.stationType = stationType;
                }

                public String getPopularTitle() {
                    return popularTitle;
                }

                public void setPopularTitle(String popularTitle) {
                    this.popularTitle = popularTitle;
                }

                public String getShortTitle() {
                    return shortTitle;
                }

                public void setShortTitle(String shortTitle) {
                    this.shortTitle = shortTitle;
                }

                public TRANSPORT_TYPE getTransportType() {
                    return transportType;
                }

                public void setTransportType(TRANSPORT_TYPE transportType) {
                    this.transportType = transportType;
                }

                public String getStationTypeName() {
                    return stationTypeName;
                }

                public void setStationTypeName(String stationTypeName) {
                    this.stationTypeName = stationTypeName;
                }

                public TYPE getType() {
                    return type;
                }

                public void setType(TYPE type) {
                    this.type = type;
                }

                @Override
                public String toString() {
                    return "From{" +
                            "code='" + code + '\'' +
                            ", title='" + title + '\'' +
                            ", stationType=" + stationType +
                            ", popularTitle='" + popularTitle + '\'' +
                            ", shortTitle='" + shortTitle + '\'' +
                            ", transportType=" + transportType +
                            ", stationTypeName='" + stationTypeName + '\'' +
                            ", type=" + type +
                            '}';
                }
            }

            public static class Thread {
                /**
                 * uid : SU-6154_7_c8565_547
                 * title : Симферополь — Москва
                 * number : SU 6154
                 * short_title : Симферополь — Москва
                 * thread_method_link : api.rasp.yandex.net/v3/thread/?date=2018-05-24&uid=SU-6154_7_c8565_547
                 * carrier : {"code":8565,"contacts":"Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>\r\nПредставительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>\r\n\r\nТелефон call-центра: 647-0-647.","url":"http://www.rossiya-airlines.com/","logo_svg":"//yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg","title":"Россия","phone":"","codes":{"icao":null,"sirena":"ФВ","iata":"SU"},"address":"Санкт-Петербург, ул. Пилотов, д. 18","logo":"//yastatic.net/rasp/media/data/company/logo/logorus_1.jpg","email":""}
                 * transport_type : plane
                 * vehicle : Boeing 737-800
                 * transport_subtype : {"color":null,"code":null,"title":null}
                 * express_type : null
                 */

                private String uid = null;
                private String title = null;
                private String number = null;
                private String shortTitle = null;
                private String threadMethodLink = null;
                private Carrier carrier = new Carrier();//Информация о перевозчике.
                private TRANSPORT_TYPE transportType = null;
                private String vehicle;//Название транспортного средства.
                private TransportSubtype transportSubtype = new TransportSubtype();
                private EXPRESS_TYPE expressType = null;

                public String getUid() {
                    return uid;
                }

                public void setUid(String uid) {
                    this.uid = uid;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getNumber() {
                    return number;
                }

                public void setNumber(String number) {
                    this.number = number;
                }

                public String getShortTitle() {
                    return shortTitle;
                }

                public void setShortTitle(String shortTitle) {
                    this.shortTitle = shortTitle;
                }

                public String getThreadMethodLink() {
                    return threadMethodLink;
                }

                public void setThreadMethodLink(String threadMethodLink) {
                    this.threadMethodLink = threadMethodLink;
                }

                public Carrier getCarrier() {
                    return carrier;
                }

                public void setCarrier(Carrier carrier) {
                    this.carrier = carrier;
                }

                public TRANSPORT_TYPE getTransportType() {
                    return transportType;
                }

                public void setTransportType(TRANSPORT_TYPE transportType) {
                    this.transportType = transportType;
                }

                @Override
                public String toString() {
                    return "Thread{" +
                            "uid='" + uid + '\'' +
                            ", title='" + title + '\'' +
                            ", number='" + number + '\'' +
                            ", shortTitle='" + shortTitle + '\'' +
                            ", threadMethodLink='" + threadMethodLink + '\'' +
                            ", carrier=" + carrier +
                            ", transportType=" + transportType +
                            ", vehicle='" + vehicle + '\'' +
                            ", transportSubtype=" + transportSubtype +
                            ", expressType=" + expressType +
                            '}';
                }

                public String getVehicle() {
                    return vehicle;
                }

                public void setVehicle(String vehicle) {
                    this.vehicle = vehicle;
                }

                public TransportSubtype getTransportSubtype() {
                    return transportSubtype;
                }

                public void setTransportSubtype(TransportSubtype transportSubtype) {
                    this.transportSubtype = transportSubtype;
                }

                public EXPRESS_TYPE getExpressType() {
                    return expressType;
                }

                public void setExpressType(EXPRESS_TYPE expressType) {
                    this.expressType = expressType;
                }

                public static class Carrier {
                    /**
                     * code : 8565
                     * contacts : Офис продаж в Санкт-Петербурге: Невский пр., д. 61 <br>
                     * Представительство в Москве: ул. 2-ая Тверская-Ямская, д.20/22 <br>
                     * <p>
                     * Телефон call-центра: 647-0-647.
                     * url : http://www.rossiya-airlines.com/
                     * logo_svg : //yastatic.net/rasp/media/data/company/svg/R-30x30__opt.svg
                     * title : Россия
                     * phone :
                     * codes : {"icao":null,"sirena":"ФВ","iata":"SU"}
                     * address : Санкт-Петербург, ул. Пилотов, д. 18
                     * logo : //yastatic.net/rasp/media/data/company/logo/logorus_1.jpg
                     * email :
                     */

                    private String code = null;
                    private String contacts = null;
                    private String url = null;
                    private String logo_svg = null;
                    private String title = null;
                    private String phone = null;
                    private Codes codes = new Codes();
                    private String address = null;
                    private String logo = null;
                    private String email = null;

                    public String getCode() {
                        return code;
                    }

                    public void setCode(String code) {
                        this.code = code;
                    }

                    public String getContacts() {
                        return contacts;
                    }

                    public void setContacts(String contacts) {
                        this.contacts = contacts;
                    }

                    public String getUrl() {
                        return url;
                    }

                    public void setUrl(String url) {
                        this.url = url;
                    }

                    public String getLogo_svg() {
                        return logo_svg;
                    }

                    public void setLogo_svg(String logo_svg) {
                        this.logo_svg = logo_svg;
                    }

                    public String getTitle() {
                        return title;
                    }

                    public void setTitle(String title) {
                        this.title = title;
                    }

                    public String getPhone() {
                        return phone;
                    }

                    public void setPhone(String phone) {
                        this.phone = phone;
                    }

                    public Codes getCodes() {
                        return codes;
                    }

                    public void setCodes(Codes codes) {
                        this.codes = codes;
                    }

                    public String getAddress() {
                        return address;
                    }

                    public void setAddress(String address) {
                        this.address = address;
                    }

                    public String getLogo() {
                        return logo;
                    }

                    public void setLogo(String logo) {
                        this.logo = logo;
                    }

                    public String getEmail() {
                        return email;
                    }

                    public void setEmail(String email) {
                        this.email = email;
                    }

                    @Override
                    public String toString() {
                        return "Carrier{" +
                                "code='" + code + '\'' +
                                ", contacts='" + contacts + '\'' +
                                ", url='" + url + '\'' +
                                ", logo_svg='" + logo_svg + '\'' +
                                ", title='" + title + '\'' +
                                ", phone='" + phone + '\'' +
                                ", codes=" + codes +
                                ", address='" + address + '\'' +
                                ", logo='" + logo + '\'' +
                                ", email='" + email + '\'' +
                                '}';
                    }
                }

                public static class TransportSubtype {
                    /**
                     * color : null
                     * code : null
                     * title : null
                     */

                    private String color = null;
                    private String code = null;
                    private String title = null;

                    public String getColor() {
                        return color;
                    }

                    public void setColor(String color) {
                        this.color = color;
                    }

                    public String getCode() {
                        return code;
                    }

                    public void setCode(String code) {
                        this.code = code;
                    }

                    public String getTitle() {
                        return title;
                    }

                    public void setTitle(String title) {
                        this.title = title;
                    }

                    @Override
                    public String toString() {
                        return "TransportSubtype{" +
                                "color='" + color + '\'' +
                                ", code='" + code + '\'' +
                                ", title='" + title + '\'' +
                                '}';
                    }
                }


            }

            public static class To {
                /**
                 * code : s9600215
                 * title : Внуково
                 * station_type : airport
                 * popular_title :
                 * short_title :
                 * transport_type : plane
                 * station_type_name : аэропорт
                 * type : station
                 */

                private String code = null;
                private String title = null;
                private STATION_TYPE stationType = null;
                private String popularTitle = null;
                private String shortTitle = null;
                private TRANSPORT_TYPE transportType = null;
                private String stationTypeName = null;
                private TYPE type = null;

                public String getCode() {
                    return code;
                }

                public TRANSPORT_TYPE getTransportType() {
                    return transportType;
                }

                public TYPE getType() {
                    return type;
                }

                public void setCode(String code) {
                    this.code = code;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public STATION_TYPE getStationType() {
                    return stationType;
                }

                public void setStationType(STATION_TYPE stationType) {
                    this.stationType = stationType;
                }

                public void setTransportType(TRANSPORT_TYPE transportType) {
                    this.transportType = transportType;
                }

                public void setType(TYPE type) {
                    this.type = type;
                }

                public String getPopularTitle() {
                    return popularTitle;
                }

                public void setPopularTitle(String popularTitle) {
                    this.popularTitle = popularTitle;
                }

                public String getShortTitle() {
                    return shortTitle;
                }

                public void setShortTitle(String shortTitle) {
                    this.shortTitle = shortTitle;
                }

                public String getStationTypeName() {
                    return stationTypeName;
                }

                public void setStationTypeName(String stationTypeName) {
                    this.stationTypeName = stationTypeName;
                }

                @Override
                public String toString() {
                    return "To{" +
                            "code='" + code + '\'' +
                            ", title='" + title + '\'' +
                            ", stationType=" + stationType +
                            ", popularTitle='" + popularTitle + '\'' +
                            ", shortTitle='" + shortTitle + '\'' +
                            ", transportType=" + transportType +
                            ", stationTypeName='" + stationTypeName + '\'' +
                            ", type=" + type +
                            '}';
                }
            }
        }
    }

    //Список станций следования
    public static class ThreadAPI {
        /**
         * except_days :
         * arrival_date : null
         * from : null
         * uid : 6991_0_9600786_g18_4
         * title : Рязань-1 — Москва (Казанский вокзал)
         * departure_date : null
         * start_time : 03:38
         * number : 6991
         * short_title : Рязань-1 — М-Казанская
         * days : ежедневно
         * to : null
         * carrier : {"code":153,"offices":[],"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"}
         * transport_type : suburban
         * stops : [{"arrival":null,"departure":"2018-01-06 03:38:00","terminal":null,"platform":"","station":{"code":"s9600786","title":"Рязань-1","station_type":"train_station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"вокзал","type":"station"},"stop_time":null,"duration":0},{"arrival":"2018-01-06 03:45:00","departure":"2018-01-06 03:46:00","terminal":null,"platform":"","station":{"code":"s9601670","title":"Лагерный","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":480},{"arrival":"2018-01-06 03:48:00","departure":"2018-01-06 03:49:00","terminal":null,"platform":"","station":{"code":"s9600713","title":"Дягилево","station_type":"station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"станция","type":"station"},"stop_time":60,"duration":660},{"arrival":"2018-01-06 03:51:00","departure":"2018-01-06 03:52:00","terminal":null,"platform":"","station":{"code":"s9601242","title":"Недостоево","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":840},{"arrival":"2018-01-06 03:53:00","departure":"2018-01-06 03:53:00","terminal":null,"platform":"","station":{"code":"s9633997","title":"189 км","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":900},{"arrival":"2018-01-06 03:54:00","departure":"2018-01-06 03:55:00","terminal":null,"platform":"","station":{"code":"s9601812","title":"187 км","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":1020},{"arrival":"2018-01-06 03:57:00","departure":"2018-01-06 03:58:00","terminal":null,"platform":"","station":{"code":"s9601809","title":"Рязань-Сорт. (Жилые дома)","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":1200},{"arrival":"2018-01-06 03:59:00","departure":"2018-01-06 03:59:00","terminal":null,"platform":"","station":{"code":"s9633649","title":"Депо","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":1260},{"arrival":"2018-01-06 04:02:00","departure":"2018-01-06 04:05:00","terminal":null,"platform":"","station":{"code":"s9600690","title":"Ходынино","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":180,"duration":1620},{"arrival":"2018-01-06 04:09:00","departure":"2018-01-06 04:10:00","terminal":null,"platform":"","station":{"code":"s9600736","title":"Рыбное","station_type":"train_station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"вокзал","type":"station"},"stop_time":60,"duration":1920},{"arrival":"2018-01-06 04:14:00","departure":"2018-01-06 04:15:00","terminal":null,"platform":"","station":{"code":"s9601766","title":"Истодники","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":2220},{"arrival":"2018-01-06 04:22:00","departure":"2018-01-06 04:23:00","terminal":null,"platform":"","station":{"code":"s9600712","title":"Дивово","station_type":"station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"станция","type":"station"},"stop_time":60,"duration":2700},{"arrival":"2018-01-06 04:27:00","departure":"2018-01-06 04:28:00","terminal":null,"platform":"","station":{"code":"s9601011","title":"Слёмы","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":3000},{"arrival":"2018-01-06 04:33:00","departure":"2018-01-06 04:34:00","terminal":null,"platform":"","station":{"code":"s9600710","title":"Алпатьево","station_type":"station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"станция","type":"station"},"stop_time":60,"duration":3360},{"arrival":"2018-01-06 04:40:00","departure":"2018-01-06 04:41:00","terminal":null,"platform":"","station":{"code":"s9600709","title":"Фруктовая","station_type":"station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"станция","type":"station"},"stop_time":60,"duration":3780},{"arrival":"2018-01-06 04:47:00","departure":"2018-01-06 04:48:00","terminal":null,"platform":"","station":{"code":"s9600708","title":"Подлипки","station_type":"station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"станция","type":"station"},"stop_time":60,"duration":4200},{"arrival":"2018-01-06 04:50:00","departure":"2018-01-06 04:51:00","terminal":null,"platform":"","station":{"code":"s9601205","title":"142 км","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":4380},{"arrival":"2018-01-06 04:56:00","departure":"2018-01-06 04:57:00","terminal":null,"platform":"","station":{"code":"s9600726","title":"Луховицы","station_type":"train_station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"вокзал","type":"station"},"stop_time":60,"duration":4740},{"arrival":"2018-01-06 05:07:00","departure":"2018-01-06 05:08:00","terminal":null,"platform":"","station":{"code":"s9601806","title":"Чёрная","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":5400},{"arrival":"2018-01-06 05:14:00","departure":"2018-01-06 05:15:00","terminal":null,"platform":"","station":{"code":"s9601772","title":"Щурово","station_type":"station","popular_title":null,"short_title":null,"transport_type":"train","station_type_name":"станция","type":"station"},"stop_time":60,"duration":5820},{"arrival":"2018-01-06 05:21:00","departure":"2018-01-06 05:22:00","terminal":null,"platform":"1 платф.","station":{"code":"s9600716","title":"Голутвин","station_type":"train_station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"вокзал","type":"station"},"stop_time":60,"duration":6240},{"arrival":"2018-01-06 05:26:00","departure":"2018-01-06 05:27:00","terminal":null,"platform":"","station":{"code":"s9601311","title":"Коломна","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":6540},{"arrival":"2018-01-06 05:28:00","departure":"2018-01-06 05:28:00","terminal":null,"platform":"","station":{"code":"s9601950","title":"113 км","station_type":"platform","popular_title":null,"short_title":null,"transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":6600},{"arrival":"2018-01-06 05:31:00","departure":"2018-01-06 05:32:00","terminal":null,"platform":"","station":{"code":"s9602286","title":"Хорошово","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":6840},{"arrival":"2018-01-06 05:33:00","departure":"2018-01-06 05:33:00","terminal":null,"platform":"","station":{"code":"s9601837","title":"Конев Бор","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":6900},{"arrival":"2018-01-06 05:38:00","departure":"2018-01-06 05:39:00","terminal":null,"platform":"","station":{"code":"s9600707","title":"Пески","station_type":"station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"станция","type":"station"},"stop_time":60,"duration":7260},{"arrival":"2018-01-06 05:43:00","departure":"2018-01-06 05:44:00","terminal":null,"platform":"","station":{"code":"s9601983","title":"Цемгигант","station_type":"platform","popular_title":null,"short_title":null,"transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":7560},{"arrival":"2018-01-06 05:45:00","departure":"2018-01-06 05:46:00","terminal":null,"platform":"","station":{"code":"s9602059","title":"Москворецкая","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":7680},{"arrival":"2018-01-06 05:47:00","departure":"2018-01-06 05:47:00","terminal":null,"platform":"","station":{"code":"s9601654","title":"Шиферная","station_type":"station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"станция","type":"station"},"stop_time":0,"duration":7740},{"arrival":"2018-01-06 05:51:00","departure":"2018-01-06 05:52:00","terminal":null,"platform":"","station":{"code":"s9600705","title":"Воскресенск","station_type":"train_station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"вокзал","type":"station"},"stop_time":60,"duration":8040},{"arrival":"2018-01-06 05:54:00","departure":"2018-01-06 05:55:00","terminal":null,"platform":"","station":{"code":"s9601903","title":"88 км","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":8220},{"arrival":"2018-01-06 05:56:00","departure":"2018-01-06 05:56:00","terminal":null,"platform":"","station":{"code":"s9633647","title":"Трофимово","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":8280},{"arrival":"2018-01-06 06:02:00","departure":"2018-01-06 06:03:00","terminal":null,"platform":"","station":{"code":"s9601634","title":"Конобеево","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":8700},{"arrival":"2018-01-06 06:04:00","departure":"2018-01-06 06:04:00","terminal":null,"platform":"","station":{"code":"s9602075","title":"Виноградово","station_type":"station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"станция","type":"station"},"stop_time":0,"duration":8760},{"arrival":"2018-01-06 06:05:00","departure":"2018-01-06 06:05:00","terminal":null,"platform":"","station":{"code":"s9601194","title":"Золотово","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":8820},{"arrival":"2018-01-06 06:11:00","departure":"2018-01-06 06:12:00","terminal":null,"platform":"","station":{"code":"s9602276","title":"Фаустово","station_type":"station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"станция","type":"station"},"stop_time":60,"duration":9240},{"arrival":"2018-01-06 06:14:00","departure":"2018-01-06 06:15:00","terminal":null,"platform":"","station":{"code":"s9602006","title":"Белоозёрская","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":9420},{"arrival":"2018-01-06 06:16:00","departure":"2018-01-06 06:16:00","terminal":null,"platform":"","station":{"code":"s9601707","title":"63 км","station_type":"platform","popular_title":null,"short_title":null,"transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":9480},{"arrival":"2018-01-06 06:17:00","departure":"2018-01-06 06:17:00","terminal":null,"platform":"","station":{"code":"s9601324","title":"Радуга","station_type":"platform","popular_title":null,"short_title":null,"transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":9540},{"arrival":"2018-01-06 06:20:00","departure":"2018-01-06 06:21:00","terminal":null,"platform":"","station":{"code":"s9601978","title":"Бронницы","station_type":"train_station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"вокзал","type":"station"},"stop_time":60,"duration":9780},{"arrival":"2018-01-06 06:23:00","departure":"2018-01-06 06:24:00","terminal":null,"platform":"","station":{"code":"s9601690","title":"Загорново","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":9960},{"arrival":"2018-01-06 06:27:00","departure":"2018-01-06 06:29:00","terminal":null,"platform":"","station":{"code":"s9600770","title":"Совхоз","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":120,"duration":10260},{"arrival":"2018-01-06 06:33:00","departure":"2018-01-06 06:34:00","terminal":null,"platform":"1 платф.","station":{"code":"s9601197","title":"47 км","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":10560},{"arrival":"2018-01-06 06:36:00","departure":"2018-01-06 06:37:00","terminal":null,"platform":"2 платф.","station":{"code":"s9601841","title":"Раменское","station_type":"train_station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"вокзал","type":"station"},"stop_time":60,"duration":10740},{"arrival":"2018-01-06 06:38:00","departure":"2018-01-06 06:38:00","terminal":null,"platform":"","station":{"code":"s9600961","title":"Фабричная","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":10800},{"arrival":"2018-01-06 06:39:00","departure":"2018-01-06 06:39:00","terminal":null,"platform":"","station":{"code":"s9601504","title":"42 км","station_type":"platform","popular_title":null,"short_title":null,"transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":10860},{"arrival":"2018-01-06 06:40:00","departure":"2018-01-06 06:40:00","terminal":null,"platform":"","station":{"code":"s9601438","title":"Кратово","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":10920},{"arrival":"2018-01-06 06:41:00","departure":"2018-01-06 06:41:00","terminal":null,"platform":"","station":{"code":"s9602223","title":"Отдых","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":10980},{"arrival":"2018-01-06 06:42:00","departure":"2018-01-06 06:42:00","terminal":null,"platform":"","station":{"code":"s9601929","title":"Ильинская","station_type":"platform","popular_title":null,"short_title":null,"transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":11040},{"arrival":"2018-01-06 06:43:00","departure":"2018-01-06 06:43:00","terminal":null,"platform":"","station":{"code":"s9600921","title":"Быково","station_type":"station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"станция","type":"station"},"stop_time":0,"duration":11100},{"arrival":"2018-01-06 06:44:00","departure":"2018-01-06 06:44:00","terminal":null,"platform":"","station":{"code":"s9601915","title":"Удельная","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":11160},{"arrival":"2018-01-06 06:45:00","departure":"2018-01-06 06:45:00","terminal":null,"platform":"","station":{"code":"s9601758","title":"Малаховка","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":11220},{"arrival":"2018-01-06 06:46:00","departure":"2018-01-06 06:46:00","terminal":null,"platform":"","station":{"code":"s9602033","title":"Красково","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":11280},{"arrival":"2018-01-06 06:47:00","departure":"2018-01-06 06:47:00","terminal":null,"platform":"","station":{"code":"s9601822","title":"Томилино","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":11340},{"arrival":"2018-01-06 06:48:00","departure":"2018-01-06 06:48:00","terminal":null,"platform":"","station":{"code":"s9601919","title":"Панки","station_type":"train_station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"вокзал","type":"station"},"stop_time":0,"duration":11400},{"arrival":"2018-01-06 06:49:00","departure":"2018-01-06 06:49:00","terminal":null,"platform":"","station":{"code":"s9601636","title":"Люберцы 1","station_type":"train_station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"вокзал","type":"station"},"stop_time":0,"duration":11460},{"arrival":"2018-01-06 06:50:00","departure":"2018-01-06 06:50:00","terminal":null,"platform":"","station":{"code":"s9601152","title":"Ухтомская","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":11520},{"arrival":"2018-01-06 06:51:00","departure":"2018-01-06 06:51:00","terminal":null,"platform":"","station":{"code":"s9601635","title":"Косино","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":11580},{"arrival":"2018-01-06 07:06:00","departure":"2018-01-06 07:07:00","terminal":null,"platform":"2 платф.","station":{"code":"s9601627","title":"Выхино","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":12540},{"arrival":"2018-01-06 07:08:00","departure":"2018-01-06 07:08:00","terminal":null,"platform":"","station":{"code":"s9601964","title":"Вешняки","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":12600},{"arrival":"2018-01-06 07:09:00","departure":"2018-01-06 07:09:00","terminal":null,"platform":"","station":{"code":"s9601186","title":"Плющево","station_type":"platform","popular_title":"","short_title":null,"transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":12660},{"arrival":"2018-01-06 07:11:00","departure":"2018-01-06 07:12:00","terminal":null,"platform":"2 платф.","station":{"code":"s9600931","title":"Перово","station_type":"station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"станция","type":"station"},"stop_time":60,"duration":12840},{"arrival":"2018-01-06 07:13:00","departure":"2018-01-06 07:14:00","terminal":null,"platform":"2 платф.","station":{"code":"s9601991","title":"Фрезер","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":12960},{"arrival":"2018-01-06 07:16:00","departure":"2018-01-06 07:17:00","terminal":null,"platform":"2 платф.","station":{"code":"s9601642","title":"Новая","station_type":"platform","popular_title":"","short_title":"","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":13140},{"arrival":"2018-01-06 07:19:00","departure":"2018-01-06 07:20:00","terminal":null,"platform":"2 платф.","station":{"code":"s9601335","title":"Сортировочная","station_type":"platform","popular_title":"","short_title":null,"transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":60,"duration":13320},{"arrival":"2018-01-06 07:21:00","departure":"2018-01-06 07:21:00","terminal":null,"platform":"","station":{"code":"s9601647","title":"Электрозаводская","station_type":"platform","popular_title":"","short_title":"Эл-заводская","transport_type":"train","station_type_name":"платформа","type":"station"},"stop_time":0,"duration":13380},{"arrival":"2018-01-06 07:28:00","departure":null,"terminal":null,"platform":"","station":{"code":"s2000003","title":"Москва (Казанский вокзал)","station_type":"train_station","popular_title":"Казанский вокзал","short_title":"М-Казанская","transport_type":"train","station_type_name":"вокзал","type":"station"},"stop_time":null,"duration":13800}]
         * vehicle : null
         * start_date : 2018-01-06
         * transport_subtype : {"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"}
         * express_type : null
         */

        private String exceptDays;
        private String arrivalDate;
        private String from;
        private String uid;
        private String title;
        private String departureDate;
        private String startTime;
        private String number;
        private String shortTitle;
        private String days;
        private String to;
        private Carrier carrier;
        private TRANSPORT_TYPE transportType;
        private List<Stops> stops = null;
        private String vehicle;
        private String startDate;
        private TransportSubtype transportSubtype;
        private EXPRESS_TYPE expressType;

        public String getExceptDays() {
            return exceptDays;
        }

        public void setExceptDays(String exceptDays) {
            this.exceptDays = exceptDays;
        }

        public String getArrivalDate() {
            return arrivalDate;
        }

        public void setArrivalDate(String arrivalDate) {
            this.arrivalDate = arrivalDate;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDepartureDate() {
            return departureDate;
        }

        public void setDepartureDate(String departureDate) {
            this.departureDate = departureDate;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getShortTitle() {
            return shortTitle;
        }

        public void setShortTitle(String shortTitle) {
            this.shortTitle = shortTitle;
        }

        public String getDays() {
            return days;
        }

        public void setDays(String days) {
            this.days = days;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public Carrier getCarrier() {
            return carrier;
        }

        public void setCarrier(Carrier carrier) {
            this.carrier = carrier;
        }

        public TRANSPORT_TYPE getTransportType() {
            return transportType;
        }

        public void setTransportType(TRANSPORT_TYPE transportType) {
            this.transportType = transportType;
        }

        public List<Stops> getStops() {
            return stops;
        }

        public void setStops(List<Stops> stops) {
            this.stops = stops;
        }

        public String getVehicle() {
            return vehicle;
        }

        public void setVehicle(String vehicle) {
            this.vehicle = vehicle;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public TransportSubtype getTransportSubtype() {
            return transportSubtype;
        }

        public void setTransportSubtype(TransportSubtype transportSubtype) {
            this.transportSubtype = transportSubtype;
        }

        public EXPRESS_TYPE getExpressType() {
            return expressType;
        }

        public void setExpressType(EXPRESS_TYPE expressType) {
            this.expressType = expressType;
        }

        @Override
        public String toString() {
            return "ThreadAPI{" +
                    "exceptDays='" + exceptDays + '\'' +
                    ", arrivalDate=" + arrivalDate +
                    ", from=" + from +
                    ", uid='" + uid + '\'' +
                    ", title='" + title + '\'' +
                    ", departureDate=" + departureDate +
                    ", startTime='" + startTime + '\'' +
                    ", number='" + number + '\'' +
                    ", shortTitle='" + shortTitle + '\'' +
                    ", days='" + days + '\'' +
                    ", to=" + to +
                    ", carrier=" + carrier +
                    ", transportType='" + transportType + '\'' +
                    ", stops=" + stops +
                    ", vehicle=" + vehicle +
                    ", startDate='" + startDate + '\'' +
                    ", transportSubtype=" + transportSubtype +
                    ", expressType=" + expressType +
                    '}';
        }

        public static class Carrier {
            /**
             * code : 153
             * offices : []
             * codes : {"icao":null,"sirena":null,"iata":null}
             * title : Центральная пригородная пассажирская компания
             */

            private String code;
            private Codes codes;
            private String title;
            private List<?> offices;

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public Codes getCodes() {
                return codes;
            }

            public void setCodes(Codes codes) {
                this.codes = codes;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public List<?> getOffices() {
                return offices;
            }

            public void setOffices(List<?> offices) {
                this.offices = offices;
            }
        }

        public static class Stops {
            /**
             * arrival : null
             * departure : 2018-01-06 03:38:00
             * terminal : null
             * platform :
             * station : {"code":"s9600786","title":"Рязань-1","station_type":"train_station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"вокзал","type":"station"}
             * stop_time : null
             * duration : 0
             */

            private String arrival;
            private String departure;
            private String terminal;
            private String platform;
            private Station station;
            private String stopTime;
            private String duration;

            public String getArrival() {
                return arrival;
            }

            public void setArrival(String arrival) {
                this.arrival = arrival;
            }

            public String getDeparture() {
                return departure;
            }

            public void setDeparture(String departure) {
                this.departure = departure;
            }

            public String getTerminal() {
                return terminal;
            }

            public void setTerminal(String terminal) {
                this.terminal = terminal;
            }

            public String getPlatform() {
                return platform;
            }

            public void setPlatform(String platform) {
                this.platform = platform;
            }

            public Station getStation() {
                return station;
            }

            public void setStation(Station station) {
                this.station = station;
            }

            public String getStopTime() {
                return stopTime;
            }

            public void setStopTime(String stopTime) {
                this.stopTime = stopTime;
            }

            public String getDuration() {
                return duration;
            }

            public void setDuration(String duration) {
                this.duration = duration;
            }
        }
    }

    //Расписание рейсов по станции
    public static class ScheduleAPI {
        /**
         * pagination : {"total":26,"limit":100,"offset":0}
         * schedule : [{"except_days":null,"arrival":"04:56","thread":{"uid":"6991_0_9600786_g18_4","title":"Рязань-1 \u2014 Москва (Казанский вокзал)","number":"6991","short_title":"Рязань-1 \u2014 М-Казанская","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"},"express_type":null},"is_fuzzy":false,"days":"ежедневно","stops":"до 88 км везде, кроме: 113 км, Конев Бор, Шиферная, далее Конобеево, Фаустово, Белоозёрская, Бронницы, Загорново, Совхоз, 47 км, Раменское, Выхино, Перово, Фрезер, Новая, Сортировочная, Москва (Казанский вокзал)","departure":"04:57","terminal":null,"platform":""},{"except_days":null,"arrival":"06:34","thread":{"uid":"6993_0_9600786_g18_4","title":"Рязань-1 \u2014 Москва (Казанский вокзал)","number":"6993","short_title":"Рязань-1 \u2014 М-Казанская","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"},"express_type":null},"is_fuzzy":false,"days":"ежедневно","stops":"до 88 км везде, кроме: 113 км, Хорошово, Конев Бор, Шиферная, далее Конобеево, Фаустово, Белоозёрская, Бронницы, Загорново, 47 км, Раменское, Люберцы 1, Выхино, Фрезер, Новая, Москва (Казанский вокзал)","departure":"06:35","terminal":null,"platform":""},{"except_days":null,"arrival":"06:47","thread":{"uid":"6010_0_9600716_g18_4","title":"Голутвин \u2014 Рязань-1","number":"6010","short_title":"Голутвин \u2014 Рязань-1","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"},"express_type":null},"is_fuzzy":false,"days":"ежедневно","stops":"кроме: 179 км, 189 км","departure":"06:48","terminal":null,"platform":""},{"except_days":null,"arrival":"07:31","thread":{"uid":"6003_0_9600786_g18_4","title":"Рязань-1 \u2014 Голутвин","number":"6003","short_title":"Рязань-1 \u2014 Голутвин","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"},"express_type":null},"is_fuzzy":false,"days":"ежедневно","stops":"везде","departure":"07:32","terminal":null,"platform":""},{"except_days":null,"arrival":"08:31","thread":{"uid":"6014_0_9600716_g18_4","title":"Голутвин \u2014 Рязань-1","number":"6014","short_title":"Голутвин \u2014 Рязань-1","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"},"express_type":null},"is_fuzzy":false,"days":"ежедневно","stops":"кроме: 179 км, 189 км","departure":"08:32","terminal":null,"platform":""},{"except_days":null,"arrival":"08:36","thread":{"uid":"7041_0_9600786_g18_4","title":"Рязань-1 \u2014 Москва (Казанский вокзал)","number":"7041","short_title":"Рязань-1 \u2014 М-Казанская","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"exprdal","title":"экспресс<br/>(билеты&nbsp;c указанием&nbsp;мест)"},"express_type":"express"},"is_fuzzy":false,"days":"ежедневно","stops":"Голутвин, 88 км, Москва (Казанский вокзал)","departure":"08:37","terminal":null,"platform":""},{"except_days":null,"arrival":"09:09","thread":{"uid":"7040_0_2000003_g18_4","title":"Москва (Казанский вокзал) \u2014 Рязань-1","number":"7040","short_title":"М-Казанская \u2014 Рязань-1","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"exprdal","title":"экспресс<br/>(билеты&nbsp;c указанием&nbsp;мест)"},"express_type":"express"},"is_fuzzy":false,"days":"ежедневно","stops":"Фруктовая, Рыбное, Рязань-1","departure":"09:10","terminal":null,"platform":""},{"except_days":null,"arrival":"11:01","thread":{"uid":"6992_0_2000003_g18_4","title":"Москва (Казанский вокзал) \u2014 Рязань-1","number":"6992","short_title":"М-Казанская \u2014 Рязань-1","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"},"express_type":null},"is_fuzzy":false,"days":"ежедневно","stops":"кроме: 179 км, Депо, 189 км","departure":"11:02","terminal":null,"platform":""},{"except_days":null,"arrival":"11:30","thread":{"uid":"884M_0_2","title":"Москва \u2014 Рязань","number":"884М","short_title":"Москва \u2014 Рязань","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"train","vehicle":null,"transport_subtype":{"color":null,"code":null,"title":null},"express_type":null},"is_fuzzy":false,"days":"28 апреля, 5, 12, 19, 26 мая, 2, 9, 16, 23, 30 июня, 7, 14, 21, 28 июля, 4, 11, 18, 25 августа, 2, 9 сентября, \u2026","stops":"","departure":"11:32","terminal":null,"platform":""},{"except_days":null,"arrival":"12:39","thread":{"uid":"7043_0_9600786_g18_4","title":"Рязань-1 \u2014 Москва (Казанский вокзал)","number":"7043","short_title":"Рязань-1 \u2014 М-Казанская","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"exprdal","title":"экспресс<br/>(билеты&nbsp;c указанием&nbsp;мест)"},"express_type":"express"},"is_fuzzy":false,"days":"ежедневно","stops":"Голутвин, Москва (Казанский вокзал)","departure":"12:40","terminal":null,"platform":""},{"except_days":null,"arrival":"14:32","thread":{"uid":"7042_0_2000003_g18_4","title":"Москва (Казанский вокзал) \u2014 Рязань-1","number":"7042","short_title":"М-Казанская \u2014 Рязань-1","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"exprdal","title":"экспресс<br/>(билеты&nbsp;c указанием&nbsp;мест)"},"express_type":"express"},"is_fuzzy":false,"days":"ежедневно","stops":"Фруктовая, Рыбное, Рязань-1","departure":"14:33","terminal":null,"platform":""},{"except_days":null,"arrival":"15:04","thread":{"uid":"7085_0_9600786_g18_4","title":"Рязань-1 \u2014 Москва (Казанский вокзал)","number":"7085","short_title":"Рязань-1 \u2014 М-Казанская","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"exprdal","title":"экспресс<br/>(билеты&nbsp;c указанием&nbsp;мест)"},"express_type":"express"},"is_fuzzy":false,"days":"только 8 января","stops":"Голутвин, Москва (Казанский вокзал)","departure":"15:05","terminal":null,"platform":""},{"except_days":null,"arrival":"15:36","thread":{"uid":"6995_0_9600746_g18_4","title":"Рязань-2 \u2014 Москва (Казанский вокзал)","number":"6995","short_title":"Рязань-2 \u2014 М-Казанская","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"},"express_type":null},"is_fuzzy":false,"days":"ежедневно","stops":"до Белоозёрская везде, кроме: 113 км, Конев Бор, Цемгигант, Москворецкая, Шиферная, Трофимово, Золотово, далее Бронницы, Загорново, Совхоз, 47 км, Раменское, Люберцы 1, Выхино, Перово, Фрезер, Новая, Москва (Казанский вокзал)","departure":"15:37","terminal":null,"platform":""},{"except_days":null,"arrival":"16:29","thread":{"uid":"7051_0_9600746_g18_4","title":"Рязань-2 \u2014 Москва (Казанский вокзал)","number":"7051","short_title":"Рязань-2 \u2014 М-Казанская","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"exprdal","title":"экспресс<br/>(билеты&nbsp;c указанием&nbsp;мест)"},"express_type":"express"},"is_fuzzy":false,"days":"вс","stops":"Голутвин, Москва (Казанский вокзал)","departure":"16:30","terminal":null,"platform":""},{"except_days":null,"arrival":"16:29","thread":{"uid":"7051_1_9600746_g18_4","title":"Рязань-2 \u2014 Москва (Казанский вокзал)","number":"7051","short_title":"Рязань-2 \u2014 М-Казанская","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"exprdal","title":"экспресс<br/>(билеты&nbsp;c указанием&nbsp;мест)"},"express_type":"express"},"is_fuzzy":false,"days":"только 8 января","stops":"Голутвин, Москва (Казанский вокзал)","departure":"16:30","terminal":null,"platform":""},{"except_days":null,"arrival":"17:31","thread":{"uid":"7045_0_9600786_g18_4","title":"Рязань-1 \u2014 Москва (Казанский вокзал)","number":"7045","short_title":"Рязань-1 \u2014 М-Казанская","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"exprdal","title":"экспресс<br/>(билеты&nbsp;c указанием&nbsp;мест)"},"express_type":"express"},"is_fuzzy":false,"days":"ежедневно","stops":"Голутвин, 88 км, Москва (Казанский вокзал)","departure":"17:32","terminal":null,"platform":""},{"except_days":null,"arrival":"17:43","thread":{"uid":"6996_0_2000003_g18_4","title":"Москва (Казанский вокзал) \u2014 Рязань-1","number":"6996","short_title":"М-Казанская \u2014 Рязань-1","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"},"express_type":null},"is_fuzzy":false,"days":"ежедневно","stops":"кроме: 179 км, Депо, 189 км","departure":"17:44","terminal":null,"platform":""},{"except_days":null,"arrival":"18:17","thread":{"uid":"6997_0_9600786_g18_4","title":"Рязань-1 \u2014 Москва (Казанский вокзал)","number":"6997","short_title":"Рязань-1 \u2014 М-Казанская","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"},"express_type":null},"is_fuzzy":false,"days":"ежедневно","stops":"Чёрная, Щурово, Голутвин, Коломна, Пески, Воскресенск, 88 км, Конобеево, Виноградово, Фаустово, Белоозёрская, Бронницы, Раменское, Фабричная, Отдых, Быково, Малаховка, Томилино, Люберцы 1, Выхино, Перово, далее везде","departure":"18:18","terminal":null,"platform":""},{"except_days":"5 января","arrival":"19:32","thread":{"uid":"7050_0_2000003_g18_4","title":"Москва (Казанский вокзал) \u2014 Рязань-2","number":"7050","short_title":"М-Казанская \u2014 Рязань-2","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"exprdal","title":"экспресс<br/>(билеты&nbsp;c указанием&nbsp;мест)"},"express_type":"express"},"is_fuzzy":false,"days":"пт","stops":"Фруктовая, Рыбное, Рязань-2","departure":"19:34","terminal":null,"platform":""},{"except_days":null,"arrival":"19:48","thread":{"uid":"6013_0_9600786_g18_4","title":"Рязань-1 \u2014 Голутвин","number":"6013","short_title":"Рязань-1 \u2014 Голутвин","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"},"express_type":null},"is_fuzzy":false,"days":"ежедневно","stops":"везде","departure":"19:49","terminal":null,"platform":""},{"except_days":null,"arrival":"20:08","thread":{"uid":"7044_0_2000003_g18_4","title":"Москва (Казанский вокзал) \u2014 Рязань-1","number":"7044","short_title":"М-Казанская \u2014 Рязань-1","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"exprdal","title":"экспресс<br/>(билеты&nbsp;c указанием&nbsp;мест)"},"express_type":"express"},"is_fuzzy":false,"days":"ежедневно","stops":"Фруктовая, Рыбное, Рязань-1","departure":"20:09","terminal":null,"platform":""},{"except_days":null,"arrival":"20:46","thread":{"uid":"6028_0_9600716_g18_4","title":"Голутвин \u2014 Рязань-1","number":"6028","short_title":"Голутвин \u2014 Рязань-1","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"},"express_type":null},"is_fuzzy":false,"days":"ежедневно","stops":"кроме: 179 км","departure":"20:47","terminal":null,"platform":""},{"except_days":"5 января","arrival":"21:01","thread":{"uid":"7098_0_2000003_g18_4","title":"Москва (Казанский вокзал) \u2014 Рязань-2","number":"7098","short_title":"М-Казанская \u2014 Рязань-2","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"exprdal","title":"экспресс<br/>(билеты&nbsp;c указанием&nbsp;мест)"},"express_type":"express"},"is_fuzzy":false,"days":"пт","stops":"Фруктовая, Рыбное, Рязань-2","departure":"21:03","terminal":null,"platform":""},{"except_days":null,"arrival":"21:28","thread":{"uid":"6019_0_9600786_g18_4","title":"Рязань-1 \u2014 Голутвин","number":"6019","short_title":"Рязань-1 \u2014 Голутвин","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"},"express_type":null},"is_fuzzy":false,"days":"ежедневно","stops":"везде","departure":"21:29","terminal":null,"platform":""},{"except_days":null,"arrival":"21:29","thread":{"uid":"6998_0_2000003_g18_4","title":"Москва (Казанский вокзал) \u2014 Рязань-1","number":"6998","short_title":"М-Казанская \u2014 Рязань-1","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"},"express_type":null},"is_fuzzy":false,"days":"ежедневно по 9 января","stops":"кроме: 179 км, Депо, 189 км","departure":"21:30","terminal":null,"platform":""},{"except_days":null,"arrival":"21:29","thread":{"uid":"6998_1_2000003_g18_4","title":"Москва (Казанский вокзал) \u2014 Рязань-1","number":"6998","short_title":"М-Казанская \u2014 Рязань-1","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"},"express_type":null},"is_fuzzy":false,"days":"ежедневно с 10 января","stops":"кроме: 179 км, Депо, 189 км","departure":"21:30","terminal":null,"platform":""}]
         * station : {"code":"s9600726","title":"Луховицы","station_type":"train_station","popular_title":"","short_title":"","transport_type":"train","station_type_name":"вокзал","type":"station"}
         * date : null
         * interval_schedule : []
         * event : departure
         */

        private Pagination pagination;
        private List<Schedule> schedule = null;
        private Station station;
        private String date;
        private List<Object> intervalSchedule = null;
        private String event;

        public Pagination getPagination() {
            return pagination;
        }

        public void setPagination(Pagination pagination) {
            this.pagination = pagination;
        }

        public List<Schedule> getSchedule() {
            return schedule;
        }

        public void setSchedule(List<Schedule> schedule) {
            this.schedule = schedule;
        }

        public Station getStation() {
            return station;
        }

        public void setStation(Station station) {
            this.station = station;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public List<Object> getIntervalSchedule() {
            return intervalSchedule;
        }

        public void setIntervalSchedule(List<Object> intervalSchedule) {
            this.intervalSchedule = intervalSchedule;
        }

        public String getEvent() {
            return event;
        }

        public void setEvent(String event) {
            this.event = event;
        }

        public static class Schedule {
            /**
             * except_days : null
             * arrival : 04:56
             * thread : {"uid":"6991_0_9600786_g18_4","title":"Рязань-1 \u2014 Москва (Казанский вокзал)","number":"6991","short_title":"Рязань-1 \u2014 М-Казанская","carrier":{"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"},"transport_type":"suburban","vehicle":null,"transport_subtype":{"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"},"express_type":null}
             * is_fuzzy : false
             * days : ежедневно
             * stops : до 88 км везде, кроме: 113 км, Конев Бор, Шиферная, далее Конобеево, Фаустово, Белоозёрская, Бронницы, Загорново, Совхоз, 47 км, Раменское, Выхино, Перово, Фрезер, Новая, Сортировочная, Москва (Казанский вокзал)
             * departure : 04:57
             * terminal : null
             * platform :
             */

            private String exceptDays;
            private String arrival;
            private Thread thread;
            private Boolean isFuzzy;
            private String days;
            private String stops;
            private String departure;
            private String terminal;
            private String platform;


            public String getArrival() {
                return arrival;
            }

            public void setArrival(String arrival) {
                this.arrival = arrival;
            }

            public Thread getThread() {
                return thread;
            }

            public void setThread(Thread thread) {
                this.thread = thread;
            }

            public String getExceptDays() {
                return exceptDays;
            }

            public void setExceptDays(String exceptDays) {
                this.exceptDays = exceptDays;
            }

            public Boolean getFuzzy() {
                return isFuzzy;
            }

            public void setFuzzy(Boolean fuzzy) {
                isFuzzy = fuzzy;
            }

            public String getDays() {
                return days;
            }

            public void setDays(String days) {
                this.days = days;
            }

            public String getStops() {
                return stops;
            }

            public void setStops(String stops) {
                this.stops = stops;
            }

            public String getDeparture() {
                return departure;
            }

            public void setDeparture(String departure) {
                this.departure = departure;
            }

            public String getTerminal() {
                return terminal;
            }

            public void setTerminal(String terminal) {
                this.terminal = terminal;
            }

            public String getPlatform() {
                return platform;
            }

            public void setPlatform(String platform) {
                this.platform = platform;
            }

            @Override
            public String toString() {
                return "Schedule{" +
                        "exceptDays=" + exceptDays +
                        ", arrival='" + arrival + '\'' +
                        ", thread=" + thread +
                        ", isFuzzy=" + isFuzzy +
                        ", days='" + days + '\'' +
                        ", stops='" + stops + '\'' +
                        ", departure='" + departure + '\'' +
                        ", terminal=" + terminal +
                        ", platform='" + platform + '\'' +
                        '}';
            }

            public static class Thread {
                /**
                 * uid : 6991_0_9600786_g18_4
                 * title : Рязань-1 — Москва (Казанский вокзал)
                 * number : 6991
                 * short_title : Рязань-1 — М-Казанская
                 * carrier : {"code":153,"codes":{"icao":null,"sirena":null,"iata":null},"title":"Центральная пригородная пассажирская компания"}
                 * transport_type : suburban
                 * vehicle : null
                 * transport_subtype : {"color":"#FF7F44","code":"suburban","title":"Пригородный поезд"}
                 * express_type : null
                 */

                private String uid;
                private String title;
                private String number;
                private String shortTitle;
                private Carrier carrier;
                private TRANSPORT_TYPE transportType;
                private String vehicle;
                private TransportSubtype transportSubtype;
                private EXPRESS_TYPE expressType;

                public String getUid() {
                    return uid;
                }

                public void setUid(String uid) {
                    this.uid = uid;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getNumber() {
                    return number;
                }

                public void setNumber(String number) {
                    this.number = number;
                }


                public Carrier getCarrier() {
                    return carrier;
                }

                public void setCarrier(Carrier carrier) {
                    this.carrier = carrier;
                }

                public String getVehicle() {
                    return vehicle;
                }

                public void setVehicle(String vehicle) {
                    this.vehicle = vehicle;
                }

                public String getShortTitle() {
                    return shortTitle;
                }

                public void setShortTitle(String shortTitle) {
                    this.shortTitle = shortTitle;
                }

                public TRANSPORT_TYPE getTransportType() {
                    return transportType;
                }

                public void setTransportType(TRANSPORT_TYPE transportType) {
                    this.transportType = transportType;
                }

                public TransportSubtype getTransportSubtype() {
                    return transportSubtype;
                }

                public void setTransportSubtype(TransportSubtype transportSubtype) {
                    this.transportSubtype = transportSubtype;
                }

                public EXPRESS_TYPE getExpressType() {
                    return expressType;
                }

                @Override
                public String toString() {
                    return "Thread{" +
                            "uid='" + uid + '\'' +
                            ", title='" + title + '\'' +
                            ", number='" + number + '\'' +
                            ", shortTitle='" + shortTitle + '\'' +
                            ", carrier=" + carrier +
                            ", transportType='" + transportType + '\'' +
                            ", vehicle=" + vehicle +
                            ", transportSubtype=" + transportSubtype +
                            ", expressType=" + expressType +
                            '}';
                }

                public void setExpressType(EXPRESS_TYPE expressType) {
                    this.expressType = expressType;
                }

                public static class Carrier {
                    @Override
                    public String toString() {
                        return "Carrier{" +
                                "code=" + code +
                                ", codes=" + codes +
                                ", title='" + title + '\'' +
                                '}';
                    }

                    /**
                     * code : 153
                     * codes : {"icao":null,"sirena":null,"iata":null}
                     * title : Центральная пригородная пассажирская компания
                     */

                    private String code;
                    private Codes codes;
                    private String title;

                    public String getCode() {
                        return code;
                    }

                    public void setCode(String code) {
                        this.code = code;
                    }

                    public Codes getCodes() {
                        return codes;
                    }

                    public void setCodes(Codes codes) {
                        this.codes = codes;
                    }

                    public String getTitle() {
                        return title;
                    }

                    public void setTitle(String title) {
                        this.title = title;
                    }
                }
            }
        }
    }

    //-------------------------------вспомогательные классы-------------------------------------------//
    public static class TransportSubtype {
        /**
         * color : #FF7F44
         * code : suburban
         * title : Пригородный поезд
         */

        private String color;
        private String code;
        private String title;

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        @Override
        public String toString() {
            return "TransportSubtype{" +
                    "color='" + color + '\'' +
                    ", code='" + code + '\'' +
                    ", title='" + title + '\'' +
                    '}';
        }
    }

    public static class Codes {
        /**
         * icao : null
         * sirena : ФВ
         * iata : SU
         */

        private String icao;
        private String sirena;
        private String iata;

        public String getIcao() {
            return icao;
        }

        public void setIcao(String icao) {
            this.icao = icao;
        }

        public String getSirena() {
            return sirena;
        }

        public void setSirena(String sirena) {
            this.sirena = sirena;
        }

        public String getIata() {
            return iata;
        }

        public void setIata(String iata) {
            this.iata = iata;
        }
    }

    public static class Pagination {
        @Override
        public String toString() {
            return "Pagination{" +
                    "total=" + total +
                    ", limit=" + limit +
                    ", offset=" + offset +
                    '}';
        }

        /**
         * total : 26
         * limit : 100
         * offset : 0
         */

        private String total;
        private String limit;
        private String offset;

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getLimit() {
            return limit;
        }

        public void setLimit(String limit) {
            this.limit = limit;
        }

        public String getOffset() {
            return offset;
        }

        public void setOffset(String offset) {
            this.offset = offset;
        }
    }

    public static class Station {
        /**
         * code : s9600726
         * title : Луховицы
         * station_type : train_station
         * popular_title :
         * short_title :
         * transport_type : train
         * station_type_name : вокзал
         * type : station
         */

        private String code;
        private String title;
        private STATION_TYPE stationType;
        private String popularTitle;
        private String shortTitle;
        private TRANSPORT_TYPE transportType;
        private String stationTypeName;
        private TYPE type;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getTitle() {
            return title;
        }

        @Override
        public String toString() {
            return "Station{" +
                    "code='" + code + '\'' +
                    ", title='" + title + '\'' +
                    ", stationType='" + stationType + '\'' +
                    ", popularTitle='" + popularTitle + '\'' +
                    ", shortTitle='" + shortTitle + '\'' +
                    ", transportType='" + transportType + '\'' +
                    ", stationTypeName='" + stationTypeName + '\'' +
                    ", type='" + type + '\'' +
                    '}';
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public STATION_TYPE getStationType() {
            return stationType;
        }

        public void setStationType(STATION_TYPE stationType) {
            this.stationType = stationType;
        }

        public String getPopularTitle() {
            return popularTitle;
        }

        public void setPopularTitle(String popularTitle) {
            this.popularTitle = popularTitle;
        }

        public String getShortTitle() {
            return shortTitle;
        }

        public void setShortTitle(String shortTitle) {
            this.shortTitle = shortTitle;
        }

        public TRANSPORT_TYPE getTransportType() {
            return transportType;
        }

        public void setTransportType(TRANSPORT_TYPE transportType) {
            this.transportType = transportType;
        }

        public String getStationTypeName() {
            return stationTypeName;
        }

        public void setStationTypeName(String stationTypeName) {
            this.stationTypeName = stationTypeName;
        }

        public TYPE getType() {
            return type;
        }

        public void setType(TYPE type) {
            this.type = type;
        }
    }

    //----------------------------------------------------------------------------------------------//
    public static String getTypeStation(String typeStation) {
        switch (typeStation) {
            case "bus_stop":
                typeStation = "автобусная остановка";
                break;
            case "bus_station":
                typeStation = "автобусная остановка";
                break;
            case "train_station":
                typeStation = "платформа";
                break;
            case "station":
                typeStation = "остановка";
                break;
            case "airport":
                typeStation = "аэропорт";
                break;
            case "marine_station":
                typeStation = "морского судна остановка";
                break;
            case "stop":
                typeStation = "остановка";
                break;
            case "unknown":
                typeStation = "...";
                break;
            case "river_port":
                typeStation = "порт";
                break;
            case "platform":
                typeStation = "платформа";
                break;
            default:
                typeStation = "";
        }
        return typeStation;
    }

    public static STATION_TYPE setTypeStation(String typeStation) {
        STATION_TYPE type = null;
        switch (typeStation) {
            case "bus_stop":
                type = STATION_TYPE.bus_stop;
                break;
            case "bus_station":
                type = STATION_TYPE.bus_station;
                break;
            case "train_station":
                type = STATION_TYPE.train_station;
                break;
            case "station":
                type = STATION_TYPE.station;
                break;
            case "airport":
                type = STATION_TYPE.airport;
                break;
            case "marine_station":
                type = STATION_TYPE.marine_station;
                break;
            case "stop":
                type = STATION_TYPE.stop;
                break;
            case "unknown":
                type = STATION_TYPE.unknown;
                break;
            case "river_port":
                type = STATION_TYPE.river_port;
                break;
            case "platform":
                type = STATION_TYPE.platform;
                break;
            default:
                break;
        }
        return type;
    }

    public static String getTransportType(String typeStation) {
        switch (typeStation) {
            case "plane":
                typeStation = "самолет";
                break;
            case "train":
                typeStation = "поезд";
                break;
            case "suburban":
                typeStation = "электричка";
                break;
            case "bus":
                typeStation = "автобус";
                break;
            case "water":
                typeStation = "водный транспорт";
                break;
            case "helicopter":
                typeStation = "вертолет";
                break;
            default:
                typeStation = "";
        }
        return typeStation;
    }

    public static TRANSPORT_TYPE setTransportType(String typeStation) {
        TRANSPORT_TYPE type = null;
        switch (typeStation) {
            case "plane":
                type = TRANSPORT_TYPE.plane;
                break;
            case "train":
                type = TRANSPORT_TYPE.train;
                break;
            case "suburban":
                type = TRANSPORT_TYPE.suburban;
                break;
            case "bus":
                type = TRANSPORT_TYPE.bus;
                break;
            case "water":
                type = TRANSPORT_TYPE.water;
                break;
            case "helicopter":
                type = TRANSPORT_TYPE.helicopter;
                break;
            default:
                break;
        }
        return type;
    }

    public static TYPE setType(String type) {
        TYPE t = null;
        switch (type)
        {
            case "station":
                t = TYPE.station;
                break;
            case "settlement":
                t = TYPE.settlement;
                break;
            default:
                break;
        }
        return t;
    }

    public static EXPRESS_TYPE setExpressType(String type) {
        EXPRESS_TYPE t = null;
        switch (type)
        {
            case "aeroexpress":
                t = EXPRESS_TYPE.aeroexpress;
                break;
            case "express":
                t = EXPRESS_TYPE.express;
                break;
            default:
                break;
        }
        return t;
    }

    public static String getExpressType(EXPRESS_TYPE expressType) {
        String type = null;
        switch (expressType)
        {
            case aeroexpress:
                type = "aeroexpress";
                break;
            case express:
                type = "express";
                break;
            default:
                break;
        }
        return type;
    }

}
