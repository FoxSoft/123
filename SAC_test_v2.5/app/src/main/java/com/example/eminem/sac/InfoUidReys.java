package com.example.eminem.sac;

/**
 * Created by eminem on 22.10.2017.
 */

public class InfoUidReys {
    private boolean check;
    private int countItem;
    private String nameReys;
    private String number;
    private String uid;

    public InfoUidReys() {
        this.check = false;
    }

    public InfoUidReys(String nameReys, String number, String uid) {
        this.check = false;
        this.nameReys = nameReys;
        this.number = number;
        this.uid = uid;
    }

    @Override
    public String toString() {
        return "InfoUidReys{" +
                "check=" + check +
                ", countItem=" + countItem +
                ", nameReys='" + nameReys + '\'' +
                ", number='" + number + '\'' +
                ", uid='" + uid + '\'' +
                '}';
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public int getCountItem() {
        return countItem;
    }

    public void setCountItem(int countItem) {
        this.countItem = countItem;
    }

    public String getNameReys() {
        return nameReys;
    }

    public void setNameReys(String nameReys) {
        this.nameReys = nameReys;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}


