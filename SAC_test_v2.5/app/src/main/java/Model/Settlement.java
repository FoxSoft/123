package Model;

/**
 * Created by eminem on 18.12.2017.
 */

public class Settlement {
    private String distance;
    private String code;
    private double lat;
    private double lng;
    private String type;
    private String title;
    private String popularTitle;
    private String shorTitle;
    private int id;

    public Settlement() {
    }

    public Settlement(String distance, String code, double lat, double lng, String type, String title, String popularTitle, String shorTitle, int id) {

        this.distance = distance;
        this.code = code;
        this.lat = lat;
        this.lng = lng;
        this.type = type;
        this.title = title;
        this.popularTitle = popularTitle;
        this.shorTitle = shorTitle;
        this.id = id;
    }

    public boolean isEmpty()
    {
        if (title.isEmpty() & code.isEmpty()){
            return true;
        }
        else
            return false;
    }
    public String getDistance() {
        return distance;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Settlement{" +
                "distance='" + distance + '\'' +
                ", code='" + code + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                ", type='" + type + '\'' +
                ", title='" + title + '\'' +
                ", popularTitle='" + popularTitle + '\'' +
                ", shorTitle='" + shorTitle + '\'' +
                ", id=" + id +
                '}';
    }

    public String getPopularTitle() {
        return popularTitle;
    }

    public void setPopularTitle(String popularTitle) {
        this.popularTitle = popularTitle;
    }

    public String getShorTitle() {
        return shorTitle;
    }

    public void setShorTitle(String shorTitle) {
        this.shorTitle = shorTitle;
    }
}
