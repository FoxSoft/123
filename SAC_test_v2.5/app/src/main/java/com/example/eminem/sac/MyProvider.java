package com.example.eminem.sac;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.format.Time;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by eminem on 31.12.2017.
 */

public class MyProvider extends AppWidgetProvider {
    final static String ACTION_ON_CLICK = "ru.eminem.sac.widget.itemonclick";
    final static String ITEM_POSITION = "item_position";
    private static RemoteViews views;
    private PendingIntent service;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        for (int i : appWidgetIds) {
            updateWidget(context, appWidgetManager, i);
        }
    }

    public static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                       int appWidgetId) {

        views = new RemoteViews(context.getPackageName(), R.layout.widget);
        appWidgetManager.updateAppWidget(appWidgetId, views);
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId,
                R.id.lvList);
    }

    public void updateWidget(Context context, AppWidgetManager appWidgetManager,
                             int appWidgetId) {
        views = new RemoteViews(context.getPackageName(),
                R.layout.widget);

        setClickHeaderTitle(views, context, appWidgetId);

        setClickHeader(views, context, appWidgetId);

        setList(views, context, appWidgetId);

        setListClick(views, context, appWidgetId);

        appWidgetManager.updateAppWidget(appWidgetId, views);
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId,
                R.id.lvList);
    }

    void setClickHeaderTitle(RemoteViews rv, Context context, int appWidgetId) {

        Intent intent = new Intent(context, ScrollingActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        rv.setOnClickPendingIntent(R.id.textView10, pendingIntent);
    }

    void setClickHeader(RemoteViews rv, Context context, int appWidgetId) {

        Intent intent = new Intent(context, ScrollingActivity.class);
        PendingIntent pendingIntent2 = PendingIntent.getActivity(context, 0, intent, 0);
        rv.setOnClickFillInIntent(R.id.liner, intent);
    }

    public static void setList(RemoteViews rv, Context context, int appWidgetId) {
        Intent adapter = new Intent(context, MyService.class);
        adapter.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        Uri data = Uri.parse(adapter.toUri(Intent.URI_INTENT_SCHEME));
        adapter.setData(data);
        rv.setRemoteAdapter(R.id.lvList, adapter);
    }

    void setListClick(RemoteViews rv, Context context, int appWidgetId) {
        Intent listClickIntent = new Intent(context, MyProvider.class);
        listClickIntent.setAction(ACTION_ON_CLICK);
        PendingIntent listClickPIntent = PendingIntent.getBroadcast(context, 0,
                listClickIntent, 0);
        rv.setPendingIntentTemplate(R.id.lvList, listClickPIntent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        final AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        final Calendar startTime = Calendar.getInstance();
        startTime.set(Calendar.MINUTE, 0);
        startTime.set(Calendar.SECOND, 0);
        startTime.set(Calendar.MILLISECOND, 0);
        final Intent i = new Intent(context, ServiceWidget.class);
        if (intent.getAction().equalsIgnoreCase(ACTION_ON_CLICK)) {
            String itemPos = (String) intent.getSerializableExtra(ITEM_POSITION);
            if (itemPos != "") {
                TastyToast.makeText(context, itemPos, TastyToast.LENGTH_LONG, TastyToast.INFO);
            }
        }

        if (service == null) {
            service = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
        }


        manager.setRepeating(AlarmManager.RTC, startTime.getTime().getTime(), 5000, service);
    }

    @Override
    public void onEnabled(Context context) {
    }

    @Override
    public void onDisabled(Context context) {

    }

    public static class Status {
        private int idImg;
        private int max;
        public boolean isAdd = true;

        public static enum StatusTransport {
            DRIVE, ARRIVAL, DEPARTURE  //в пути, уехал, отправится
        }

        public Status() {
        }

        public static String FormatDurationTotime(String Duration) {
            int sec = (int) Float.parseFloat(Duration);
            int hour = sec / 3600;
            int minute = (int) (((double) (sec - (hour * 3600))) * 0.016666666667d);
            return hour == 0 ? minute + " мин." : hour + " ч. " + minute + " мин.";
        }

        public static List<Integer> FormatSecToTime(Integer time) {
            List<Integer> result = new ArrayList();
            Integer hour = time / 60;
            Integer minute = time - (hour * 60);
            result.add(hour);
            result.add(minute);
            return result;
        }

        @NonNull
        public static int[] timeToInt(String time) {
            int[] result = new int[0];
            String[] str = time.split(":");
            int chour = Integer.parseInt(str[0]);
            int minute = Integer.parseInt(str[1]);
            return new int[]{chour, minute};
        }

        public static int secondFromTime(String time) {
            Time t = new Time();
            int[] timeMas = timeToInt(time);
            t.set(0, timeMas[1], timeMas[0], 0, 0, 0);
            Time tNow = new Time(Time.getCurrentTimezone());
            tNow.setToNow();
            int hour = t.hour - tNow.hour;
            int result = (hour * 60) + (t.minute - tNow.minute);
            Log.d("MyProvider_test", String.valueOf(result));

            if (result < 1) {
                return 0;
            } else return result;
        }

        public String statusBusV2(String time) {
            Time t = new Time();
            int[] timeMas = timeToInt(time);
            t.set(0, timeMas[1], timeMas[0], 0, 0, 0);
            Time tNow = new Time(Time.getCurrentTimezone());
            tNow.setToNow();
            int hour = t.hour - tNow.hour;
            int result = (hour * 60) + (t.minute - tNow.minute);
            Integer pHour = FormatSecToTime(result).get(0);
            Integer pMinute = FormatSecToTime(result).get(1);
            if (result > 0) {
                String str;
                isAdd = true;
                this.idImg = R.drawable.buses_green;
                StringBuilder append = new StringBuilder().append("отправка через ").append(pHour == 0 ? "" : String.valueOf(pHour) + " ч. ");
                if (pMinute == 0) {
                    str = "";
                } else {
                    str = String.valueOf(pMinute) + " мин.";
                }
                return append.append(str).toString();
            } else if (hour == 0) {
                isAdd = false;
                this.idImg = R.drawable.buses_yellow;
                return "ушел " + String.valueOf(pMinute < 0 ? -pMinute : pMinute) + " мин. назад";
            } else if (result > (-this.max)) {
                isAdd = false;
                this.idImg = R.drawable.buses_yellow;
                return "ушел";
            } else {
                isAdd = false;
                this.idImg = R.drawable.buses_red;
                return "ушел";
            }
        }
    }

}