package com.example.eminem.sac;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ActivityThread extends AppCompatActivity {
    private final int IDD_CHECK_CATS = 3;
    AlertDialog.Builder ad;
    //private AdapterStationList adapterStationList;
    public String[] checkCatsName;
    Context context;
    private DBAdapter dbAdapter;
    private Download download;
    private EditText editNumber;
    private List<InfoUidReys> infoUidReysList;
    private ListView listView;
    public boolean[] mCheckedItems;
//    public List<StationInfo> stationListList;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String string2 = editNumber.getText().toString();
                if (!string2.isEmpty()) {
                    infoUidReysList = dbAdapter.findeUIDandNameReys(string2);
                    if (!infoUidReysList.isEmpty()) {
                        mCheckedItems = new boolean[infoUidReysList.size()];
                        checkCatsName = new String[infoUidReysList.size()];
                        for (int i = 0; i < infoUidReysList.size(); ++i) {
                            checkCatsName[i] = (infoUidReysList.get(i)).getNameReys();
                            mCheckedItems[i] = false;
                        }
                        showDialog(3);
                        return;
                    }
                    Snackbar.make(view, "\u041d\u0435\u0442 \u0442\u0430\u043a\u043e\u0433\u043e \u043d\u043e\u043c\u0435\u0440\u0430 \u0432 \u0431\u0430\u0437\u0435...", (int)0).setAction((CharSequence)"Action", null).show();
                    return;
                }
                Snackbar.make(view, "\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u043d\u043e\u043c\u0435\u0440 \u043c\u0430\u0440\u0448\u0440\u0443\u0442\u0430", (int)0).setAction((CharSequence)"Action", null).show();
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMax(100);
        progressDialog.setTitle("Загрузка рассписания");
        progressDialog.setMessage("Соединение с сервером...");

        if (Download.isOnline2(getApplicationContext())) {
            download.execute();
            refresh();
        } else Log.d("schedule_parse", "no net");
    }

    protected Dialog onCreateDialog(int n) {
        switch (n) {
            default: {
                return null;
            }
            case 3:
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.bus_station);
        builder.setTitle("\u0412\u044b\u0431\u0435\u0440\u0438\u0442\u0435 \u043c\u0430\u0440\u0448\u0440\u0443\u0442").setCancelable(false).setMultiChoiceItems(checkCatsName, mCheckedItems, new DialogInterface.OnMultiChoiceClickListener(){

            public void onClick(DialogInterface dialogInterface, int n, boolean bl) {
                mCheckedItems[n] = bl;
                infoUidReysList.get(n).setCheck(bl);
            }
        }).setPositiveButton("\u0413\u043e\u0442\u043e\u0432\u043e", new DialogInterface.OnClickListener(){

            /*
             * Enabled aggressive block sorting
             */
            public void onClick(DialogInterface dialogInterface, int n) {
                StringBuilder stringBuilder = new StringBuilder();


                int n2 = 0;
                do {
                    if (n2 >= infoUidReysList.size()) {
                        Toast.makeText(getApplicationContext(), stringBuilder.toString(), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    stringBuilder.append("" + infoUidReysList.get(n2).getUid());
                    if (infoUidReysList.get(n2).isCheck()) {
                  //      download = new Download(infoUidReysList.get(n2).getUid());
                  //      download.execute(new String[]{""});
                        stringBuilder.append(" \u0432\u044b\u0431\u0440\u0430\u043d\n");
                    } else {
                        stringBuilder.append(" \u043d\u0435 \u0432\u044b\u0431\u0440\u0430\u043d\n");
                    }
                    ++n2;
                } while (true);
            }
        }).setNegativeButton("\u041e\u0442\u043c\u0435\u043d\u0430", new DialogInterface.OnClickListener(){

            public void onClick(DialogInterface dialogInterface, int n) {
                dialogInterface.cancel();
            }
        });
        return builder.create();
    }

    private void refresh() {
        Timer t = new Timer();
        Timer tim = new Timer();
        final DBThread thread = new DBThread(getApplicationContext(), tim);
        tim.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (!download.isSet) {
                            progressDialog.show();
                            Log.d("shedule", "-");
                        } else {
                            progressDialog.dismiss();
                            Log.d("shedule", "+");
                            thread.start();
                        }
                    }
                });
            }
        }, 0, 1000);

    }
}
