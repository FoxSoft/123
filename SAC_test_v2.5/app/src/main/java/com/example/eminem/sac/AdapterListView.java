package com.example.eminem.sac;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Picture;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PictureDrawable;
import android.os.Trace;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.xw.repo.BubbleSeekBar;

import java.util.List;

/**
 * Created by eminem on 18.11.2017.
 */

public class AdapterListView extends RecyclerView.Adapter<AdapterListView.ViewHolder> {

    private List<Bus> buses;
    private Context context;
    private LayoutInflater inflater;

    public AdapterListView(Context context, List<Bus> buses) {
        this.inflater = LayoutInflater.from(context);
        this.buses = buses;
        this.context = context;
    }

    @Override
    public boolean onFailedToRecycleView(ViewHolder holder) {
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Bus bus = buses.get(position);

        viewHolder.TNumber.setText(buses.get(position).getNumber());
        viewHolder.TFromTime.setText(buses.get(position).getFromTime());
        viewHolder.TFrom.setText(buses.get(position).getFromStation());
        viewHolder.TTo.setText(buses.get(position).getToStation());
        viewHolder.TToTime.setText(buses.get(position).getToTime());
        viewHolder.TDuration.setText(buses.get(position).getDuration());
        viewHolder.TDays.setText(buses.get(position).getDays());
        viewHolder.TNameReys.setText((buses.get(position)).getNameReys());
        viewHolder.TStatus.setText(buses.get(position).getStatus());
        viewHolder.Img.setImageResource(buses.get(position).getIdImg());
        viewHolder.seekBar.setMax(buses.get(position).getTrack_max());
        viewHolder.seekBar.setProgress(buses.get(position).getPosition());
        //рабоатет смена ползунка при разном типе транспорта
        //viewHolder.seekBar.setThumb(context.getResources().getDrawable(R.drawable.seek_thumb_surban));
        viewHolder.bubbleSeekBar.getConfigBuilder().max(buses.get(position).getTrack_max()).build();
        viewHolder.bubbleSeekBar.setProgress(buses.get(position).getPosition());
        viewHolder.startTime.setText("0 мин.");
        viewHolder.finishTime.setText(buses.get(position).getTrack_max().toString() + " мин.");
        if((buses.get(position).isTrack_visible())) {
            viewHolder.seekBar.setVisibility(View.VISIBLE);
            viewHolder.bubbleSeekBar.setVisibility(View.VISIBLE);
            viewHolder.startTime.setVisibility(View.VISIBLE);
            viewHolder.finishTime.setVisibility(View.VISIBLE);
        } else {
            viewHolder.seekBar.setVisibility(View.INVISIBLE);
            viewHolder.bubbleSeekBar.setVisibility(View.INVISIBLE);
            viewHolder.startTime.setVisibility(View.INVISIBLE);
            viewHolder.finishTime.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return buses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView Img;
        public TextView TDays;
        public TextView TDuration;
        public TextView TFrom;
        public TextView TFromTime;
        public TextView TNameReys;
        public TextView TNumber;
        public TextView TStatus;
        public TextView TTo;
        public TextView TToTime;
        public BubbleSeekBar bubbleSeekBar;
        public TextView finishTime;
        public SeekBar seekBar;
        public TextView startTime;

        public ViewHolder(View view) {
            super(view);
            this.TFrom = (TextView)view.findViewById(R.id.fromStation);
            this.TTo = (TextView)view.findViewById(R.id.toStation);
            this.TToTime = (TextView)view.findViewById(R.id.arrival);
            this.TFromTime = (TextView)view.findViewById(R.id.fromTime);
            this.TStatus = (TextView)view.findViewById(R.id.status);
            this.TDays = (TextView)view.findViewById(R.id.days);
            this.TDuration = (TextView)view.findViewById(R.id.duration);
            this.TNameReys = (TextView)view.findViewById(R.id.nameReys);
            this.TNumber = (TextView)view.findViewById(R.id.number);
            this.Img = (ImageView)view.findViewById(R.id.imageView);
            this.bubbleSeekBar = (BubbleSeekBar)view.findViewById(R.id.track_distation);
            this.seekBar = (SeekBar)view.findViewById(R.id.track_bus);
            this.startTime = (TextView)view.findViewById(R.id.t1);
            this.finishTime = (TextView)view.findViewById(R.id.t2);
        }
    }
}
