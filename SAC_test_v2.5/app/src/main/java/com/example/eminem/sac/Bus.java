package com.example.eminem.sac;

import android.widget.TextView;

/**
 * Created by eminem on 22.10.2017.
 */

public class Bus {

    private String fromStation;
    private String toStation;
    private String fromTime;
    private String toTime;
    private String number;
    private String duration;
    private String nameReys;
    private String days;
    private String typeTransport;
    private String Uid;
    //=========================//
    private Integer id;
    private Integer track_max = 0;
    private boolean track_visible ;
    private Integer position = 1;
    private TextView StartTime;
    private TextView FinishTime;
    private String status;
    private Integer idImg;

    public Bus(Integer id, String fromStation, String toStation, String fromTime, String toTime,
               String number, String duration, String nameReys, String days, String status, Integer idImg) {
        this.id = id;
        this.fromStation = fromStation;
        this.toStation = toStation;
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.number = number;
        this.duration = duration;
        this.nameReys = nameReys;
        this.days = days;
        this.status = status;
        this.idImg = idImg;

        this.track_max = 0;
        this.position = 0;
        this.track_visible = false;
    }

    public Bus() {
//        this.track_max = 0;
//        this.position = 0;
//        this.track_visible = false;
    }

    public String getUid() {
        return Uid;
    }

    public void setUid(String Uid) {
        this.Uid = Uid;
    }

    public String getTypeTransport() {
        return typeTransport;
    }

    public void setTypeTransport(String typeTransport) {
        this.typeTransport = typeTransport;
    }

    public TextView getStartTime() {
        return StartTime;
    }

    public void setStartTime(TextView startTime) {
        StartTime = startTime;
    }

    public TextView getFinishTime() {
        return FinishTime;
    }

    public void setFinishTime(TextView finishTime) {
        FinishTime = finishTime;
    }

    public Integer getTrack_max() {
        return track_max;
    }

    public void setTrack_max(Integer track_max) {
        this.track_max = track_max;
    }

    public boolean isTrack_visible() {
        return track_visible;
    }

    public void setTrack_visible(boolean track_visible) {
        this.track_visible = track_visible;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFromStation() {
        return fromStation;
    }

    public void setFromStation(String fromStation) {
        this.fromStation = fromStation;
    }

    public String getToStation() {
        return toStation;
    }

    public void setToStation(String toStation) {
        this.toStation = toStation;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getNameReys() {
        return nameReys;
    }

    public void setNameReys(String nameReys) {
        this.nameReys = nameReys;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getIdImg() {
        return idImg;
    }

    public void setIdImg(Integer idImg) {
        this.idImg = idImg;
    }

    @Override
    public String toString() {
        return "Bus{" +
                "id=" + id +
                ", fromStation='" + fromStation + '\'' +
                ", toStation='" + toStation + '\'' +
                ", fromTime='" + fromTime + '\'' +
                ", toTime='" + toTime + '\'' +
                ", number='" + number + '\'' +
                ", duration='" + duration + '\'' +
                ", nameReys='" + nameReys + '\'' +
                ", days='" + days + '\'' +
                ", status='" + status + '\'' +
                ", idImg=" + idImg +
                ", typeTransport='" + typeTransport + '\'' +
                ", Uid='" + Uid + '\'' +
                ", track_max=" + track_max +
                ", track_visible=" + track_visible +
                ", position=" + position +
                ", StartTime=" + StartTime +
                ", FinishTime=" + FinishTime +
                '}';
    }

    public void clear()
    {
        this.id = null;
        this.fromStation = null;
        this.toStation = null;
        this.fromTime = null;
        this.toTime = null;
        this.number = null;
        this.duration = null;
        this.nameReys = null;
        this.days = null;
        this.status = null;
        this.idImg = null;
    }
}
