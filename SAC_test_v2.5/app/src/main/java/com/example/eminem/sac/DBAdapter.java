package com.example.eminem.sac;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import Model.StationDB;
import Model.Widget;

import static android.content.ContentValues.TAG;

/**
 * Created by eminem on 22.10.2017.
 */

public class DBAdapter {
    public static final String DB_NAME = "list_station.db";
    private static String DB_PATH = "";
    public static final Integer DB_VERSION = 1;
    private List<Bus> BusList = new ArrayList();
    public Context context;
    private Yandex.SearchAPI searchAPI = new Yandex.SearchAPI();;
    public SQLiteDatabase db;
    private DBAdapter.DbHelper dbHelper;

    public DBAdapter(Context context) {
        this.context = context;
        this.dbHelper = new DBAdapter.DbHelper(context);
        this.db = this.dbHelper.getReadableDatabase();
    }



    private List filListRasp(Cursor cursor) {
        BusList.clear();
        if (cursor != null) {
            while (true) {
                try {
                    if (!cursor.moveToNext()) {
                        break;
                    }

                    Bus bus = new Bus();
                    bus.setId(Integer.valueOf(cursor.getInt(cursor.getColumnIndex("_id"))));
                    bus.setNumber(cursor.getString(cursor.getColumnIndex("number")));
                    bus.setFromStation(cursor.getString(cursor.getColumnIndex("from_station")));
                    bus.setToStation(cursor.getString(cursor.getColumnIndex("to_station")));
                    bus.setFromTime(cursor.getString(cursor.getColumnIndex("from_time")));
                    bus.setToTime(cursor.getString(cursor.getColumnIndex("to_time")));
                    bus.setDuration(cursor.getString(cursor.getColumnIndex("duration")));
                    bus.setDays(cursor.getString(cursor.getColumnIndex("days")));
                    bus.setIdImg(R.drawable.buses_green);
                    bus.setNameReys(cursor.getString(cursor.getColumnIndex("name_reys")));
                    BusList.add(bus);
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            }
        }

        return BusList;
    }

    private Yandex.SearchAPI filListRaspV2(Cursor cursor) {
        if (cursor != null) {
            while (true) {
                try {
                    if (!cursor.moveToNext()) {
                        break;
                    }

                    Yandex.SearchAPI.Segments segments = new Yandex.SearchAPI.Segments();

                    searchAPI.getPagination().setTotal(cursor.getString(cursor.getColumnIndex("PaginationTotal")));
                    searchAPI.getPagination().setLimit(cursor.getString(cursor.getColumnIndex("PaginationLimit")));
                    searchAPI.getPagination().setOffset(cursor.getString(cursor.getColumnIndex("PaginationOffset")));
                    searchAPI.getSearch().setDate(cursor.getString(cursor.getColumnIndex("SearchDate")));

                    searchAPI.getSearch().getToSearch().setCode(cursor.getString(cursor.getColumnIndex("SearchToCode")));
                    searchAPI.getSearch().getToSearch().setType(Yandex.setType(cursor.getString(cursor.getColumnIndex("SearchToType"))));
                    searchAPI.getSearch().getToSearch().setPopularTitle(cursor.getString(cursor.getColumnIndex("SearchToPopularTitle")));
                    searchAPI.getSearch().getToSearch().setShortTitle(cursor.getString(cursor.getColumnIndex("SearchToShortTitle")));
                    searchAPI.getSearch().getToSearch().setTitle(cursor.getString(cursor.getColumnIndex("SearchToTitle")));

                    searchAPI.getSearch().getFromSearch().setCode(cursor.getString(cursor.getColumnIndex("SearchFromCode")));
                    searchAPI.getSearch().getFromSearch().setType(Yandex.setType(cursor.getString(cursor.getColumnIndex("SearchFromType"))));
                    searchAPI.getSearch().getFromSearch().setPopularTitle(cursor.getString(cursor.getColumnIndex("SearchFromPopularTitle")));
                    searchAPI.getSearch().getFromSearch().setShortTitle(cursor.getString(cursor.getColumnIndex("SearchFromShortTitle")));
                    searchAPI.getSearch().getFromSearch().setTitle(cursor.getString(cursor.getColumnIndex("SearchFromTitle")));

                    segments.setExceptDays(cursor.getString(cursor.getColumnIndex("ExceptDays")));
                    segments.setArrival(cursor.getString(cursor.getColumnIndex("Arrival")));
                    segments.setDeparturePlatform(cursor.getString(cursor.getColumnIndex("DeparturePlatform")));
                    segments.setDeparture(cursor.getString(cursor.getColumnIndex("Departure")));
                    segments.setStops(cursor.getString(cursor.getColumnIndex("Stops")));
                    segments.setDays(cursor.getString(cursor.getColumnIndex("Days")));
                    segments.setDuration(cursor.getString(cursor.getColumnIndex("Duration")));
                    segments.setDeparturePlatform(cursor.getString(cursor.getColumnIndex("DepartureTerminal")));
                    segments.setArrivalTerminal(cursor.getString(cursor.getColumnIndex("ArrivalTerminal")));
                    segments.setStartDate(cursor.getString(cursor.getColumnIndex("StartDate")));
                    segments.setArrivalPlatform(cursor.getString(cursor.getColumnIndex("ArrivalPlatform")));

                    segments.getFrom().setCode(cursor.getString(cursor.getColumnIndex("FromStationCode")));
                    segments.getFrom().setTitle(cursor.getString(cursor.getColumnIndex("FromStationTitle")));
                    segments.getFrom().setStationType(Yandex.setTypeStation(cursor.getString(cursor.getColumnIndex("FromStationStationType"))));
                    segments.getFrom().setPopularTitle(cursor.getString(cursor.getColumnIndex("FromStationPopularTitle")));
                    segments.getFrom().setShortTitle(cursor.getString(cursor.getColumnIndex("FromStationShortTitle")));
                    segments.getFrom().setTransportType(Yandex.setTransportType(cursor.getString(cursor.getColumnIndex("FromStationTransportType"))));
                    segments.getFrom().setStationTypeName(cursor.getString(cursor.getColumnIndex("FromStationStationTypeName")));
                    segments.getFrom().setType(Yandex.setType(cursor.getString(cursor.getColumnIndex("FromStationType"))));

                    segments.getThread().setShortTitle(cursor.getString(cursor.getColumnIndex("ThreadShortTitle")));
                    segments.getThread().setThreadMethodLink(cursor.getString(cursor.getColumnIndex("ThreadMethodLink")));
                    segments.getThread().setTransportType(Yandex.setTransportType(cursor.getString(cursor.getColumnIndex("ThreadTransportType"))));
                    segments.getThread().setVehicle(cursor.getString(cursor.getColumnIndex("ThreadVehicle")));
                    segments.getThread().setExpressType(Yandex.setExpressType(cursor.getString(cursor.getColumnIndex("ThreadExpressType"))));
                    segments.getThread().setUid(cursor.getString(cursor.getColumnIndex("Uid")));
                    segments.getThread().setTitle(cursor.getString(cursor.getColumnIndex("ThreadTitle")));
                    segments.getThread().setNumber(cursor.getString(cursor.getColumnIndex("Number")));

                    segments.getThread().getCarrier().setCode(cursor.getString(cursor.getColumnIndex("CarrierCode")));
                    segments.getThread().getCarrier().setContacts(cursor.getString(cursor.getColumnIndex("CarrierContacts")));
                    segments.getThread().getCarrier().setUrl(cursor.getString(cursor.getColumnIndex("CarrierUrl")));
                    segments.getThread().getCarrier().setLogo_svg(cursor.getString(cursor.getColumnIndex("CarrierLogo_svg")));
                    segments.getThread().getCarrier().setTitle(cursor.getString(cursor.getColumnIndex("CarrierTitle")));
                    segments.getThread().getCarrier().setPhone(cursor.getString(cursor.getColumnIndex("CarrierPhone")));
                  //  segments.getThread().getCarrier().setco().getFromSearch().setCode(cursor.getString(cursor.getColumnIndex("CarrierCodes")));
                    segments.getThread().getCarrier().setAddress(cursor.getString(cursor.getColumnIndex("CarrierAddress")));
                    segments.getThread().getCarrier().setLogo(cursor.getString(cursor.getColumnIndex("CarrierLogo")));
                    segments.getThread().getCarrier().setEmail(cursor.getString(cursor.getColumnIndex("CarrierEmail")));

//                    searchAPI.getSearch().getFromSearch().setCode(cursor.getString(cursor.getColumnIndex("CodesIcao")));
//                    searchAPI.getSearch().getFromSearch().setCode(cursor.getString(cursor.getColumnIndex("CodesSirena")));
//                    searchAPI.getSearch().getFromSearch().setCode(cursor.getString(cursor.getColumnIndex("CodesIata")));

                    segments.getThread().getTransportSubtype().setColor(cursor.getString(cursor.getColumnIndex("TransportSubtypeColor")));
                    segments.getThread().getTransportSubtype().setCode(cursor.getString(cursor.getColumnIndex("TransportSubtypeCode")));
                    segments.getThread().getTransportSubtype().setTitle(cursor.getString(cursor.getColumnIndex("TransportSubtypeTitle")));

                    segments.getTo().setCode(cursor.getString(cursor.getColumnIndex("ToStationCode")));
                    segments.getTo().setTitle(cursor.getString(cursor.getColumnIndex("ToStationTitle")));
                    segments.getTo().setStationType(Yandex.setTypeStation(cursor.getString(cursor.getColumnIndex("ToStationStationType"))));
                    segments.getTo().setPopularTitle(cursor.getString(cursor.getColumnIndex("ToStationPopularTitle")));
                    segments.getTo().setShortTitle(cursor.getString(cursor.getColumnIndex("ToStationShortTitle")));
                    segments.getTo().setTransportType(Yandex.setTransportType(cursor.getString(cursor.getColumnIndex("ToStationTransportType"))));
                    segments.getTo().setStationTypeName(cursor.getString(cursor.getColumnIndex("ToStationStationTypeName")));
                    segments.getTo().setType(Yandex.setType(cursor.getString(cursor.getColumnIndex("ToStationType"))));

                    searchAPI.getSegments().add(segments);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("search__", "try catch");
                    break;
                }
            }
        }

        Log.d("search__", searchAPI.getSegments().get(0).getArrival());

        return searchAPI;
    }

    public void createDataBaseInstence() {
        dbHelper = new DBAdapter.DbHelper(context);
        db = dbHelper.getReadableDatabase();
    }

    public void createTable(String nameTable) {
        try {
            String SQL = "CREATE TABLE [" + nameTable +
                    "] ( _id INTEGER PRIMARY KEY AUTOINCREMENT, number VARCHAR, from_station   VARCHAR, to_station VARCHAR, from_time VARCHAR, to_time" +
                    " VARCHAR, duration VARCHAR, days VARCHAR, transportType  VARCHAR, uid VARCHAR, name_reys VARCHAR );";
            this.db.execSQL(SQL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createTableV2(String nameTable) {
        try {
            String SQL = "CREATE TABLE [" + nameTable + "] ( _id INTEGER PRIMARY KEY AUTOINCREMENT, PaginationTotal VARCHAR, PaginationLimit VARCHAR, PaginationOffset VARCHAR, " +
                    "SearchDate VARCHAR, SearchToCode VARCHAR, SearchToType VARCHAR, SearchToPopularTitle VARCHAR, SearchToShortTitle VARCHAR, SearchToTitle VARCHAR," +
                    " SearchFromCode VARCHAR, SearchFromType VARCHAR, SearchFromPopularTitle VARCHAR, SearchFromShortTitle VARCHAR, SearchFromTitle VARCHAR, " +
                    "ExceptDays VARCHAR, Arrival VARCHAR, DeparturePlatform VARCHAR, Departure VARCHAR, Stops VARCHAR, Days VARCHAR, Duration VARCHAR, " +
                    "DepartureTerminal VARCHAR, ArrivalTerminal VARCHAR, StartDate VARCHAR, ArrivalPlatform VARCHAR, FromStationCode VARCHAR, FromStationTitle VARCHAR, " +
                    "FromStationStationType VARCHAR, FromStationPopularTitle VARCHAR, FromStationShortTitle VARCHAR, FromStationTransportType VARCHAR, " +
                    "FromStationStationTypeName VARCHAR, FromStationType VARCHAR, Uid VARCHAR, ThreadTitle VARCHAR, Number VARCHAR, ThreadShortTitle VARCHAR, " +
                    "ThreadMethodLink VARCHAR, ThreadTransportType VARCHAR, ThreadVehicle VARCHAR, ThreadExpressType VARCHAR, CarrierCode VARCHAR, CarrierContacts VARCHAR, " +
                    "CarrierUrl VARCHAR, CarrierLogo_svg VARCHAR, CarrierTitle VARCHAR, CarrierPhone VARCHAR, CarrierCodes VARCHAR, CarrierAddress VARCHAR, " +
                    "CarrierLogo VARCHAR, CarrierEmail VARCHAR, CodesIcao VARCHAR, CodesSirena VARCHAR, CodesIata VARCHAR, TransportSubtypeColor VARCHAR, " +
                    "TransportSubtypeCode VARCHAR, TransportSubtypeTitle VARCHAR, ToStationCode VARCHAR, ToStationTitle VARCHAR, ToStationStationType VARCHAR, " +
                    "ToStationPopularTitle VARCHAR, ToStationShortTitle VARCHAR, ToStationTransportType VARCHAR, ToStationStationTypeName VARCHAR, ToStationType VARCHAR );";
            this.db.execSQL(SQL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteTable(String nameTable) {
        try {
            String SQL = "DROP TABLE [" + nameTable + "];";
            db.execSQL(SQL);
            Log.d("deleteTable", "табл удалена");
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("deleteTable", "нет такой табл");
        }
    }

    public String findCode(String code) {
        Cursor cursor = this.db.query("station_code", new String[]{"code"}, "station = ?", new String[]{code}, null, null, null);
        int count = cursor.getCount();
        String result = null;
        if (count <= 0) {
            return "";
        } else {
            while (cursor.moveToNext()) {
                result = cursor.getString(cursor.getColumnIndex("code"));
            }

            return result;
        }
    }

    public List<String> getNameStationListOfSearch(String nameStation) {
        String SQL = "select * from station_code where station like '" + nameStation + "%'";
        Cursor cursor = db.rawQuery(SQL, null);
        List<String> result = new ArrayList<>();

        if (!cursor.moveToFirst()) {
            return result;
        } else {
            while (cursor.moveToNext()) {
                // result += cursor.getString(cursor.getColumnIndex("station"));
                //Log.d("searchstation", cursor.getString(cursor.getColumnIndex("station")));
                result.add(cursor.getString(cursor.getColumnIndex("station")));
            }
            return result;
        }
        //Cursor cursor = this.db.query("station_code", new String[]{"nameStation"}, "station = ?", new String[]{code}, null, null, null);
//        int count = cursor.getCount();
//        String result = null;
//        if(count <= 0) {
//            return "";
//        } else {
//            while(cursor.moveToNext()) {
//                result = cursor.getString(cursor.getColumnIndex("code"));
//            }
//
//            return result;
//        }
    }

    public String findeNameReys(String var1) {
        Cursor var2 = this.db.query("uid_Reys", new String[]{"name_Reys"}, "number = ?", new String[]{var1}, (String) null, (String) null, (String) null);
        int var3 = var2.getCount();
        String var4 = null;
        if (var3 <= 0) {
            return "";
        } else {
            while (var2.moveToNext()) {
                var4 = var2.getString(var2.getColumnIndex("name_Reys"));
            }

            return var4;
        }
    }

    public String findeUID(String number) {
        Cursor cursor = this.db.query("uid_Reys", new String[]{"uid"}, "number = ?", new String[]{number}, null, null, null);
        int count = cursor.getCount();
        String result = null;
        if (count <= 0) {
            return "";
        } else {
            while (cursor.moveToNext()) {
                result = cursor.getString(cursor.getColumnIndex("uid"));
            }

            return result;
        }
    }

    public String test(String table) {
        Cursor cursor = this.db.query(table, new String[]{"Number"}, null, null, null, null, null);
        int count = cursor.getCount();
        String result = null;
        if (count <= 0) {
            return "+";
        } else {
            while (cursor.moveToNext()) {
                result = cursor.getString(cursor.getColumnIndex("Number"));
            }

            return result;
        }
    }

    public List findeUIDandNameReys(String number) {
        List<InfoUidReys> list = new ArrayList();

        Cursor cursor = db.query("uid_Reys", new String[]{"uid", "name_Reys"},
                "number = ?", new String[]{number}, null, null, null);
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                list.add(new InfoUidReys(cursor.getString(cursor.getColumnIndex("uid")), cursor.getString(cursor.getColumnIndex("name_Reys")), number));
            }
        }

        return list;
    }

    public void getcodeStationCode() {
        Cursor cursor = db.query("station_code", null, null, null, null, null, null);
        if (cursor != null) {
            while (cursor.moveToFirst()) {
                System.out.println(cursor.getString(cursor.getColumnIndex("code")));
            }
        }

    }

    public boolean hashCodeStation(String code) {
        int count;
        try {
            count = db.query("station_code", new String[]{"station"}, "code = ?", new String[]{code}, null, null, null).getCount();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return count > 0;
    }

    public boolean hashNumberToUid_Reys(String var1) {
        Cursor cursor;
        try {
            cursor = this.db.query("uid_Reys", new String[]{"uid"}, "number = ?", new String[]{var1}, (String) null, (String) null, (String) null);
        } catch (Exception var4) {
            var4.printStackTrace();
            return false;
        }

        return cursor != null;
    }

    public boolean hashTable(String table) {
        Cursor cursor;
        try {
            cursor = db.query("[" + table + "]", null, null, null, null, null, null);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return cursor != null;
    }

    public boolean hashUID(String uid) {
        int count;
        try {
            count = db.query("uid_Reys", new String[]{"number"}, "uid = ?", new String[]{uid}, null, null, null).getCount();
            if (count > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean hashUIDV2(String number, String UID) {
        int count;
        try {
            count = db.query("uid_Reys", new String[]{"uid"}, "number = ? AND name_Reys = ?", new String[]{number, UID}, null, null, null).getCount();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        if (count > 0) {
            return true;
        } else {

            return false;
        }
    }

    public void hiatoryRasp(String var1) {
        db.execSQL("create table historyRasp (_id INTEGER PRIMARY KEY AUTOINCREMENT, nameRyes varchar);");
    }

    public void insertCodeStation(String station, String code) {
        if (!hashCodeStation(code)) {
            String SQL = "INSERT INTO station_code(station, code)VALUES('" + station + "', '" + code + "');";
            db.execSQL(SQL);
        }

    }

    public void insertRaspTableV1(String nameTable, Bus bus) {
        String SQL = "INSERT INTO " + nameTable + " (number, from_station, to_station, from_time, to_time, duration, days, transportType, uid, name_reys)VALUES(" +
                "'" + bus.getNumber() + "', '" + bus.getFromStation() + "', '" + bus.getToStation() + "', '" +
                bus.getFromTime() + "', '" + bus.getToTime() + "', '" + bus.getDuration() + "', '" + bus.getDays() + "', '" +
                bus.getTypeTransport() + "', '" + bus.getUid() + "', '" + bus.getNameReys() + "');";
        db.execSQL(SQL);
    }

    public void insertRaspTableV2(String nameTable, Yandex.SearchAPI searchAPI)
    {

      //  Date date = new Date();
       // SimpleDateFormat formatForDateNow = new SimpleDateFormat("'последнее обновление:' hh:mm', 'yyyy.MM.dd ");
        String SQL;
            for(int i = 0; i < searchAPI.getSegments().size(); i++) {
                    SQL = "INSERT INTO [" + nameTable + "] " +
                            "(PaginationTotal, " +
                            "PaginationLimit, " +
                            "PaginationOffset, " +
                            "SearchDate, " +
                            "SearchToCode, " +
                            "SearchToType, " +
                            "SearchToPopularTitle, " +
                            "SearchToShortTitle, " +
                            "SearchToTitle," +
                            "SearchFromCode, " +
                            "SearchFromType, " +
                            "SearchFromPopularTitle, " +
                            "SearchFromShortTitle, " +
                            "SearchFromTitle, " +
                            "ExceptDays, " +
                            "Arrival, " +
                            "DeparturePlatform, " +
                            "Departure, " +
                            "Stops, " +
                            "Days, " +
                            "Duration, " +
                            "DepartureTerminal, " +
                            "ArrivalTerminal, " +
                            "StartDate, " +
                            "ArrivalPlatform, " +
                            "FromStationCode, " +
                            "FromStationTitle, " +
                            "FromStationStationType, " +
                            "FromStationPopularTitle, " +
                            "FromStationShortTitle, " +
                            "FromStationTransportType, " +
                            "FromStationStationTypeName, " +
                            "FromStationType, " +
                            "Uid, " +
                            "ThreadTitle, " +
                            "Number, " +
                            "ThreadShortTitle, " +
                            "ThreadMethodLink, " +
                            "ThreadTransportType, " +
                            "ThreadVehicle, " +
                            "ThreadExpressType, " +
                            "CarrierCode, " +
                            "CarrierContacts, " +
                            "CarrierUrl, " +
                            "CarrierLogo_svg, " +
                            "CarrierTitle, " +
                            "CarrierPhone, " +
                            "CarrierCodes, " +
                            "CarrierAddress, " +
                            "CarrierLogo, " +
                            "CarrierEmail, " +
                            "CodesIcao, " +
                            "CodesSirena, " +
                            "CodesIata, " +
                            "TransportSubtypeColor, " +
                            "TransportSubtypeCode, " +
                            "TransportSubtypeTitle, " +
                            "ToStationCode, " +
                            "ToStationTitle, " +
                            "ToStationStationType, " +
                            "ToStationPopularTitle, " +
                            "ToStationShortTitle, " +
                            "ToStationTransportType, " +
                            "ToStationStationTypeName, " +
                            "ToStationType) " +
                            "VALUES(" + "'" + searchAPI.getPagination().getTotal() + "', '" +
                            null + "', '" +
                            null + "', '" +
                            null + "', '" +
                            searchAPI.getSearch().getToSearch().getCode() + "', '" +
                            searchAPI.getSearch().getToSearch().getType() + "', '" +
                            searchAPI.getSearch().getToSearch().getPopularTitle() + "', '" +
                            searchAPI.getSearch().getToSearch().getShortTitle() + "', '" +
                            searchAPI.getSearch().getToSearch().getTitle() + "', '" +
                            searchAPI.getSearch().getFromSearch().getCode() + "', '" +
                            searchAPI.getSearch().getFromSearch().getType() + "', '" +
                            searchAPI.getSearch().getFromSearch().getPopularTitle() + "', '" +
                            searchAPI.getSearch().getFromSearch().getShortTitle() + "', '" +
                            searchAPI.getSearch().getFromSearch().getTitle() + "', '" +
                            searchAPI.getSegments().get(i).getExceptDays() + "', '" +
                            searchAPI.getSegments().get(i).getArrival() + "', '" +
                            searchAPI.getSegments().get(i).getDeparturePlatform() + "', '" +
                            searchAPI.getSegments().get(i).getDeparture() + "', '" +
                            searchAPI.getSegments().get(i).getStops() + "', '" +
                            searchAPI.getSegments().get(i).getDays() + "', '" +
                            searchAPI.getSegments().get(i).getDuration() + "', '" +
                            searchAPI.getSegments().get(i).getDepartureTerminal() + "', '" +
                            searchAPI.getSegments().get(i).getArrivalTerminal() + "', '" +
                            searchAPI.getSegments().get(i).getStartDate() + "', '" +
                            searchAPI.getSegments().get(i).getArrivalPlatform() + "', '" +
                            searchAPI.getSegments().get(i).getFrom().getCode() + "', '" +
                            searchAPI.getSegments().get(i).getFrom().getTitle() + "', '" +
                            searchAPI.getSegments().get(i).getFrom().getStationType() + "', '" +
                            searchAPI.getSegments().get(i).getFrom().getPopularTitle() + "', '" +
                            searchAPI.getSegments().get(i).getFrom().getShortTitle() + "', '" +
                            searchAPI.getSegments().get(i).getFrom().getTransportType() + "', '" +
                            searchAPI.getSegments().get(i).getFrom().getStationTypeName() + "', '" +
                            searchAPI.getSegments().get(i).getFrom().getType() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getUid() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getTitle() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getNumber() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getShortTitle() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getThreadMethodLink() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getTransportType() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getVehicle() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getExpressType() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getCarrier().getCode() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getCarrier().getContacts() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getCarrier().getUrl() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getCarrier().getLogo_svg() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getCarrier().getTitle() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getCarrier().getPhone() + "', '" +
                            null + "', '" +
                            searchAPI.getSegments().get(i).getThread().getCarrier().getAddress() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getCarrier().getLogo() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getCarrier().getEmail() + "', '" +
                            null + "', '" +
                            null + "', '" +
                            null + "', '" +
                            searchAPI.getSegments().get(i).getThread().getTransportSubtype().getColor() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getTransportSubtype().getCode() + "', '" +
                            searchAPI.getSegments().get(i).getThread().getTransportSubtype().getTitle() + "', '" +
                            searchAPI.getSegments().get(i).getTo().getCode() + "', '" +
                            searchAPI.getSegments().get(i).getTo().getTitle() + "', '" +
                            searchAPI.getSegments().get(i).getTo().getStationType() + "', '" +
                            searchAPI.getSegments().get(i).getTo().getPopularTitle() + "', '" +
                            searchAPI.getSegments().get(i).getTo().getShortTitle() + "', '" +
                            searchAPI.getSegments().get(i).getTo().getTransportType() + "', '" +
                            searchAPI.getSegments().get(i).getTo().getStationTypeName() + "', '" +
                            searchAPI.getSegments().get(i).getTo().getType() + "');";
                 this.db.execSQL(SQL);

            }

    }

    public void insertUidReys(String UID, String number, String nameReys) {
        if (!hashUID(UID)) {
            String SQL = "INSERT INTO uid_Reys(uid, number, name_Reys)VALUES('" + UID + "', '" + number + "', '" + nameReys + "');";
            db.execSQL(SQL);
        } else {
            Log.d("insertUidReys", "уже есть такой uid");
        }
    }

    public void insertUidReysV2(String UID, String number, String nameReys) {
        if (!hashUIDV2(number, nameReys)) {
            String SQL = "INSERT INTO uid_Reys(uid, number, name_Reys)VALUES('" + UID + "', '" + number + "', '" + nameReys + "');";
            db.execSQL(SQL);
        } else {
            Log.d("insertUidReys", "уже есть такой uid");
        }
    }

    public List<Bus> recordRaspTable(String nameReys) {
        return filListRasp(db.query(nameReys, null, null, null, null, null, null));
    }

    public Yandex.SearchAPI recordRaspTableV2(String nameReys) {
        return filListRaspV2(db.query(nameReys, null, null, null, null, null, null));
    }

    public boolean isRecordToRaspTable(String nameReys) {
        int count;
        try {
            count = db.query("["+nameReys+"]", new String[]{"_id"}, null, null, null, null, null).getCount();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        if (count > 0) {
            return true;
        } else {

            return false;
        }
    }

    public List<Widget> getRecordRaspTable(String nameReys) {

        List<Widget> widgets = new ArrayList<>();
        Cursor cursor = db.query(nameReys, null, null, null, null, null, null);
        Yandex.TRANSPORT_TYPE transportType;
        if (cursor != null) {
            while (true) {
                try {
                    if (!cursor.moveToNext()) {
                        break;
                    }

                    Widget widget = new Widget();
                    widget.setId(Integer.valueOf(cursor.getInt(cursor.getColumnIndex("_id"))));
                    widget.setFromTime(cursor.getString(cursor.getColumnIndex("from_time")));
                    widget.setToTime(cursor.getString(cursor.getColumnIndex("to_time")));
                    widget.setDays(cursor.getString(cursor.getColumnIndex("days")));
                    widget.setColorStatus(Color.parseColor("#ffffbb33"));
                    widget.setDuration(cursor.getString(cursor.getColumnIndex("duration")));
                    widget.setNameReys(cursor.getString(cursor.getColumnIndex("name_reys")));
                    transportType = getTypeTransport(cursor.getString(cursor.getColumnIndex("transportType")));
                    widget.setIdImg(getTypeTransportImg(transportType));
                    widgets.add(widget);
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            }
        }
        return widgets;
    }

    public Yandex.TRANSPORT_TYPE getTypeTransport(String typeTransport) {
        switch (typeTransport) {
            case "plane":
                return Yandex.TRANSPORT_TYPE.plane;
            case "train":
                return Yandex.TRANSPORT_TYPE.train;
            case "suburban":
                return Yandex.TRANSPORT_TYPE.suburban;
            case "bus":
                return Yandex.TRANSPORT_TYPE.bus;
            case "water":
                return Yandex.TRANSPORT_TYPE.water;
            case "helicopter":
                return Yandex.TRANSPORT_TYPE.helicopter;
            default:
                return Yandex.TRANSPORT_TYPE.bus;
        }
    }

    public int getTypeTransportImg(Yandex.TRANSPORT_TYPE transportType) {
        switch (transportType) {
            case bus:
                return R.drawable.buses_green;
            case plane:
                return 1;
            case train:
                return R.drawable.train_green;
            case water:
                return 1;
            case suburban:
                return R.drawable.surban_green;
            case helicopter:
                return 1;
            default:
                return 0;
        }
    }

    public String recordhistoryRasp() {
        Cursor cursor = db.query("historyRasp", null, null, null, null, null, null);
        int count = cursor.getCount();
        String result = null;
        if (count <= 0) {
            return "";
        } else {
            while (cursor.moveToNext()) {
                result = cursor.getString(cursor.getColumnIndex("nameReys"));
            }

            return result;
        }
    }

    //------------------codeAllStation--------------------------//

    List<String> getCodeStation(String nameStation) {
        String SQL = "select codeStation from codeAllStation where nameStation = '" + nameStation + "%'";
        Cursor cursor = db.rawQuery(SQL, null);
        List<String> result = new ArrayList<>();

        if (!cursor.moveToFirst()) {
            return result;
        } else {
            while (cursor.moveToNext()) {

                result.add(cursor.getString(cursor.getColumnIndex("station")));
            }
            return result;
        }
    }

    List<String> getCodeStation(String nameRegion, String nameCity, String nameStation) {
        String SQL = "select * from codeAllStation where nameRegion like '" + nameRegion +
                "%' and nameCity like '" + nameCity + "%' and nameStation like '" + nameStation + "%';";
        Cursor cursor = db.rawQuery(SQL, null);
        List<String> result = new ArrayList<>();

        if (!cursor.moveToFirst()) {
            return result;
        } else {
            while (cursor.moveToNext()) {
                result.add(cursor.getString(cursor.getColumnIndex("codeStation")));
            }
            return result;
        }
    }

//    List<rowRasp> getCodeStation(String nameRegion, String nameCity, String nameStation)
//    {
//        String SQL = "select * from codeAllStation where nameRegion like '"+nameRegion+
//                "%' and nameCity like '"+nameCity+"%' and nameStation like '"+nameStation+"%';";
//        Cursor cursor = db.rawQuery(SQL, null);
//        List<rowRasp> result = new ArrayList<>();
//
//        if (!cursor.moveToFirst())
//        {
//            return result;
//        }
//        else {
//            while (cursor.moveToNext()) {
//
//                result.add(new rowRasp(cursor.getString(cursor.getColumnIndex("nameRegion")),
//                        null, cursor.getString(cursor.getColumnIndex("nameCity")),
//                        null, cursor.getString(cursor.getColumnIndex("nameStation")),
//                        null, null, null));
//
//
//                // result.add(cursor.getString(cursor.getColumnIndex("station")));
//            }
//            return result;
//        }
//    }

    List<String> getStationOfCity(String nameCity) {
        String SQL = "select * from codeAllStation where nameCity like '" + nameCity + "%'";
        Cursor cursor = db.rawQuery(SQL, null);
        List<String> result = new ArrayList<>();

        if (!cursor.moveToFirst()) {
            return result;
        } else {
            while (cursor.moveToNext()) {
                result.add(cursor.getString(cursor.getColumnIndex("nameStation")));
            }
            return result;
        }
    }

    List<String> getCityOfRegion(String nameRegion) {
        String SQL = "select * from codeAllStation where nameCity like '" + nameRegion + "%'";
        Cursor cursor = db.rawQuery(SQL, null);
        List<String> result = new ArrayList<>();

        if (!cursor.moveToFirst()) {
            return result;
        } else {
            while (cursor.moveToNext()) {
                result.add(cursor.getString(cursor.getColumnIndex("nameStation")));
            }
            return result;
        }
    }

    public String getCodeCity(String nameCity) {
        String SQL = "select DISTINCT nameCity, codeCity from codeAllStation where nameCity='" + nameCity + "'";
        Cursor cursor2 = db.rawQuery(SQL, null);
        String result = "";
        if (cursor2.getCount() == 0) {
            return result;
        } else {
            cursor2.moveToFirst();
            while (!cursor2.isAfterLast()) {
                result = cursor2.getString(cursor2.getColumnIndex("codeCity"));
                cursor2.moveToNext();
            }
            return result;
        }
    }

    public List<StationDB> getStationOfInput2(String nameStation) {
        String SQL = "select * from codeAllStation where nameStation like '" + nameStation + "%'";
        Cursor cursor = db.rawQuery(SQL, null);
        List<StationDB> result = new ArrayList<>();
        String typeStation = "";
        if (cursor.getCount() == 0) {
            return result;
        } else {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {

                typeStation = Yandex.getTypeStation(cursor.getString(cursor.getColumnIndex("typeStation")));

                result.add(new StationDB(cursor.getString(cursor.getColumnIndex("nameRegion")), cursor.getString(cursor.getColumnIndex("codeRegion")),
                        cursor.getString(cursor.getColumnIndex("nameCity")), cursor.getString(cursor.getColumnIndex("codeCity")),
                        cursor.getString(cursor.getColumnIndex("nameStation")), cursor.getString(cursor.getColumnIndex("codeStation")),
                        typeStation, cursor.getString(cursor.getColumnIndex("typeTransport"))));
                cursor.moveToNext();
            }

            SQL = "select DISTINCT nameCity, nameRegion, codeRegion, codeCity from codeAllStation where nameCity like '" + nameStation + "%'";
            Cursor cursor2 = db.rawQuery(SQL, null);
            if (cursor2.getCount() == 0) {
                return result;
            } else {
                cursor2.moveToFirst();
                while (!cursor2.isAfterLast()) {
                    result.add(new StationDB(cursor2.getString(cursor2.getColumnIndex("nameRegion")), cursor2.getString(cursor2.getColumnIndex("codeRegion")),
                            cursor2.getString(cursor2.getColumnIndex("nameCity")), cursor2.getString(cursor2.getColumnIndex("codeCity")),
                            cursor2.getString(cursor2.getColumnIndex("nameCity")), cursor2.getString(cursor2.getColumnIndex("codeCity")),
                            "...", "..."));
                    cursor2.moveToNext();
                }
            }

        }
        return result;
    }

    public List<String> getBookMark() {
        String SQL = "select name from sqlite_master \n" +
                "where (name <> \"uid_Reys\") and (name <> \"station_code\") and (name <> \"historyRasp\") and (name <> \"android_metadata\") and (name <> \"sqlite_sequence\") and (name <> \"codeAllStation\")";
        Cursor cursor = db.rawQuery(SQL, null);
        List<String> result = new ArrayList<>();

        if (!cursor.moveToFirst()) {
            return result;
        } else {
            while (cursor.moveToNext()) {
                // result += cursor.getString(cursor.getColumnIndex("station"));
                //Log.d("searchstation", cursor.getString(cursor.getColumnIndex("station")));
                result.add(cursor.getString(cursor.getColumnIndex("name")));
            }
            return result;
        }
    }

    public List<String> getStationOfInput(String nameStation) {
        String SQL = "select * from codeAllStation where nameStation like '" + nameStation + "%'";
        Cursor cursor = db.rawQuery(SQL, null);
        List<String> result = new ArrayList<>();

        if (!cursor.moveToFirst()) {
            return result;
        } else {
            while (cursor.moveToNext()) {
                // result += cursor.getString(cursor.getColumnIndex("station"));
                //Log.d("searchstation", cursor.getString(cursor.getColumnIndex("station")));
                result.add(cursor.getString(cursor.getColumnIndex("nameStation")));
            }
            return result;
        }


        //Cursor cursor = this.db.query("station_code", new String[]{"nameStation"}, "station = ?", new String[]{code}, null, null, null);
//        int count = cursor.getCount();
//        String result = null;
//        if(count <= 0) {
//            return "";
//        } else {
//            while(cursor.moveToNext()) {
//                result = cursor.getString(cursor.getColumnIndex("code"));
//            }
//
//            return result;
//        }
    }
    //-------------------------------------------------///

    public void resetHistiryRasp(String nameReys, String nameFrom, String fromCode, String nameTo, String toCode) {
        if (hashTable("historyRasp")) {
            db.execSQL("DELETE FROM historyRasp WHERE _id = 1;");
            db.execSQL("insert into historyRasp (nameReys, nameFrom, fromCode, nameTo, toCode) " +
                    "values('" + nameReys + "', '" + nameFrom + "', '" + fromCode + "', '" + nameTo + "', '" + toCode + "');");
        } else {
            Log.d("historyRasp ", "else create");
            String SQL = "CREATE TABLE historyRasp (_id INTEGER PRIMARY KEY, nameReys , nameFrom VARCHAR, fromCode VARCHAR, nameTo VARCHAR, toCode VARCHAR);";
            db.execSQL(SQL);
            db.execSQL("insert into historyRasp (nameReys, nameFrom, fromCode, nameTo, toCode) " +
                    "values('" + nameReys + "', '" + nameFrom + "', '" + fromCode + "', '" + nameTo + "', '" + toCode + "');");
        }
    }

    public boolean hashHistoryRasp() {
        if (hashTable("historyRasp")) {
            return true;
        } else
            return false;
    }

    private static class DbHelper extends SQLiteOpenHelper {
        private Context context;

        public DbHelper(Context c) {
            super(c, "list_station.db", null, DB_VERSION);
            this.context = c;
            if (Build.VERSION.SDK_INT >= 17) {
                DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
                new File(DB_PATH).mkdir();
            } else {
                DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
                new File(DB_PATH).mkdir();
            }

            if (!checkDataBase()) {
                try {
                    copyDBFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Log.d("DATABASES", "YES");
            }
        }

        public boolean checkDataBase() {
            return (new File(DB_PATH + "list_station.db")).exists();
        }

        public void copyDBFile() throws IOException {
            InputStream mInput = this.context.getAssets().open(DB_NAME);
            //InputStream mInput = mContext.getResources().openRawResource(R.raw.info);
            OutputStream mOutput = new FileOutputStream(DB_PATH + DB_NAME);
            byte[] mBuffer = new byte[1024];
            int mLength;
            while ((mLength = mInput.read(mBuffer)) > 0)
                mOutput.write(mBuffer, 0, mLength);
            mOutput.flush();
            mOutput.close();
            mInput.close();
        }

        private void copyDataBase() {
            InputStream myInput = null;
            OutputStream myOutput = null;
            try {
                myInput = context.getAssets().open(DB_NAME);
                myOutput = new FileOutputStream(DB_PATH + "list_station.db");
                byte[] buffer = new byte[1024];
                int length;
                while ((length = myInput.read(buffer)) > 0) {
                    myOutput.write(buffer, 0, length);
                }

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            } finally {
                try {
                    myOutput.flush();
                    myOutput.close();
                    myInput.close();
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage());
                }
            }

        }

        public void onCreate(SQLiteDatabase var1) {
        }

        public void onUpgrade(SQLiteDatabase var1, int var2, int var3) {
        }
    }


}
