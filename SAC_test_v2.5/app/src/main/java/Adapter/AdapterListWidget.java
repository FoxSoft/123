package Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.eminem.sac.R;

import java.util.List;

import Model.Widget;

/**
 * Created by eminem on 31.12.2017.
 */

public class AdapterListWidget extends BaseAdapter {
    private List<Widget> widgets;
    private Context context;

    public AdapterListWidget(List<Widget> widgets, Context context) {
        this.widgets = widgets;
        this.context = context;
    }

    @Override
    public int getCount() {
        return widgets.size();
    }

    @Override
    public Object getItem(int i) {
        return widgets.get(i);
    }

    @Override
    public long getItemId(int i) {
        return widgets.get(i).getId();
    }

    @Override
    public View getView(int n, View view, ViewGroup viewGroup) {
        View v = view;
        ViewHolder viewHolder;
        if(view == null) {
            v = View.inflate(this.context, R.layout.item_widget, null);
            viewHolder = new ViewHolder(v);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)view.getTag();
        }


        viewHolder.TFromTime.setText(widgets.get(n).getFromTime());
        viewHolder.TToTime.setText(widgets.get(n).getToTime());
        viewHolder.TDuration.setText(widgets.get(n).getDuration());
        viewHolder.TNameReys.setText((widgets.get(n)).getNameReys());
        viewHolder.TStatus.setText(widgets.get(n).getStatus());
        viewHolder.Img.setImageResource(widgets.get(n).getIdImg());
        
        return v;
        
    }
    private class ViewHolder {
        public ImageView Img;
        public TextView TDays;
        public TextView TDuration;
        public TextView TFrom;
        public TextView TFromTime;
        public TextView TNameReys;
        public TextView TNumber;
        public TextView TStatus;
        public TextView TTo;
        public TextView TToTime;


        public ViewHolder(View view) {
           //this.TFrom = (TextView)view.findViewById(R.id.fromStation);
            //this.TTo = (TextView)view.findViewById(R.id.toStation);
            this.TToTime = (TextView)view.findViewById(R.id.toTime);
            this.TFromTime = (TextView)view.findViewById(R.id.fromTime);
            this.TStatus = (TextView)view.findViewById(R.id.status);
           // this.TDays = (TextView)view.findViewById(R.id.days);
            this.TDuration = (TextView)view.findViewById(R.id.duration);
            this.TNameReys = (TextView)view.findViewById(R.id.nameReys);
            //this.TNumber = (TextView)view.findViewById(R.id.number);
            this.Img = (ImageView)view.findViewById(R.id.img);
        }
    }
}
