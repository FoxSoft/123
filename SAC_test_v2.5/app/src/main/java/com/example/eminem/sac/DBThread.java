package com.example.eminem.sac;

import android.content.Context;
import android.util.Log;

import java.util.Timer;

/**
 * Created by eminem on 22.11.2017.
 */

public class DBThread extends Thread {

    private final Timer timer;
    private DBAdapter db;
    private Context context;
    @Override
    public synchronized void start() {
        super.start();

    }

    @Override
    public void run() {
        System.out.printf("Поток %s начал работу... \n", Thread.currentThread().getName());
        try{

            Thread.sleep(500);
            timer.cancel();
        }
        catch(InterruptedException e){
            System.out.println("Поток прерван");
        }
        System.out.printf("Поток %s завершил работу... \n", Thread.currentThread().getName());
    }

    public DBThread(Context context, Timer timer) {
        this.context = context;
        this.timer = timer;
    }
}
