package com.example.eminem.sac;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import java.util.ArrayList;
import java.util.List;

public class TestActivity extends AppCompatActivity {

    private AutoCompleteTextView mAutoCompleteTextView;
    private List<String> hint;
    private DBAdapter dbAdapter;
    private ArrayAdapter<String> arrayAdapter;
    private List<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        dbAdapter = new DBAdapter(getApplicationContext());

        mAutoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        hint = new ArrayList<>();
        hint.add("Зарайск");
        hint.add("Зараза");
        hint.add("Запор");
        hint.add("Замарозки");
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, hint);
        mAutoCompleteTextView.setAdapter(arrayAdapter);

        list = new ArrayList<>();
        list.clear();
        hint.clear();
        list = dbAdapter.getNameStationListOfSearch(mAutoCompleteTextView.getText().toString());
        for (int i = 0; i < list.size(); i++)
        {
            hint.add(list.get(i));
        }
        Log.d("countSearch", String.valueOf(hint));
        arrayAdapter.notifyDataSetChanged();
        mAutoCompleteTextView.setAdapter(arrayAdapter);


        mAutoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
              //  list.clear();
                hint.clear();
                list = dbAdapter.getNameStationListOfSearch(mAutoCompleteTextView.getText().toString());
                for (int i = 0; i < list.size(); i++)
                {
                    hint.add(list.get(i));
                }
                Log.d("countSearch", String.valueOf(hint));
                arrayAdapter.notifyDataSetChanged();
                mAutoCompleteTextView.setAdapter(arrayAdapter);

                Log.d("countSearch", list.size() + " - " + hint.size());
            }
        });
        Log.d("ActivityTesting", Yandex.getTypeStation("train_station"));
    }
}
