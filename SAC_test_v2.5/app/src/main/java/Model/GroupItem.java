package Model;

/**
 * Created by eminem on 12.11.2017.
 */

public class GroupItem {
    private String uid;
    private String nameReys;
    private String number;
    private String typeTransport;
    private int idImg;
    private int id;

    public GroupItem() {
    }

    public GroupItem(String uid, String nameReys, String number, String typeTransport, int idImg, int id) {
        this.uid = uid;
        this.nameReys = nameReys;
        this.number = number;
        this.typeTransport = typeTransport;
        this.idImg = idImg;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getIdImg() {
        return idImg;
    }

    public void setIdImg(int idImg) {
        this.idImg = idImg;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNameReys() {
        return nameReys;
    }

    public void setNameReys(String nameReys) {
        this.nameReys = nameReys;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTypeTransport() {
        return typeTransport;
    }

    public void setTypeTransport(String typeTransport) {
        this.typeTransport = typeTransport;
    }
}
