package com.example.eminem.sac;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;

public class Helper {
   public static void getListViewSize(ListView listView) {
      ListAdapter listAdapter = listView.getAdapter();
      if(listAdapter != null) {
         int n = 0;

         for(int i = 0; i < listAdapter.getCount(); ++i) {
            View view = listAdapter.getView(i, null, listView);
            view.measure(0, 0);
            n = (int)(n + view.getMeasuredHeightAndState() + 0.35D * view.getMeasuredHeightAndState());
         }

         LayoutParams layoutParams = listView.getLayoutParams();
         layoutParams.height = n + listView.getDividerHeight() * (-1 + listAdapter.getCount());
         listView.setLayoutParams(layoutParams);
         Log.i("height of listItem:", String.valueOf(n));
      }
   }
}
