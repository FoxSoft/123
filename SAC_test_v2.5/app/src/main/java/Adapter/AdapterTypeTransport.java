package Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.eminem.sac.AdapterBus;
import com.example.eminem.sac.Bus;
import com.example.eminem.sac.R;
import com.xw.repo.BubbleSeekBar;

import java.util.List;

import Model.TypeTransport;

/**
 * Created by eminem on 25.12.2017.
 */

public class AdapterTypeTransport extends BaseAdapter {
    private Context context;
    private List<TypeTransport> transportList;

    public AdapterTypeTransport(Context context, List<TypeTransport> transportList) {
        this.context = context;
        this.transportList = transportList;
    }

    @Override
    public int getCount() {
        return transportList.size();
    }

    @Override
    public Object getItem(int i) {
        return transportList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return transportList.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        ViewHolder viewHolder;
        if(view == null) {
            v = View.inflate(this.context, R.layout.row_spinner_type_transport, null);
            viewHolder = new ViewHolder(v);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)view.getTag();
        }

        viewHolder.title.setText(transportList.get(i).getTitle());
        viewHolder.Img.setImageResource(transportList.get(i).getImg());
        return v;
    }

    private class ViewHolder {
        public ImageView Img;
        public TextView title;

        public ViewHolder(View view) {
            this.title = (TextView)view.findViewById(R.id.title_transport);
            this.Img = (ImageView)view.findViewById(R.id.img);

        }
    }
}
