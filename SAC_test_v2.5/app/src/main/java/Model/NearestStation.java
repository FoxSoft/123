package Model;

/**
 * Created by eminem on 25.12.2017.
 */

public class NearestStation {
    private String distance;
    private String code;
    private String title;
    private String stationType;
    private String majority;
    private String lat;
    private String lng;
    private String transportType;
    private String stationTypeName;
    private String touchUrl;
    private int typeTransportImg;
    public NearestStation() {
    }

    public NearestStation(String distance, String code, String title, String stationType, String majority, String lat, String lng,
                          String transportType, String stationTypeName, String touchUrl, int typeTransportImg) {
        this.distance = distance;
        this.code = code;
        this.title = title;
        this.stationType = stationType;
        this.majority = majority;
        this.lat = lat;
        this.lng = lng;
        this.transportType = transportType;
        this.stationTypeName = stationTypeName;
        this.touchUrl = touchUrl;
        this.typeTransportImg = typeTransportImg;
    }

    public String getDistance() {
        return distance;
    }

    public void clear()
    {
        this.distance = "";
        this.code = "";
        this.title = "";
        this.stationType = "";
        this.majority = "";
        this.lat = "";
        this.lng = "";
        this.transportType = "";
        this.stationTypeName = "";
        this.touchUrl = "";
        this.typeTransportImg = 0;
    }
    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStationType() {
        return stationType;
    }

    public void setStationType(String stationType) {
        this.stationType = stationType;
    }

    public String getMajority() {
        return majority;
    }

    public void setMajority(String majority) {
        this.majority = majority;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getTransportType() {
        return transportType;
    }

    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    public String getStationTypeName() {
        return stationTypeName;
    }

    public void setStationTypeName(String stationTypeName) {
        this.stationTypeName = stationTypeName;
    }

    public String getTouchUrl() {
        return touchUrl;
    }

    public void setTouchUrl(String touchUrl) {
        this.touchUrl = touchUrl;
    }

    public int getTypeTransportImg() {
        return typeTransportImg;
    }

    public void setTypeTransportImg(int typeTransportImg) {
        this.typeTransportImg = typeTransportImg;
    }

    @Override
    public String toString() {
        return "NearestStation{" +
                "distance='" + distance + '\'' +
                ", code='" + code + '\'' +
                ", title='" + title + '\'' +
                ", stationType='" + stationType + '\'' +
                ", majority='" + majority + '\'' +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                ", transportType='" + transportType + '\'' +
                ", stationTypeName='" + stationTypeName + '\'' +
                ", touchUrl='" + touchUrl + '\'' +
                ", typeTransportImg=" + typeTransportImg +
                '}';
    }
}
