package com.example.eminem.sac;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;
import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;

import java.util.ArrayList;
import java.util.List;

import Adapter.AdapterExpandelList;
import Model.ChildsItem;
import Model.GroupItem;

/**
 * Created by eminem on 30.12.2017.
 */

public class Tab1 extends Fragment{

    private final String TAG = "MainActivity";
    private ArrayList<GroupItem> parentItems = new ArrayList<GroupItem>();
    private ArrayList<Object> childItems = new ArrayList<Object>();
    private LayoutInflater inflater;
    private AdapterExpandelList adapter;
    private DBAdapter dbAdapter;
    private  List<String> strings;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab1_fragment, container, false);

        FloatingGroupExpandableListView list = (FloatingGroupExpandableListView) view.findViewById(R.id.EXPlistview);

        inflater = getLayoutInflater();

        View header = inflater.inflate(R.layout.sample_activity_list_header, list, false);
        list.addHeaderView(header);

        dbAdapter = new DBAdapter(getContext());
        strings = new ArrayList<>();
        strings = dbAdapter.getBookMark();

        View footer = inflater.inflate(R.layout.sample_activity_list_footer, list, false);
        footer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/diegocarloslima/FloatingGroupExpandableListView"));
                startActivity(intent);
            }
        });
        list.addFooterView(footer);

        list.setChildDivider(new ColorDrawable(Color.BLACK));

        setGroupParents();

        setChildData();

        adapter = new AdapterExpandelList(getContext(), parentItems, childItems);

        WrapperExpandableListAdapter wrapperAdapter = new WrapperExpandableListAdapter(adapter);
        list.setAdapter(wrapperAdapter);


        for(int i = 0; i < wrapperAdapter.getGroupCount(); i++) {
            list.expandGroup(i);
        }

        list.setOnScrollFloatingGroupListener(new FloatingGroupExpandableListView.OnScrollFloatingGroupListener() {

            @Override
            public void onScrollFloatingGroupListener(View floatingGroupView, int scrollY) {
                float interpolation = - scrollY / (float) floatingGroupView.getHeight();

                // Changing from RGB(162,201,85) to RGB(255,255,255)
                final int greenToWhiteRed = (int) (162 + 93 * interpolation);
                final int greenToWhiteGreen = (int) (201 + 54 * interpolation);
                final int greenToWhiteBlue = (int) (85 + 170 * interpolation);
                final int greenToWhiteColor = Color.argb(255, greenToWhiteRed, greenToWhiteGreen, greenToWhiteBlue);

                // Changing from RGB(255,255,255) to RGB(0,0,0)
                final int whiteToBlackRed = (int) (255 - 255 * interpolation);
                final int whiteToBlackGreen = (int) (255 - 255 * interpolation);
                final int whiteToBlackBlue = (int) (255 - 255 * interpolation);
                final int whiteToBlackColor = Color.argb(255, whiteToBlackRed, whiteToBlackGreen, whiteToBlackBlue);

                final ImageView image = (ImageView) floatingGroupView.findViewById(R.id.sample_activity_list_group_item_image);
                image.setBackgroundColor(greenToWhiteColor);

                final Drawable imageDrawable = image.getDrawable().mutate();
                imageDrawable.setColorFilter(whiteToBlackColor, PorterDuff.Mode.SRC_ATOP);

                final View background = floatingGroupView.findViewById(R.id.sample_activity_list_group_item_background);
                background.setBackgroundColor(greenToWhiteColor);

                final TextView text = (TextView) floatingGroupView.findViewById(R.id.sample_activity_list_group_item_text);
                text.setTextColor(whiteToBlackColor);

                final ImageView expanded = (ImageView) floatingGroupView.findViewById(R.id.sample_activity_list_group_expanded_image);
                final Drawable expandedDrawable = expanded.getDrawable().mutate();
                expandedDrawable.setColorFilter(whiteToBlackColor, PorterDuff.Mode.SRC_ATOP);
            }
        });
        return view;
    }

    private void setChildData() {
//        ArrayList<ChildsItem> child = new ArrayList<>();
//        child.add(new ChildsItem("autovokzal", "bus_station", "132", "123", "123",
//                "77", "dd", 0, 0));
//        child.add(new ChildsItem("novoselki", "bus_station", "132", "123", "123",
//                "77", "dd", 0, 1));
//        child.add(new ChildsItem("bugor", "bus_station", "132", "123", "123",
//                "77", "dd", 0, 2));
//        child.add(new ChildsItem("astapovo", "bus_station", "132", "123", "123",
//                "77", "dd", 0, 3));
//        childItems.add(child);
//
//        child = new ArrayList<>();
//        child.add(new ChildsItem("bank", "bus_station", "132", "123", "123",
//                "77", "dd", 1, 0));
//        child.add(new ChildsItem("perekrestok", "bus_station", "132", "123", "123",
//                "77", "dd", 1, 1));
//        child.add(new ChildsItem("vokzal", "bus_station", "132", "123", "123",
//                "77", "dd", 1, 2));
//        childItems.add(child);
        ArrayList<ChildsItem> child;
        for (int i = 0; i < strings.size(); i++)
        {
            child = new ArrayList<>();
            for(int j = 0; j < 20; j++) {
                child.add(new ChildsItem("vokzal", "bus_station", "132", "123", "123",
                        "77", "dd", i, j));
            }
            childItems.add(child);
        }
    }

    private void setGroupParents() {
      //  parentItems.add(new GroupItem("7897", "z-k", "31", "bus", R.drawable.reys, 0));
     //   parentItems.add(new GroupItem("4234", "z-l", "24", "bus", R.drawable.reys, 1));

        for (int i = 0; i < strings.size(); i++)
        {
            parentItems.add(new GroupItem("", strings.get(i), "", "", R.drawable.reys, i));
        }
    }
}
