package com.example.eminem.sac;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class ActivityShudle extends AppCompatActivity {
    private ProgressDialog progressDialog;
    private Download download;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shudle);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMax(100);
        progressDialog.setTitle("Загрузка рассписания");
        progressDialog.setMessage("Соединение с сервером...");


        download = new Download(getApplicationContext(), "s9857588", null, null);
        if (Download.isOnline2(getApplicationContext())) {
            download.execute();
            refresh();
        } else Log.d("schedule_parse", "no net");
    }


    private void refresh() {
        Timer t = new Timer();
        Timer tim = new Timer();
        final DBThread thread = new DBThread(getApplicationContext(), tim);
        tim.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (!download.isSet) {
                            progressDialog.show();
                            Log.d("shedule", "-");
                        } else {
                            progressDialog.dismiss();
                            Log.d("shedule", "+");
                            thread.start();
                        }
                    }
                });
            }
        }, 0, 1000);

    }
}
