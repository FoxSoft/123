package com.example.eminem.sac;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;

import Model.NearestStation;
import Model.Settlement;

/**
 * Created by eminem on 24.12.2017.
 */

public class Download extends AsyncTask<Void, Void, Void> {

    private boolean isDownload = false;
    private String titleMessage;
    public DBAdapter dbAdapter;
    private String nameReysTable;

    private Context context;
    private String url = "";
    private Yandex.API api;
    private Document doc;
    //-----------------------------------//
    private double lat;
    private double lng;
    private double distance;
    public String test;
    public boolean isSet = false;
    public Settlement settlement;
    //---------------------------//
    public NearestStation nearestStation;
    public List<NearestStation> stations;
    //---------------------------------//
    public Yandex.SearchAPI searchAPI;

    //---------------------------//
    public Download(Context context, double lat, double lng, double distance) {
        this.context = context;
        this.api = Yandex.API.nearest_settlement;
        this.lat = lat;
        this.lng = lng;
        this.distance = distance;
        this.settlement = new Settlement();

        url = "https://api.rasp.yandex.net/v3.0/nearest_settlement/?apikey=86973449-b547-417e-95da-e70f87ef8748&format=xml" +
                "&lat=" + lat + "&lng=" + lng + "&distance=" + distance;
        Log.d("url", url);
    }

    public Download(Context context, double lat, double lng, double distance, Yandex.STATION_TYPE stationType, Yandex.TRANSPORT_TYPE transportType) {
        this.context = context;
        this.api = Yandex.API.nearest_stations;
        this.lat = lat;
        this.lng = lng;
        this.distance = distance;
        url = "https://api.rasp.yandex.net/v3.0/nearest_stations/?apikey=86973449-b547-417e-95da-e70f87ef8748&format=xml" +
                "&lat=" + lat + "&lng=" + lng + "&distance=" + distance + "&limit=5000";
        if (stationType != null)
            url += "&station_types=" + stationType;
        if (transportType != null)
            url += "&transport_types=" + transportType;
        Log.d("url", url);
    }

    //thread
    public Download(Context context, String codeStation, Yandex.EVENT event, Yandex.TRANSPORT_TYPE transportType) {
        this.context = context;
        this.api = Yandex.API.schedule;
        url = "https://api.rasp.yandex.net/v3.0/schedule/?apikey=86973449-b547-417e-95da-e70f87ef8748&format=xml&limit=5000&station=" + codeStation;
        if (event != null)
            url += "&event=" + event;

        if (transportType != null)
            url += "&transport_types=" + transportType;
        Log.d("url", url);
    }

    //thread
    public Download(Context context, String uid) {
        this.context = context;
        this.api = Yandex.API.thread;
        url = "https://api.rasp.yandex.net/v3.0/thread/?apikey=86973449-b547-417e-95da-e70f87ef8748&format=xml&limit=5000&uid=" + uid;
        Log.d("url", url);
    }

    //search
    public Download(Context context, String from, String to, Yandex.TRANSPORT_TYPE transportType, boolean transfers, String nameReysTable) {
        this.context = context;
        this.api = Yandex.API.search;
        this.nameReysTable = nameReysTable;
        url = "https://api.rasp.yandex.net/v3.0/search/?apikey=86973449-b547-417e-95da-e70f87ef8748&format=xml&limit=5000&from=" + from + "&to=" + to;
        if (transportType != null)
            url += "&transport_types=" + transportType;
        if (transfers)
            url += "&transfers=" + transfers;
        Log.d("url", url);
        searchAPI = new Yandex.SearchAPI();
        dbAdapter = new DBAdapter(context);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new URL(url).openStream()));
            doc.getDocumentElement().normalize();
            doc.getDocumentElement().getTagName();
            isDownload = true;
        } catch (Exception e) {
            Log.d("download", "error network");
            isDownload = false;
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        //
        super.onPreExecute();
    }

    @SuppressLint("LongLogTag")
    @Override
    protected void onPostExecute(Void aVoid) {
        switch (api) {
            case nearest_settlement:

                if (isDownload) {
                    try {
//                        Log.d("distance", doc.getDocumentElement().getElementsByTagName("distance").item(0).getTextContent());
//                        test = doc.getDocumentElement().getElementsByTagName("distance").item(0).getTextContent() + "\n";
//                        Log.d("code", doc.getDocumentElement().getElementsByTagName("code").item(0).getTextContent());
//                        test += doc.getDocumentElement().getElementsByTagName("code").item(0).getTextContent() + "\n";
//                        Log.d("title", doc.getDocumentElement().getElementsByTagName("title").item(0).getTextContent());
//                        test += doc.getDocumentElement().getElementsByTagName("title").item(0).getTextContent() + "\n";
//                        Log.d("lat", doc.getDocumentElement().getElementsByTagName("lat").item(0).getTextContent());
//                        test += doc.getDocumentElement().getElementsByTagName("lat").item(0).getTextContent() + "\n";
//                        Log.d("lng", doc.getDocumentElement().getElementsByTagName("lng").item(0).getTextContent());
//                        test += doc.getDocumentElement().getElementsByTagName("lng").item(0).getTextContent() + "\n";
//                        Log.d("type", doc.getDocumentElement().getElementsByTagName("type").item(0).getTextContent());
//                        test += doc.getDocumentElement().getElementsByTagName("type").item(0).getTextContent() + "\n";
//                        Log.d("popular_title", doc.getDocumentElement().getElementsByTagName("popular_title").item(0).getTextContent());
//                        test += doc.getDocumentElement().getElementsByTagName("popular_title").item(0).getTextContent() + "\n";
//                        Log.d("short_title", doc.getDocumentElement().getElementsByTagName("short_title").item(0).getTextContent());
//                        test += doc.getDocumentElement().getElementsByTagName("short_title").item(0).getTextContent() + "\n";

                        settlement = new Settlement(doc.getDocumentElement().getElementsByTagName("distance").item(0).getTextContent().substring(0, 4) + " км",
                                doc.getDocumentElement().getElementsByTagName("code").item(0).getTextContent(),
                                Double.parseDouble(doc.getDocumentElement().getElementsByTagName("lat").item(0).getTextContent()),
                                Double.parseDouble(doc.getDocumentElement().getElementsByTagName("lng").item(0).getTextContent()),
                                doc.getDocumentElement().getElementsByTagName("type").item(0).getTextContent(),
                                doc.getDocumentElement().getElementsByTagName("title").item(0).getTextContent(),
                                doc.getDocumentElement().getElementsByTagName("popular_title").item(0).getTextContent(),
                                doc.getDocumentElement().getElementsByTagName("short_title").item(0).getTextContent(), 1);
                        Log.d("test", test);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    titleMessage = "готово";
                } else
                    titleMessage = "выберите радиус поиска больше";
                break;
            case nearest_stations:
                if (isDownload) {
                    stations = new ArrayList<>();
                    NodeList scheduleList = doc.getElementsByTagName("station");
                    for (int i = 0; i < scheduleList.getLength(); i++) {
                        Element station = (Element) scheduleList.item(i);

                        this.nearestStation = new NearestStation();

                        Log.d("nearest_stations", station.getElementsByTagName("distance").item(0).getTextContent().substring(0, 4) + " км");
                        nearestStation.setDistance(station.getElementsByTagName("distance").item(0).getTextContent().substring(0, 4) + " км");

                        Log.d("nearest_stations", station.getElementsByTagName("code").item(0).getTextContent());
                        nearestStation.setCode(station.getElementsByTagName("code").item(0).getTextContent());

                        Log.d("nearest_stations", station.getElementsByTagName("title").item(0).getTextContent());
                        nearestStation.setTitle(station.getElementsByTagName("title").item(0).getTextContent());

                        Log.d("nearest_stations", station.getElementsByTagName("station_type").item(0).getTextContent());
                        nearestStation.setStationType(station.getElementsByTagName("station_type").item(0).getTextContent());

                        Log.d("nearest_stations", station.getElementsByTagName("majority").item(0).getTextContent());
                        nearestStation.setMajority(station.getElementsByTagName("majority").item(0).getTextContent());

                        Log.d("nearest_stations", station.getElementsByTagName("transport_type").item(0).getTextContent());
                        nearestStation.setTransportType(station.getElementsByTagName("transport_type").item(0).getTextContent());

                        Log.d("nearest_stations", station.getElementsByTagName("lat").item(0).getTextContent());
                        nearestStation.setLat(station.getElementsByTagName("lat").item(0).getTextContent());

                        Log.d("nearest_stations", station.getElementsByTagName("lng").item(0).getTextContent());
                        nearestStation.setLng(station.getElementsByTagName("lng").item(0).getTextContent());

                        Log.d("nearest_stations", station.getElementsByTagName("type").item(0).getTextContent());

                        Log.d("nearest_stations", station.getElementsByTagName("station_type_name").item(0).getTextContent());
                        nearestStation.setStationTypeName(station.getElementsByTagName("station_type_name").item(0).getTextContent());

                        nearestStation.setTypeTransportImg(R.drawable.bus_logo);
                        NodeList touch_urlList = station.getElementsByTagName("touch_url");
                        for (int j = 0; j < touch_urlList.getLength(); j++) {
                            Log.d("nearest_stations", touch_urlList.item(j).getTextContent());
                        }

                        stations.add(nearestStation);
                    }

                    Log.d("nearest_stations", String.valueOf(stations.size()));

                    titleMessage = "готово";
                } else
                    titleMessage = "выберите радиус поиска больше";
                break;
            case schedule:
                if (isDownload) {
                    Log.d("schedule_parse", doc.getDocumentElement().getElementsByTagName("total").item(0).getTextContent());
                    NodeList scheduleList = doc.getElementsByTagName("schedule");
                    for (int i = 0; i < scheduleList.getLength(); i++) {
                        Element schedule = (Element) scheduleList.item(i);
                        Log.d("schedule_parse", "================schedule==============");
                        Log.d("schedule_parse", schedule.getElementsByTagName("arrival").item(0).getTextContent());
                        Log.d("schedule_parse", schedule.getElementsByTagName("platform").item(0).getTextContent());
                        Log.d("schedule_parse", schedule.getElementsByTagName("days").item(0).getTextContent());
                        Log.d("schedule_parse", schedule.getElementsByTagName("stops").item(0).getTextContent());
                        Log.d("schedule_parse", schedule.getElementsByTagName("departure").item(0).getTextContent());
                        Log.d("schedule_parse", schedule.getElementsByTagName("terminal").item(0).getTextContent());
                        Log.d("schedule_parse", schedule.getElementsByTagName("is_fuzzy").item(0).getTextContent());

                        Log.d("schedule_parse", "================thread==============");
                        //  Element thread = (Element) schedule.getChildNodes();
                        Element thread = (Element) schedule.getElementsByTagName("thread").item(0);
                        Log.d("schedule_parse", thread.getElementsByTagName("uid").item(0).getTextContent());
                        Log.d("schedule_parse", thread.getElementsByTagName("title").item(0).getTextContent());
                        Log.d("schedule_parse", thread.getElementsByTagName("number").item(0).getTextContent());
                        Log.d("schedule_parse", thread.getElementsByTagName("short_title").item(0).getTextContent());
                        Log.d("schedule_parse", thread.getElementsByTagName("express_type").item(0).getTextContent());

                        Log.d("schedule_parse", "================carrier==============");
                        Element carrier = (Element) thread.getElementsByTagName("carrier").item(0);
                        Log.d("schedule_parse", thread.getElementsByTagName("code").item(0).getTextContent());
                        Log.d("schedule_parse", thread.getElementsByTagName("title").item(0).getTextContent());
                        Log.d("schedule_parse", thread.getElementsByTagName("express_type").item(0).getTextContent());
                        Log.d("schedule_parse", thread.getElementsByTagName("express_type").item(0).getTextContent());
                        Log.d("schedule_parse", thread.getElementsByTagName("express_type").item(0).getTextContent());

                        Log.d("schedule_parse", "================transport_subtype==============");
                        Element transport_subtype = (Element) thread.getElementsByTagName("transport_subtype").item(0);
                        Log.d("schedule_parse", thread.getElementsByTagName("color").item(0).getTextContent());
                        Log.d("schedule_parse", thread.getElementsByTagName("code").item(0).getTextContent());
                        Log.d("schedule_parse", thread.getElementsByTagName("title").item(0).getTextContent());
                    }
                    titleMessage = "готово";
                } else titleMessage = "оишибка...";
                break;
            case thread:
                if (isDownload) {
                    Log.d("thread_parse", doc.getElementsByTagName("except_days").item(0).getTextContent());
                    Log.d("thread_parse", doc.getElementsByTagName("arrival_date").item(0).getTextContent());
                    Log.d("thread_parse", doc.getElementsByTagName("uid").item(0).getTextContent());
                    Log.d("thread_parse", doc.getElementsByTagName("start_time").item(0).getTextContent());
                    Log.d("thread_parse", doc.getElementsByTagName("departure_date").item(0).getTextContent());
                    Log.d("thread_parse", doc.getElementsByTagName("arrival_date").item(0).getTextContent());
                    Log.d("thread_parse", doc.getElementsByTagName("start_date").item(0).getTextContent());
                    Log.d("thread_parse", doc.getElementsByTagName("number").item(0).getTextContent());
                    Log.d("thread_parse", doc.getElementsByTagName("short_title").item(0).getTextContent());
                    Log.d("thread_parse", doc.getElementsByTagName("days").item(0).getTextContent());
                    Log.d("thread_parse", doc.getElementsByTagName("to").item(0).getTextContent());
                    Log.d("thread_parse", doc.getElementsByTagName("carrier").item(0).getTextContent());
                    Log.d("thread_parse", doc.getElementsByTagName("transport_type").item(0).getTextContent());

                    NodeList stopList = doc.getElementsByTagName("stop");
                    for (int i = 0; i < stopList.getLength(); i++) {
                        Element stop = (Element) stopList.item(i);
                        Log.d("thread_parse", stop.getElementsByTagName("arrival").item(0).getTextContent());
                        Log.d("thread_parse", stop.getElementsByTagName("departure").item(0).getTextContent());
                        Log.d("thread_parse", stop.getElementsByTagName("terminal").item(0).getTextContent());
                        Log.d("thread_parse", stop.getElementsByTagName("platform").item(0).getTextContent());
                        Log.d("thread_parse", stop.getElementsByTagName("stop_time").item(0).getTextContent());
                        Log.d("thread_parse", stop.getElementsByTagName("duration").item(0).getTextContent());

                        Element station = (Element) stop.getElementsByTagName("station").item(0);
                        Log.d("thread_parse", stop.getElementsByTagName("code").item(0).getTextContent());
                        Log.d("thread_parse", stop.getElementsByTagName("station_type").item(0).getTextContent());
                        Log.d("thread_parse", stop.getElementsByTagName("title").item(0).getTextContent());
                        Log.d("thread_parse", stop.getElementsByTagName("popular_title").item(0).getTextContent());
                        Log.d("thread_parse", stop.getElementsByTagName("transport_type").item(0).getTextContent());
                        Log.d("thread_parse", stop.getElementsByTagName("type").item(0).getTextContent());
                    }

                    titleMessage = "готово";
                } else titleMessage = "оишибка...";
                break;
            case search:
                if (isDownload) {

                    Log.d("search_parse", doc.getDocumentElement().getElementsByTagName("total").item(0).getTextContent());
                    searchAPI.getPagination().setTotal(doc.getDocumentElement().getElementsByTagName("total").item(0).getTextContent());

                    Element search = (Element) doc.getDocumentElement().getElementsByTagName("search").item(0);
                    Log.d("search_parse", "============to===============");
                    Log.d("search_parse", search.getElementsByTagName("code").item(0).getChildNodes().item(0).getNodeValue());
                    searchAPI.getSearch().getToSearch().setCode(search.getElementsByTagName("code").item(0).getChildNodes().item(0).getNodeValue());
                    Log.d("search_parse", search.getElementsByTagName("type").item(0).getChildNodes().item(0).getNodeValue());
                    searchAPI.getSearch().getToSearch().setType(Yandex.setType(search.getElementsByTagName("type").item(0).getChildNodes().item(0).getNodeValue()));
                    try {
                        Log.d("search_parse", search.getElementsByTagName("popular_title").item(0).getChildNodes().item(0).getTextContent());
                        searchAPI.getSearch().getToSearch().setPopularTitle(search.getElementsByTagName("popular_title").item(0).getChildNodes().item(0).getNodeValue());
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    Log.d("search_parse", search.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
                    searchAPI.getSearch().getToSearch().setTitle(search.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
                    try {
                        Log.d("search_parse", search.getElementsByTagName("short_title").item(0).getChildNodes().item(0).getNodeValue());
                        searchAPI.getSearch().getToSearch().setShortTitle(search.getElementsByTagName("short_title").item(0).getChildNodes().item(0).getNodeValue());
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    Log.d("search_parse", "============from===============");
                    Log.d("search_parse", search.getElementsByTagName("code").item(1).getChildNodes().item(0).getNodeValue());
                    searchAPI.getSearch().getFromSearch().setCode(search.getElementsByTagName("code").item(1).getChildNodes().item(0).getNodeValue());
                    Log.d("search_parse", search.getElementsByTagName("type").item(1).getChildNodes().item(0).getNodeValue());
                    searchAPI.getSearch().getFromSearch().setType(Yandex.setType(search.getElementsByTagName("type").item(1).getChildNodes().item(0).getNodeValue()));
                    Log.d("search_parse", search.getElementsByTagName("popular_title").item(1).getChildNodes().item(0).getNodeValue());
                    searchAPI.getSearch().getFromSearch().setPopularTitle(search.getElementsByTagName("popular_title").item(1).getChildNodes().item(0).getNodeValue());
                    Log.d("search_parse", search.getElementsByTagName("title").item(1).getChildNodes().item(0).getNodeValue());
                    searchAPI.getSearch().getFromSearch().setTitle(search.getElementsByTagName("title").item(1).getChildNodes().item(0).getNodeValue());
                    Log.d("search_parse", search.getElementsByTagName("short_title").item(1).getChildNodes().item(0).getNodeValue());
                    searchAPI.getSearch().getFromSearch().setShortTitle(search.getElementsByTagName("short_title").item(1).getChildNodes().item(0).getNodeValue());

                    NodeList scheduleList = doc.getElementsByTagName("segment");
                    for (int i = 0; i < scheduleList.getLength(); i++) {
                        Yandex.SearchAPI.Segments segmen = new Yandex.SearchAPI.Segments();

                        Element segment = (Element) scheduleList.item(i);
                        Log.d("search_parse", segment.getElementsByTagName("except_days").item(0).getTextContent());
                        segmen.setExceptDays(segment.getElementsByTagName("except_days").item(0).getTextContent());
                        Log.d("search_parse", segment.getElementsByTagName("arrival").item(0).getTextContent());
                        segmen.setArrival(segment.getElementsByTagName("arrival").item(0).getTextContent());
                        Log.d("search_parse", segment.getElementsByTagName("departure").item(0).getTextContent());
                        segmen.setDeparture(segment.getElementsByTagName("departure").item(0).getTextContent());
                        Log.d("search_parse", segment.getElementsByTagName("duration").item(0).getTextContent());
                        segmen.setDuration(segment.getElementsByTagName("duration").item(0).getTextContent());
                        Log.d("search_parse", segment.getElementsByTagName("arrival_terminal").item(0).getTextContent());
                        segmen.setArrivalTerminal(segment.getElementsByTagName("arrival_terminal").item(0).getTextContent());
                        Log.d("search_parse", segment.getElementsByTagName("arrival_platform").item(0).getTextContent());
                        segmen.setArrivalPlatform(segment.getElementsByTagName("arrival_platform").item(0).getTextContent());
                        Log.d("search_parse", segment.getElementsByTagName("departure_platform").item(0).getTextContent());
                        segmen.setDeparturePlatform(segment.getElementsByTagName("departure_platform").item(0).getTextContent());
                        Log.d("search_parse", segment.getElementsByTagName("days").item(0).getTextContent());
                        segmen.setDays(segment.getElementsByTagName("days").item(0).getTextContent());
                        Log.d("search_parse", segment.getElementsByTagName("start_date").item(0).getTextContent());
                        segmen.setStartDate(segment.getElementsByTagName("start_date").item(0).getTextContent());
                        Log.d("search_parse", segment.getElementsByTagName("stops").item(0).getTextContent());
                        segmen.setStops(segment.getElementsByTagName("stops").item(0).getTextContent());

                        Element from = (Element) segment.getElementsByTagName("from").item(0);
                        Log.d("search_parse", from.getElementsByTagName("code").item(0).getTextContent());
                        segmen.getFrom().setCode(from.getElementsByTagName("code").item(0).getTextContent());
                        Log.d("search_parse", from.getElementsByTagName("station_type").item(0).getTextContent());
                        segmen.getFrom().setStationType(Yandex.setTypeStation(from.getElementsByTagName("station_type").item(0).getTextContent()));
                        Log.d("search_parse", from.getElementsByTagName("title").item(0).getTextContent());
                        segmen.getFrom().setTitle(from.getElementsByTagName("title").item(0).getTextContent());
                        Log.d("search_parse", from.getElementsByTagName("popular_title").item(0).getTextContent());
                        segmen.getFrom().setPopularTitle(from.getElementsByTagName("popular_title").item(0).getTextContent());
                        Log.d("search_parse", from.getElementsByTagName("transport_type").item(0).getTextContent());
                        segmen.getFrom().setTransportType(Yandex.setTransportType(from.getElementsByTagName("transport_type").item(0).getTextContent()));
                        Log.d("search_parse", from.getElementsByTagName("station_type_name").item(0).getTextContent());
                        segmen.getFrom().setStationTypeName(from.getElementsByTagName("station_type_name").item(0).getTextContent());
                        Log.d("search_parse", from.getElementsByTagName("type").item(0).getTextContent());
                        segmen.getFrom().setType(Yandex.setType(from.getElementsByTagName("type").item(0).getTextContent()));

                        Element thread = (Element) segment.getElementsByTagName("thread").item(0);

                        Log.d("search_parse_thread", thread.getElementsByTagName("transport_type").item(0).getTextContent());
                        segmen.getThread().setTransportType(Yandex.setTransportType(thread.getElementsByTagName("transport_type").item(0).getTextContent()));
                        Log.d("search_parse_thread", thread.getElementsByTagName("uid").item(0).getTextContent());
                        segmen.getThread().setUid(thread.getElementsByTagName("uid").item(0).getTextContent());
                        Log.d("search_parse_thread", "#" + i + " non     " + thread.getChildNodes().item(4).getTextContent());
                        segmen.getThread().setTitle(thread.getChildNodes().item(4).getTextContent());
                        Log.d("search_parse_thread", thread.getElementsByTagName("vehicle").item(0).getTextContent());
                        segmen.getThread().setVehicle(thread.getElementsByTagName("vehicle").item(0).getTextContent());
                        Log.d("search_parse_thread", thread.getElementsByTagName("number").item(0).getTextContent());
                        segmen.getThread().setNumber(thread.getElementsByTagName("number").item(0).getTextContent());
                        Log.d("search_parse_thread", thread.getElementsByTagName("express_type").item(0).getTextContent());
                        segmen.getThread().setExpressType(Yandex.setExpressType(thread.getElementsByTagName("express_type").item(0).getTextContent()));
                        Log.d("search_parse_thread", thread.getElementsByTagName("thread_method_link").item(0).getTextContent());
                        segmen.getThread().setThreadMethodLink(thread.getElementsByTagName("thread_method_link").item(0).getTextContent());

                        Log.d("search_parse_transport_subtype", thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(0).getTextContent());
                        segmen.getThread().getTransportSubtype().setColor(thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(0).getTextContent());
                        Log.d("search_parse_transport_subtype", thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(1).getTextContent());
                        segmen.getThread().getTransportSubtype().setCode(thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(1).getTextContent());
                        Log.d("search_parse_transport_subtype", thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(2).getTextContent());
                        segmen.getThread().getTransportSubtype().setTitle(thread.getElementsByTagName("transport_subtype").item(0).getChildNodes().item(2).getTextContent());

                        try {
                            Element carrier = (Element) thread.getElementsByTagName("carrier").item(0);
                            Log.d("search_parse", carrier.getElementsByTagName("code").item(0).getTextContent());
                            segmen.getThread().getCarrier().setCode(carrier.getElementsByTagName("code").item(0).getTextContent());
                            Log.d("search_parse_title_carrier", carrier.getElementsByTagName("title").item(0).getChildNodes().item(0).getTextContent());
                            segmen.getThread().getCarrier().setTitle(carrier.getElementsByTagName("title").item(0).getTextContent());
                            Log.d("search_parse", carrier.getElementsByTagName("url").item(0).getTextContent());
                            segmen.getThread().getCarrier().setUrl(carrier.getElementsByTagName("url").item(0).getTextContent());
                            Log.d("search_parse", carrier.getElementsByTagName("phone").item(0).getTextContent());
                            segmen.getThread().getCarrier().setPhone(carrier.getElementsByTagName("phone").item(0).getTextContent());
                            Log.d("search_parse", carrier.getElementsByTagName("address").item(0).getTextContent());
                            segmen.getThread().getCarrier().setAddress(carrier.getElementsByTagName("address").item(0).getTextContent());
                            Log.d("search_parse", carrier.getElementsByTagName("email").item(0).getTextContent());
                            segmen.getThread().getCarrier().setEmail(carrier.getElementsByTagName("email").item(0).getTextContent());
                            Log.d("search_parse", carrier.getElementsByTagName("logo").item(0).getTextContent());
                            segmen.getThread().getCarrier().setLogo(carrier.getElementsByTagName("logo").item(0).getTextContent());
                            Log.d("search_parse", carrier.getElementsByTagName("contacts").item(0).getTextContent());
                            segmen.getThread().getCarrier().setContacts(carrier.getElementsByTagName("contacts").item(0).getTextContent());
                        } catch (Exception e)
                        {
                            e.printStackTrace();
                            Log.d("search_parse", "carrier пустое поле");
                        }

                        Element to = (Element) segment.getElementsByTagName("to").item(0);
                        Log.d("search_parse", to.getElementsByTagName("code").item(0).getTextContent());
                        segmen.getTo().setCode(to.getElementsByTagName("code").item(0).getTextContent());
                        Log.d("search_parse", to.getElementsByTagName("station_type").item(0).getTextContent());
                        segmen.getTo().setStationType(Yandex.setTypeStation(to.getElementsByTagName("station_type").item(0).getTextContent()));
                        Log.d("search_parse", to.getElementsByTagName("title").item(0).getTextContent());
                        segmen.getTo().setTitle(to.getElementsByTagName("title").item(0).getTextContent());
                        Log.d("search_parse", to.getElementsByTagName("popular_title").item(0).getTextContent());
                        segmen.getTo().setPopularTitle(to.getElementsByTagName("popular_title").item(0).getTextContent());
                        Log.d("search_parse", to.getElementsByTagName("transport_type").item(0).getTextContent());
                        segmen.getTo().setTransportType(Yandex.setTransportType(to.getElementsByTagName("transport_type").item(0).getTextContent()));
                        Log.d("search_parse", to.getElementsByTagName("station_type_name").item(0).getTextContent());
                        segmen.getTo().setStationTypeName(to.getElementsByTagName("station_type_name").item(0).getTextContent());
                        Log.d("search_parse", to.getElementsByTagName("type").item(0).getTextContent());
                        segmen.getTo().setType(Yandex.setType(to.getElementsByTagName("type").item(0).getTextContent()));

                        searchAPI.getSegments().add(segmen);
                    }
                    dbAdapter.insertRaspTableV2(nameReysTable, searchAPI);
                    titleMessage = "готово";
                    Log.d("SearchAPI", searchAPI.getSegments().toString());
                } else titleMessage = "оишибка...";
                break;
        }


        Toast.makeText(context, titleMessage, Toast.LENGTH_SHORT).show();
        isSet = true;
        super.onPostExecute(aVoid);
    }

    public boolean isValidNearestSettlement() {
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public static boolean isOnline2(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public Yandex.SearchAPI getsearchAPI() {
        return searchAPI;
    }
}
