package Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eminem.sac.ActivityWebView;
import com.example.eminem.sac.AdapterListView;
import com.example.eminem.sac.Bus;
import com.example.eminem.sac.R;
import com.example.eminem.sac.Yandex;
import com.xw.repo.BubbleSeekBar;

import java.util.List;

/**
 * Created by eminem on 06.02.2018.
 */

public class AdapterListSearch  extends RecyclerView.Adapter<AdapterListSearch.ViewHolder> {

    private Yandex.SearchAPI searchAPI;
    private Context context;
    private LayoutInflater inflater;

    public AdapterListSearch(Context context, Yandex.SearchAPI searchAPI) {
        this.inflater = LayoutInflater.from(context);
        this.searchAPI = searchAPI;
        this.context = context;
    }

    @Override
    public boolean onFailedToRecycleView(AdapterListSearch.ViewHolder holder) {
        return true;
    }

    @Override
    public AdapterListSearch.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row, parent, false);
        return new AdapterListSearch.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterListSearch.ViewHolder viewHolder, int position) {
        viewHolder.itemView.setId(position);
//        viewHolder.itemView.setTag(searchAPI.getSegments().get(position).getId(), "http://ya.ru/"+viewHolder.itemView.getId());

        viewHolder.TNumber.setText(searchAPI.getSegments().get(position).getThread().getNumber());
        viewHolder.TFromTime.setText(searchAPI.getSegments().get(position).getDeparture());
        viewHolder.TFrom.setText(searchAPI.getSegments().get(position).getFrom().getTitle());
        viewHolder.TTo.setText(searchAPI.getSegments().get(position).getTo().getTitle());
        viewHolder.TToTime.setText(searchAPI.getSegments().get(position).getArrival());
        viewHolder.TDuration.setText(searchAPI.getSegments().get(position).getDuration());
        viewHolder.TDays.setText(searchAPI.getSegments().get(position).getDays());
        viewHolder.TNameReys.setText((searchAPI.getSegments().get(position)).getThread().getTitle());
        viewHolder.TStatus.setText(searchAPI.getSegments().get(position).getStatus());
        viewHolder.Img.setImageResource(searchAPI.getSegments().get(position).getIdImg());
        viewHolder.seekBar.setMax(searchAPI.getSegments().get(position).getTrack_max());
        viewHolder.seekBar.setProgress(searchAPI.getSegments().get(position).getPosition());
        //рабоатет смена ползунка при разном типе транспорта
        //viewHolder.seekBar.setThumb(context.getResources().getDrawable(R.drawable.seek_thumb_surban));
        viewHolder.bubbleSeekBar.getConfigBuilder().max(searchAPI.getSegments().get(position).getTrack_max()).build();
        viewHolder.bubbleSeekBar.setProgress(searchAPI.getSegments().get(position).getPosition());
        viewHolder.startTime.setText("0 мин.");
        viewHolder.finishTime.setText(searchAPI.getSegments().get(position).getTrack_max().toString() + " мин.");
        if((searchAPI.getSegments().get(position).isTrack_visible())) {
            viewHolder.seekBar.setVisibility(View.VISIBLE);
            viewHolder.bubbleSeekBar.setVisibility(View.VISIBLE);
            viewHolder.startTime.setVisibility(View.VISIBLE);
            viewHolder.finishTime.setVisibility(View.VISIBLE);
        } else {
            viewHolder.seekBar.setVisibility(View.INVISIBLE);
            viewHolder.bubbleSeekBar.setVisibility(View.INVISIBLE);
            viewHolder.startTime.setVisibility(View.INVISIBLE);
            viewHolder.finishTime.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return searchAPI.getSegments().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView Img;
        public TextView TDays;
        public TextView TDuration;
        public TextView TFrom;
        public TextView TFromTime;
        public TextView TNameReys;
        public TextView TNumber;
        public TextView TStatus;
        public TextView TTo;
        public TextView TToTime;
        public BubbleSeekBar bubbleSeekBar;
        public TextView finishTime;
        public SeekBar seekBar;
        public TextView startTime;

        public ViewHolder(View view) {
            super(view);
            this.TFrom = (TextView) view.findViewById(R.id.fromStation);
            this.TTo = (TextView) view.findViewById(R.id.toStation);
            this.TToTime = (TextView) view.findViewById(R.id.arrival);
            this.TFromTime = (TextView) view.findViewById(R.id.fromTime);
            this.TStatus = (TextView) view.findViewById(R.id.status);
            this.TDays = (TextView) view.findViewById(R.id.days);
            this.TDuration = (TextView) view.findViewById(R.id.duration);
            this.TNameReys = (TextView) view.findViewById(R.id.nameReys);
            this.TNumber = (TextView) view.findViewById(R.id.number);
            this.Img = (ImageView) view.findViewById(R.id.imageView);
            this.bubbleSeekBar = (BubbleSeekBar) view.findViewById(R.id.track_distation);
            this.seekBar = (SeekBar) view.findViewById(R.id.track_bus);
            this.startTime = (TextView) view.findViewById(R.id.t1);
            this.finishTime = (TextView) view.findViewById(R.id.t2);

            view.setTag(itemView.getId(), "http://ya.ru/"+itemView.getId());


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                PopupMenu popup = new PopupMenu(view.getContext(), view);
                popup.inflate(R.menu.popup_menu);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        Log.d("ssss", "+"+view.getTag(itemView.getId()));
                        return false;
                    }
                });
                popup.show();
            }
        });
    }
//
//        @Override
//        public boolean onMenuItemClick(MenuItem menuItem) {
//
//        switch (menuItem.getItemId())
//        {
//            case R.id.item1:
//                Toast.makeText(context, " "+menuItem.getItemId(), Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(context, ActivityWebView.class);
//                intent.putExtra("i1", "ya.ru");
//                context.startActivity(intent);
//                return true;
//            case R.id.item2:
//                return true;
//            default:
//                return false;
//        }
//        }
        }

}
