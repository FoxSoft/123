package com.example.eminem.sac;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.xw.repo.BubbleSeekBar;
import com.yalantis.phoenix.PullToRefreshView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import Adapter.AdapterNearestStation;
import Adapter.AdapterTypeTransport;
import Model.NearestStation;
import Model.TypeTransport;

public class ActivityNearestStations extends AppCompatActivity implements LocationListener {

    private Spinner spinner;
    private AdapterTypeTransport adapterTypeTransport;
    private List<TypeTransport> transportList;
    private RecyclerView recyclerView;
    private AdapterNearestStation adapterNearestStation;
    private List<NearestStation> stationList;
    private RecyclerView.LayoutManager layoutManager;
    private Download download;
    private Thread myThread;
    private TextView lan;
    private TextView lng;
    private TextView provider;
    private BubbleSeekBar bubbleSeekBar;
    private LocationManager locationManager;
    private double longitude = 0;
    private double latitude = 0;
    private String nameProvider;
    private PullToRefreshView mPullToRefreshView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearest_stations);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lan = (TextView) findViewById(R.id.myLan);
        lng = (TextView) findViewById(R.id.myLng);
        provider = (TextView) findViewById(R.id.provider);
        bubbleSeekBar = (BubbleSeekBar) findViewById(R.id.seekBar);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        mPullToRefreshView = (PullToRefreshView) findViewById(R.id.pull_to_refresh);
        mPullToRefreshView.setOnRefreshListener(new PullToRefreshView.OnRefreshListener() {
            @Override
            public void onRefresh() {

                mPullToRefreshView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isOnline3() & (longitude != 0) & (latitude != 0)) {
                            download = new Download(getApplicationContext(), latitude, longitude, bubbleSeekBar.getProgress(), null, null);
                            download.execute();
                            threadNotifyRecycler();
                        } else Toast.makeText(getApplicationContext(), "нет интернета...", Toast.LENGTH_SHORT).show();
                        mPullToRefreshView.setRefreshing(false);
                    }
                }, 3000);
            }
        });

        spinner = (Spinner) findViewById(R.id.spinner);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerStations);

        layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        stationList = new ArrayList<>();
        transportList = new ArrayList<>();
        stationList.add(new NearestStation("5 км", "s7846", "Бревно", "bus_stop", "2", "45.8456", "14.7894", "bus",
                "автобусная остановка", "http://vk.com", R.drawable.buses_green));
        stationList.add(new NearestStation("5 км", "s7846", "Бревно", "bus_stop", "2", "45.8456", "14.7894", "bus",
                "автобусная остановка", "http://vk.com", R.drawable.buses_green));
        stationList.add(new NearestStation("5 км", "s7846", "Бревно", "bus_stop", "2", "45.8456", "14.7894", "bus",
                "автобусная остановка", "http://vk.com", R.drawable.buses_green));

        transportList.add(new TypeTransport("автобус", R.drawable.buses_red, Yandex.TRANSPORT_TYPE.bus,  0));
        transportList.add(new TypeTransport("поезд", R.drawable.train_red, Yandex.TRANSPORT_TYPE.train, 1));
        transportList.add(new TypeTransport("электричка", R.drawable.surban_red, Yandex.TRANSPORT_TYPE.suburban, 2));

        adapterTypeTransport = new AdapterTypeTransport(getApplicationContext(), transportList);
        spinner.setAdapter(adapterTypeTransport);

        adapterNearestStation = new AdapterNearestStation(stationList, getApplicationContext());
        recyclerView.setAdapter(adapterNearestStation);

//        download = new Download(getApplicationContext(), 54.777965, 38.885574, 5, null, null);
//        if (isOnline3()) {
//            download.execute();
//
//        } else
//            Toast.makeText(getApplicationContext(), "нет интернета...", Toast.LENGTH_SHORT).show();

        //threadNotifyRecycler();


    }

    private void threadNotifyRecycler() {
        myThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()) {
                    Log.d("myThread+", "Привет из потока " + Thread.currentThread().getName());

                    if (download.isSet) {
                        stationList.clear();
                        Log.d("myThread+", String.valueOf(download.stations.size()));
                        for (int i = 0; i < download.stations.size(); i++) {
                            stationList.add(download.stations.get(i));
                        }
                        ActivityNearestStations.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                recyclerView.getAdapter().notifyDataSetChanged();
                                Log.d("myThread+", "Привет из потока runOnUiThread");
                            }
                        });

                        myThread.interrupt();

                        Log.d("myThread+", "сдохни " + Thread.currentThread().getName());
                    }

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }
        });
        myThread.start();
    }

    protected boolean isOnline3() {
        String cs = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(cs);
        if (cm.getActiveNetworkInfo() == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        getLocation(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        provider.setText(nameProvider);
    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private void formatLocation(Location location) {
        longitude = location.getLongitude();
        Log.d("locale_lon", String.valueOf(location.getLongitude()));
        lng.setText(String.valueOf(location.getLongitude()));
        latitude = location.getLatitude();
        Log.d("locale_lan", String.valueOf(location.getLatitude()));
        lan.setText(String.valueOf(location.getLatitude()));
        nameProvider = location.getProvider();
        provider.setText(nameProvider);
    }

    void getLocation(Location location) {
        if (location == null)
            return;
        if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
            formatLocation(location);
        } else if (location.getProvider().equals(LocationManager.NETWORK_PROVIDER)) {
            formatLocation(location);
        }

    }

}
