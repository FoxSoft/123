package Model;

import android.widget.TextView;

/**
 * Created by eminem on 31.12.2017.
 */

public class Widget {
    private Integer id;
    private String fromTime;
    private String toTime;
    private String duration;
    private String nameReys;
    private String status;
    private String days;
    private int idImg;
    private int colorStatus;

    public Widget() {
    }

    public Widget(Integer id, String fromTime, String toTime, String duration, String nameReys, String status, String days, int idImg, int colorStatus) {
        this.id = id;
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.duration = duration;
        this.nameReys = nameReys;
        this.status = status;
        this.days = days;
        this.idImg = idImg;
        this.colorStatus = colorStatus;

    }

    public int getColorStatus() {
        return colorStatus;
    }

    public void setColorStatus(int colorStatus) {
        this.colorStatus = colorStatus;
    }

    public int getIdImg() {
        return idImg;
    }

    public void setIdImg(int idImg) {
        this.idImg = idImg;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    @Override
    public String toString() {
        return "Widget{" +
                "id=" + id +
                ", fromTime='" + fromTime + '\'' +
                ", toTime='" + toTime + '\'' +
                ", duration='" + duration + '\'' +
                ", nameReys='" + nameReys + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getNameReys() {
        return nameReys;
    }

    public void setNameReys(String nameReys) {
        this.nameReys = nameReys;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
