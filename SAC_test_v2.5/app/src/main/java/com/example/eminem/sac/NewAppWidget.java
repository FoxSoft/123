package com.example.eminem.sac;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.format.Time;
import android.util.Log;
import android.widget.ListView;
import android.widget.RemoteViews;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import Adapter.AdapterListWidget;
import Model.Widget;

/**
 * Implementation of App Widget functionality.
 */
public class NewAppWidget extends AppWidgetProvider {


    private static RemoteViews views;
    private PendingIntent service = null;
    private static List<Widget> widgets = new ArrayList<>();
    public static class Status{
        private int idImg;
        private int max;

        public static enum StatusTransport{
            DRIVE, ARRIVAL, DEPARTURE  //в пути, уехал, отправится
        }
        public Status() {
        }

        public static String FormatDurationTotime(String Duration) {
            int sec = (int) Float.parseFloat(Duration);
            int hour = sec / 3600;
            int minute = (int) (((double) (sec - (hour * 3600))) * 0.016666666667d);
            return hour == 0 ? minute + " мин.":hour + " ч. " + minute + " мин.";
        }

        public static List<Integer> FormatSecToTime(Integer time) {
            List<Integer> result = new ArrayList();
            Integer hour = time / 60;
            Integer minute = time - (hour * 60);
            result.add(hour);
            result.add(minute);
            return result;
        }

        @NonNull
        public static int[] timeToInt(String time) {
            int[] result = new int[0];
            String[] str = time.split(":");
            int chour = Integer.parseInt(str[0]);
            int minute = Integer.parseInt(str[1]);
            return new int[]{chour, minute};
        }

        public String statusBusV2(String time) {
            Time t = new Time();
            int[] timeMas = timeToInt(time);
            t.set(0, timeMas[1], timeMas[0], 0, 0, 0);
            Time tNow = new Time(Time.getCurrentTimezone());
            tNow.setToNow();
            int hour = t.hour - tNow.hour;
            int result = (hour * 60) + (t.minute - tNow.minute);
            Integer pHour = FormatSecToTime(result).get(0);
            Integer pMinute = FormatSecToTime(result).get(1);
            if (result > 0) {
                String str;
                this.idImg = R.drawable.buses_green;
                StringBuilder append = new StringBuilder().append("отправка через ").append(pHour == 0 ? "" : String.valueOf(pHour) + " ч. ");
                if (pMinute== 0) {
                    str = "";
                } else {
                    str = String.valueOf(pMinute) + " мин.";
                }
                return append.append(str).toString();
            } else if (hour == 0) {
                this.idImg = R.drawable.buses_yellow;
                return "ушел " + String.valueOf(pMinute < 0 ? -pMinute: pMinute) + " мин. назад";
            } else if (result > (-this.max)) {
                this.idImg = R.drawable.buses_yellow;
                return "ушел";
            } else {
                this.idImg = R.drawable.buses_red;
                return "ушел";
            }
        }
    }

    public static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        views = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
        String s = "09:58:55";
        Status status = new Status();
        Log.d("enum_exp", status.statusBusV2("22:00"));
        views.setTextViewText(R.id.nameReys, "Зарайск-Коломна");
        views.setTextViewText(R.id.duration, "1 ч. 20 мин.");
        views.setTextViewText(R.id.status, getDate());
        views.setTextViewText(R.id.fromTime, s);
        views.setTextViewText(R.id.toTime, "13:10");
        if (s.compareTo(getDate()) == 0) {
            views.setImageViewResource(R.id.img, R.drawable.buses_green);

        } else{
            views.setImageViewResource(R.id.img, R.drawable.buses_red);
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

//        views = new RemoteViews(context.getPackageName(), R.layout.widget);
//        random = new Random();
//        views.setImageViewResource(R.id.img, R.drawable.buses_red);
//       // views.setTextViewText(R.id.nameReys, "Зарайск-Коломна");
//        views.setTextViewText(R.id.duration, "1 ч. 20 мин.");
//        views.setTextViewText(R.id.status, "ушел 20 мин. назад");
//        views.setTextViewText(R.id.fromTime, "12:25");
//        views.setTextViewText(R.id.toTime, "13:10");

    }

    private static String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
        return dateFormat.format(new Date());
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        final AlarmManager manager =(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        final Calendar startTime = Calendar.getInstance();
        startTime.set(Calendar.MINUTE, 0);
        startTime.set(Calendar.SECOND, 0);
        startTime.set(Calendar.MILLISECOND, 0);
        final Intent i = new Intent(context, ServiceWidget.class);

        if (service == null)
        {
            service = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
        }

        manager.setRepeating(AlarmManager.RTC,startTime.getTime().getTime(),500,service);
    }

    @Override
    public void onEnabled(Context context) {

    }

    @Override
    public void onDisabled(Context context) {

    }
}



