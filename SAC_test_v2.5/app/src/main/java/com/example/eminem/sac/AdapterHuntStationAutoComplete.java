package com.example.eminem.sac;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import Model.StationDB;

/**
 * Created by eminem on 02.12.2017.
 */

public class AdapterHuntStationAutoComplete extends BaseAdapter implements Filterable {
    private static final int MAX_RESULTS = 10;

    private Context context;
    private List<StationDB> stationDBList;
    private DBAdapter dbAdapter;

    public AdapterHuntStationAutoComplete(Context context) {
        this.context = context;
        this.stationDBList = new ArrayList<>();
        this.dbAdapter = new DBAdapter(context);
    }

    @Override
    public int getCount() {
        return stationDBList.size();
    }

    @Override
    public StationDB getItem(int position) {
        return stationDBList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.item_hint_auto_complete_station, parent, false);
        }
        StationDB stationDB = getItem(position);
        ((TextView) convertView.findViewById(R.id.nameStation)).setText(stationDB.getNameStation());
        ((TextView) convertView.findViewById(R.id.nameCity)).setText(stationDB.getNameCity());
        ((TextView) convertView.findViewById(R.id.nameRegion)).setText(stationDB.getNameRegion());
        ((TextView) convertView.findViewById(R.id.typeSation)).setText(stationDB.getTypeStation());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        final Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    List<StationDB> stationDBS = findStations(constraint.toString());
                    // Assign the data to the FilterResults
                    filterResults.values = stationDBS;
                    filterResults.count = stationDBS.size();
                }
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    stationDBList = (List<StationDB>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }
        private List<StationDB> findStations(String station) {

        List<StationDB> list = new ArrayList<>();
        list = dbAdapter.getStationOfInput2(station);
        return list;

    }
}
