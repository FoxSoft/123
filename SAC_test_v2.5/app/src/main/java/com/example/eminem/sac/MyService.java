package com.example.eminem.sac;

import android.content.Intent;
import android.widget.RemoteViewsService;

/**
 * Created by eminem on 31.12.2017.
 */
public class MyService extends RemoteViewsService {

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new MyFactory(getApplicationContext(), intent);
    }

}