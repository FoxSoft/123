package Model;

/**
 * Created by eminem on 12.11.2017.
 */

public class ChildsItem {
    private String nameStation;
    private String typeStation;
    private String duration;
    private String arrival;
    private String departure;
    private String codeStation;
    private String stopTime;

    private int idGroup;
    private int idChield;


    public ChildsItem() {
    }

    public ChildsItem(String nameStation, String typeStation, String duration, String arrival, String departure,
                      String codeStation, String stopTime, int idGroup, int idChield) {
        this.nameStation = nameStation;
        this.typeStation = typeStation;
        this.duration = duration;
        this.arrival = arrival;
        this.departure = departure;
        this.codeStation = codeStation;
        this.stopTime = stopTime;
        this.idGroup = idGroup;
        this.idChield = idChield;
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public int getIdChield() {
        return idChield;
    }

    public void setIdChield(int idChield) {
        this.idChield = idChield;
    }

    public String getNameStation() {
        return nameStation;
    }

    public void setNameStation(String nameStation) {
        this.nameStation = nameStation;
    }

    public String getTypeStation() {
        return typeStation;
    }

    public void setTypeStation(String typeStation) {
        this.typeStation = typeStation;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getCodeStation() {
        return codeStation;
    }

    public void setCodeStation(String codeStation) {
        this.codeStation = codeStation;
    }

    public String getStopTime() {
        return stopTime;
    }

    public void setStopTime(String stopTime) {
        this.stopTime = stopTime;
    }
}
