package com.example.eminem.sac;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.xw.repo.BubbleSeekBar;

import java.util.List;

/**
 * Created by eminem on 22.10.2017.
 */

public class AdapterBus extends BaseAdapter {
    private List<Bus> buses;
    private Context context;

    public AdapterBus(Context context, List<Bus> buses) {
        this.context = context;
        this.buses = buses;
    }

    public int getCount() {
        return buses.size();
    }

    public Object getItem(int n) {
        return buses.get(n);
    }

    @Override
    public boolean isEnabled(int position) {
        return super.isEnabled(position);
    }

    public long getItemId(int n) {
        return buses.get(n).getId();
    }

    public View getView(int n, View view, ViewGroup viewGroup) {
        View v = view;
        AdapterBus.ViewHolder viewHolder;
        if(view == null) {
            v = View.inflate(this.context, R.layout.row, null);
            viewHolder = new AdapterBus.ViewHolder(v);
            v.setTag(viewHolder);
        } else {
            viewHolder = (AdapterBus.ViewHolder)view.getTag();
        }

        viewHolder.TNumber.setText(buses.get(n).getNumber());
        viewHolder.TFromTime.setText(buses.get(n).getFromTime());
        viewHolder.TFrom.setText(buses.get(n).getFromStation());
        viewHolder.TTo.setText(buses.get(n).getToStation());
        viewHolder.TToTime.setText(buses.get(n).getToTime());
        viewHolder.TDuration.setText(buses.get(n).getDuration());
        viewHolder.TDays.setText(buses.get(n).getDays());
        viewHolder.TNameReys.setText((buses.get(n)).getNameReys());
        viewHolder.TStatus.setText(buses.get(n).getStatus());
        viewHolder.Img.setImageResource(buses.get(n).getIdImg());
        viewHolder.seekBar.setMax(buses.get(n).getTrack_max());
        viewHolder.seekBar.setProgress(buses.get(n).getPosition());
        viewHolder.bubbleSeekBar.getConfigBuilder().max(buses.get(n).getTrack_max()).build();
        viewHolder.bubbleSeekBar.setProgress(buses.get(n).getPosition());
        viewHolder.startTime.setText("0 мин.");
        viewHolder.finishTime.setText(buses.get(n).getTrack_max().toString() + " мин.");
        if((buses.get(n).isTrack_visible())) {
            viewHolder.seekBar.setVisibility(View.VISIBLE);
            viewHolder.bubbleSeekBar.setVisibility(View.VISIBLE);
            viewHolder.startTime.setVisibility(View.VISIBLE);
            viewHolder.finishTime.setVisibility(View.VISIBLE);
            return v;
        } else {
            viewHolder.seekBar.setVisibility(View.INVISIBLE);
            viewHolder.bubbleSeekBar.setVisibility(View.INVISIBLE);
            viewHolder.startTime.setVisibility(View.INVISIBLE);
            viewHolder.finishTime.setVisibility(View.INVISIBLE);
            return v;
        }
    }

    public void refreshAdapter() {
        this.notifyDataSetChanged();
    }

    private class ViewHolder {
        public ImageView Img;
        public TextView TDays;
        public TextView TDuration;
        public TextView TFrom;
        public TextView TFromTime;
        public TextView TNameReys;
        public TextView TNumber;
        public TextView TStatus;
        public TextView TTo;
        public TextView TToTime;
        public BubbleSeekBar bubbleSeekBar;
        public TextView finishTime;
        public SeekBar seekBar;
        public TextView startTime;

        public ViewHolder(View view) {
            this.TFrom = (TextView)view.findViewById(R.id.fromStation);
            this.TTo = (TextView)view.findViewById(R.id.toStation);
            this.TToTime = (TextView)view.findViewById(R.id.arrival);
            this.TFromTime = (TextView)view.findViewById(R.id.fromTime);
            this.TStatus = (TextView)view.findViewById(R.id.status);
            this.TDays = (TextView)view.findViewById(R.id.days);
            this.TDuration = (TextView)view.findViewById(R.id.duration);
            this.TNameReys = (TextView)view.findViewById(R.id.nameReys);
            this.TNumber = (TextView)view.findViewById(R.id.number);
            this.Img = (ImageView)view.findViewById(R.id.imageView);
            this.bubbleSeekBar = (BubbleSeekBar)view.findViewById(R.id.track_distation);
            this.seekBar = (SeekBar)view.findViewById(R.id.track_bus);
            this.startTime = (TextView)view.findViewById(R.id.t1);
            this.finishTime = (TextView)view.findViewById(R.id.t2);
        }
    }
}
