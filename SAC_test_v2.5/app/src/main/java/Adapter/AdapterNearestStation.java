package Adapter;

        import android.content.Context;
        import android.content.Intent;
        import android.support.v7.widget.RecyclerView;
        import android.util.Log;
        import android.view.ContextMenu;
        import android.view.LayoutInflater;
        import android.view.MenuItem;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ImageView;
        import android.widget.PopupMenu;
        import android.widget.SeekBar;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.example.eminem.sac.ActivityWebView;
        import com.example.eminem.sac.AdapterListView;
        import com.example.eminem.sac.Bus;
        import com.example.eminem.sac.R;
        import com.shehabic.droppy.DroppyClickCallbackInterface;
        import com.shehabic.droppy.DroppyMenuPopup;
        import com.shehabic.droppy.animations.DroppyFadeInAnimation;
        import com.xw.repo.BubbleSeekBar;

        import java.util.List;

        import Model.NearestStation;

        import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Created by eminem on 25.12.2017.
 */

public class AdapterNearestStation extends RecyclerView.Adapter<AdapterNearestStation.ViewHolder> {

    private List<NearestStation> stations;
    private Context context;
    private LayoutInflater inflater;
    private int position;

    public AdapterNearestStation(List<NearestStation> stations, Context context) {
        this.stations = stations;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public boolean onFailedToRecycleView(ViewHolder holder) {
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_nearest_station, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        viewHolder.stationType.setText(stations.get(position).getStationType());
        viewHolder.title.setText(stations.get(position).getTitle());
        viewHolder.distance.setText(stations.get(position).getDistance());
        viewHolder.lat.setText(stations.get(position).getLat());
        viewHolder.lng.setText(stations.get(position).getLng());
        viewHolder.majority.setText(stations.get(position).getMajority());
        viewHolder.stationTypeName.setText(stations.get(position).getStationTypeName());
        viewHolder.transportTypeImg.setImageResource((stations.get(position)).getTypeTransportImg());

//        viewHolder.droppyMenu = viewHolder.droppyBuilder.fromMenu(R.menu.menu_pop)
//                        .triggerOnAnchorClick(false)
//                .setOnClick(new DroppyClickCallbackInterface() {
//                    @Override
//                    public void call(View v, int id) {
//                        Log.d("Id:", String.valueOf(id));
//                    }
//                }).setOnDismissCallback(new DroppyMenuPopup.OnDismissCallback() {
//                    @Override
//                    public void call() {
//
//                    }
//                }).setPopupAnimation(new DroppyFadeInAnimation())
//                .setXOffset(5)
//                .setYOffset(5)
//                .build();
        viewHolder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.droppyMenu.show();
            }
        });

        viewHolder.itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu contextMenu, final View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
                contextMenu.add("link");
                contextMenu.getItem(position).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        Intent intent = new Intent(context.getApplicationContext(), ActivityWebView.class);
                        intent.putExtra("link", stations.get(position).getTouchUrl());
                        context.startActivity(intent.setFlags(FLAG_ACTIVITY_NEW_TASK));
                        return false;
                    }
                });
            }
        });

        // this.position = position;
    }

    @Override
    public int getItemCount() {
        return stations.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView distance;
        private TextView title;
        private TextView stationType;
        private TextView majority;
        private TextView lat;
        private TextView lng;
        private ImageView transportTypeImg;
        private TextView stationTypeName;
        private ImageView menu;
        private DroppyMenuPopup.Builder droppyBuilder;
        private DroppyMenuPopup droppyMenu;


        public ViewHolder(final View view) {
            super(view);
            this.distance = (TextView) view.findViewById(R.id.distance);
            this.title = (TextView) view.findViewById(R.id.title);
            this.stationType = (TextView) view.findViewById(R.id.stationType);
            this.majority = (TextView) view.findViewById(R.id.majority);
            this.lat = (TextView) view.findViewById(R.id.lat);
            this.lng = (TextView) view.findViewById(R.id.lng);
            this.transportTypeImg = (ImageView) view.findViewById(R.id.transportTypeImg);
            this.stationTypeName = (TextView) view.findViewById(R.id.stationTypeName);
            this.lat = (TextView) view.findViewById(R.id.lat);
            this.menu = (ImageView) view.findViewById(R.id.menu);

            droppyBuilder = new DroppyMenuPopup.Builder(view.getContext(), menu);
            DroppyMenuPopup droppyMenu = droppyBuilder.fromMenu(R.menu.menu_pop)
                    .triggerOnAnchorClick(false)
                    .setOnClick(new DroppyClickCallbackInterface() {
                        @Override
                        public void call(View v, int id) {
                            Log.d("Id:", String.valueOf(id));
                        }
                    }).setOnDismissCallback(new DroppyMenuPopup.OnDismissCallback() {
                @Override
                public void call()
                {
                    Log.d("popMenu", "Menu dismissed");
                }
            }).setPopupAnimation(new DroppyFadeInAnimation())
                    .setXOffset(5)
                    .setYOffset(5)
                    .build();
        }


    }
}
