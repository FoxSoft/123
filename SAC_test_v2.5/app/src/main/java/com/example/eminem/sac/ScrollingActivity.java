package com.example.eminem.sac;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Toast;
import com.bigkoo.svprogresshud.SVProgressHUD;
import com.bigkoo.svprogresshud.listener.OnDismissListener;
import com.polyak.iconswitch.IconSwitch;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import Adapter.AdapterTypeTransport;
import Model.StationDB;
import Model.TypeTransport;

public class ScrollingActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public AdapterListView adapterBus;
    public List<Bus> busList;

    public DBAdapter dbAdapter;
    private Builder dialog;
    private AutoCompleteTextView editTextFrom;
    private AutoCompleteTextView editTextTo;
    public Boolean internetConnect = false;
    public RecyclerView recycler;
    public String searchReys = null;
    private String from;
    private String to;
    public String searchReysReverse = null;
    private SVProgressHUD mSVProgressHUD;
    private IconSwitch iconSwitch;
    private AdapterHuntStationAutoComplete stationAutoComplete;
    private final String TAG = "ScrollingActivity";
    private StationDB stationDBFrom;
    private StationDB stationDBTo;

    private Download download;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        isOnline();

        dbAdapter = new DBAdapter(this);
        editTextFrom = (AutoCompleteTextView) findViewById(R.id.fromStationEdit);
        editTextTo = (AutoCompleteTextView) findViewById(R.id.toStationEdit);
        iconSwitch = (IconSwitch) findViewById(R.id.iconSwitch);
        setIconSwitch();

        creteDialog();

        stationDBFrom = new StationDB();
        stationDBTo = new StationDB();

        mSVProgressHUD = new SVProgressHUD(this);
        mSVProgressHUD.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(SVProgressHUD hud) {
                // todo something, like: finish current activity
                // Toast.makeText(getApplicationContext(), "dismiss", Toast.LENGTH_SHORT).show();
            }
        });

        ((FloatingActionButton) findViewById(R.id.fab)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                isOnline();

                boolean inputFrom = !editTextFrom.getText().toString().equals("") ? true : false;
                boolean inputTo = !editTextTo.getText().toString().equals("") ? true : false;

                if ((inputFrom & inputTo)) {
                    searchReys = editTextFrom.getText() + "_" + editTextTo.getText();
                    searchReysReverse = editTextTo.getText() + "_" + editTextFrom.getText();
                    if (dbAdapter.hashTable(searchReys)) {
                        dialog.show();
                    } else {
                        if (internetConnect) {
                            Log.d("SEARCHB", "=================");


                            String fromCode = stationDBFrom.getCodeStation();
                            String toCode = stationDBTo.getCodeStation();
                            Log.d("SEARCHB", fromCode);
                            Log.d("SEARCHB", toCode);

                            inputFrom = !fromCode.equals("") ? true : false;
                            inputTo = !toCode.equals("") ? true : false;
                            if ((inputFrom & inputTo)) {
                                dbAdapter.createTable(searchReys);
                                busList.clear();

                                download = new Download(getApplicationContext(), fromCode, toCode, null, false, searchReys);
                                download.execute();

                                RefrashTime();
                                dbAdapter.createTable(searchReysReverse);
                                Download dBack = new Download(getApplicationContext(), toCode, fromCode, null, false, searchReysReverse);
                                dBack.execute();

                               // Log.d("search__", download.searchAPI.getSegments().get(0).toString());

                                mSVProgressHUD.showSuccessWithStatus("Загружено!");
                                dbAdapter.resetHistiryRasp(searchReys, editTextFrom.getText().toString(), stationDBFrom.getCodeStation(),
                                        editTextTo.getText().toString(), stationDBTo.getCodeStation());
                                Log.d("search", fromCode + "  " + toCode);


                            } else {

                                mSVProgressHUD.showInfoWithStatus("Упс... Возможно, вы допустили ошибку. Если все верно, тогда сорри. Такого города нет в базе...");
                                //Snackbar.make(view, "Упс... Возможно, вы допустили ошибку. Если все верно, тогда сорри. Такого города нет в базе...", 0).setAction("Action", null).show();


                            }
                        } else {
                            mSVProgressHUD.showInfoWithStatus("нет интернета...");
                        }
                    }
                } else {
                    mSVProgressHUD.showErrorWithStatus("Заполните все поля ...");
                }


            }

        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return false;
            }
        });





        recycler = (RecyclerView) findViewById(R.id.recycler);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        recycler.setLayoutManager(layoutManager);
        recycler.setNestedScrollingEnabled(false);

        createEdits();

        busList = new ArrayList();

        // preview();

        adapterBus = new AdapterListView(getApplicationContext(), busList);
        recycler.setAdapter(adapterBus);
        loadHistory();


        dbAdapter.createTableV2("Рязань_Коломна");
        Download d = new Download(getApplicationContext(), dbAdapter.getCodeCity("Рязань"),
                dbAdapter.getCodeCity("Коломна"), null, false, "Рязань_Коломна");
        d.execute();
        Date date = new Date();
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("'последнее обновление:' hh:mm', 'yyyy.MM.dd ");

       Log.d("search__",formatForDateNow.format(date));


    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void createEdits() {
        stationAutoComplete = new AdapterHuntStationAutoComplete(this);
        editTextFrom.setAdapter(stationAutoComplete);
        editTextFrom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                stationDBFrom = (StationDB) adapterView.getItemAtPosition(position);
                editTextFrom.setText(stationDBFrom.getNameStation());
                Toast.makeText(getApplicationContext(), stationDBFrom.getCodeStation(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, stationDBFrom.getCodeStation() + " " + stationDBFrom.getCodeCity() + " " + stationDBFrom.getCodeRegion());
            }
        });

        editTextTo.setAdapter(stationAutoComplete);
        editTextTo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                stationDBTo = (StationDB) adapterView.getItemAtPosition(position);
                editTextTo.setText(stationDBTo.getNameStation());
                Toast.makeText(getApplicationContext(), stationDBTo.getCodeStation(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, stationDBTo.getCodeStation() + " " + stationDBTo.getCodeCity() + " " + stationDBTo.getCodeRegion());
            }
        });
    }

    private void preview() {
        busList.add(new Bus(1, "Зарайск", "Коломна", "23:00", "8:10", "31", "4200",
                "Зарайск - Коломна Зарайск - Коломна", "ежедневно", "впути", R.drawable.buses_yellow));
        busList.add(new Bus(2, "Зарайск", "Коломна", "21:00", "8:10", "31", "4200",
                "Зарайск - Коломна", "ежедневно", "впути", R.drawable.buses_red));
        busList.add(new Bus(3, "Зарайск", "Коломна", "14:00", "8:10", "31", "4200",
                "Зарайск - Коломна", " Января. 12. 25 Декабря. 12. 45 .47. 7 Февраля. 12.14.15.16.17.18.19. Сентября. 5. 6. 7. 8. 9" +
                " Января. 12. 25 Декабря. 12. 45 .47. 7 Февраля. 12.14.15.16.17.18.19. Сентября", "впути", R.drawable.buses_red));
        busList.add(new Bus(4, "Зарайск", "Коломна", "8:00", "8:10", "31", "4200",
                "Зарайск - Коломна", "ежедневно", "впути", R.drawable.buses_green));
        busList.add(new Bus(5, "Зарайск", "Коломна", "23:32", "8:10", "31", "4200",
                "Зарайск - Коломна", "5. 6. 7. 8. 9 Января. 12. 25 Декабря. 12. 45 .47. 7" +
                " Февраля. 12.14.15.16.17.18.19. Сентября", "впути", Integer.valueOf(R.drawable.buses_red)));
    }

    private void creteDialog() {
        dialog = new Builder(this);
        dialog.setTitle("поиск расписания");
        dialog.setMessage("Такой маршрут уже был добавлен ранее, хотите обновить?");
        dialog.setPositiveButton("Да", new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                isOnline();
                if (internetConnect) {
                    dbAdapter.deleteTable(searchReys);
                    dbAdapter.createTable(searchReys);
                    busList.clear();
//                    new Download(getApplicationContext(), dbAdapter.findCode(editTextFrom.getText().toString()),
//                            dbAdapter.findCode(editTextTo.getText().toString()), null, false).execute();
                    RefrashTime();
                    dbAdapter.resetHistiryRasp(searchReys, editTextFrom.getText().toString(), stationDBFrom.getCodeStation(),
                            editTextTo.getText().toString(), stationDBTo.getCodeStation());

                    mSVProgressHUD.showSuccessWithStatus("Готово!");
                    Toast.makeText(getApplicationContext(), "Расписание " + editTextFrom.getText()
                            + "-" + editTextTo.getText() + " успешно обновлено...", Toast.LENGTH_LONG).show();

                } else {
                    mSVProgressHUD.showInfoWithStatus("нет интернета...");
                }
            }
        });
        dialog.setNegativeButton("Нет", new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "Расписание используется из БД...", Toast.LENGTH_SHORT).show();
                busList.clear();
                List<Bus> lb = new ArrayList<>();
                lb = dbAdapter.recordRaspTable(searchReys);
                Log.d("OOOOP", lb.toString());
                Log.d("OOOOP", lb.toString());
                for (int i = 0; i < lb.size(); i++) {
                    busList.add(lb.get(i));
                }
//               RefrashTime();
                adapterBus.notifyDataSetChanged();
                recycler.setAdapter(adapterBus);
//                dbAdapter.resetHistiryRasp(searchReys);
                mSVProgressHUD.showSuccessWithStatus("Готово!");
            }
        });
    }

    private void setIconSwitch() {
        final IconSwitch.Checked checked = null;
        iconSwitch.setBackgroundColor(Color.parseColor("#FF1D1D1D"));

        iconSwitch.setThumbColorLeft(Color.parseColor("#00ff8a"));
        iconSwitch.setThumbColorRight(Color.parseColor("#ff0066"));

        iconSwitch.setActiveTintIconLeft(Color.parseColor("#ffffff"));
        iconSwitch.setInactiveTintIconLeft(Color.parseColor("#00ff7e"));
        iconSwitch.setActiveTintIconRight(Color.parseColor("#ffffff"));
        iconSwitch.setInactiveTintIconRight(Color.parseColor("#ff0066"));
        iconSwitch.setCheckedChangeListener(new IconSwitch.CheckedChangeListener() {
            @Override
            public void onCheckChanged(IconSwitch.Checked current) {
                switch (current.toggle()) {
                    case LEFT:
                        if (searchReys != null) {
                            busList.clear();
                            List<Bus> list = new ArrayList();
                            list = dbAdapter.recordRaspTable(searchReysReverse);

                            for (int i = 0; i < list.size(); i++) {
                                busList.add(list.get(i));
                            }

                            // RefrashTime();
                            adapterBus.notifyDataSetChanged();
                            recycler.setAdapter(adapterBus);
                            //   Toast.makeText(getApplicationContext(), "left", Toast.LENGTH_SHORT).show();
                            editTextTo.setText(from);
                            editTextFrom.setText(to);
                            mSVProgressHUD.showInfoWithStatus(editTextTo.getText() + "-" + editTextFrom.getText());
                        }

                        break;
                    case RIGHT:
                        if(searchReysReverse != null) {
                            busList.clear();
                            List<Bus> list2 = new ArrayList();
                            list2 = dbAdapter.recordRaspTable(searchReys);

                            for (int i = 0; i < list2.size(); i++) {
                                busList.add(list2.get(i));
                            }

                            //  RefrashTime();
                            adapterBus.notifyDataSetChanged();
                            recycler.setAdapter(adapterBus);
                            //     Toast.makeText(getApplicationContext(), "right", Toast.LENGTH_SHORT).show();
                            editTextTo.setText(to);
                            editTextFrom.setText(from);
                            mSVProgressHUD.showInfoWithStatus(editTextFrom.getText() + "-" + editTextTo.getText());

                        }
                        break;
                    default:
                }
            }
        });
    }


    public void showErrorWithStatus(View view) {
        mSVProgressHUD.showErrorWithStatus("заполните все поля", SVProgressHUD.SVProgressHUDMaskType.GradientCancel);
    }

    public boolean isNetworkOnline(Context context) {
        boolean status = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);
            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                status = true;
            } else {
                netInfo = cm.getNetworkInfo(1);
                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED)
                    status = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return status;

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {
            Toast.makeText(this, "dddd", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.action_search_op) {

//            CustomDialogFragment customDialogFragment = new CustomDialogFragment();
//            customDialogFragment.show(getSupportFragmentManager(), "d");
////            MyDialog myDialog = new MyDialog(this);
////            myDialog.createOneButtonAlertDialog("lol", "ddddd");
////
////            Intent intent = getIntent();
////
//            Bundle extras = getIntent().getExtras();
////
//           textView.setText(extras.getString("type")+"   "+ extras.getString("isCheck"));

            createDialogOptionSearch();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public class Status {
        public Integer idImg;
        public int max = 0;
        public boolean send = false;

        public Integer trackTransport(String fromTime, String toTime) {
            Time tFrom = new Time();
            Time tTo = new Time();
            Time time = new Time(Time.getCurrentTimezone());
            time.setToNow();
            int[] timeFrom = timeToInt(fromTime);
            int[] timeTo = timeToInt(toTime);
            tFrom.set(0, timeFrom[1], timeFrom[0], 0, 0, 0);
            tTo.set(0, timeTo[1], timeTo[0], 0, 0, 0);
            Log.d("TRACKING_TRANSPORT", String.valueOf(timeFrom[0]) + "  " + String.valueOf(timeFrom[1]));
            Log.d("TRACKING_TRANSPORT", String.valueOf(timeTo[0]) + "  " + String.valueOf(timeTo[1]));
            int hourFrom = tFrom.hour - time.hour;
            int minuteFrom = tFrom.minute - time.minute;
            int hourTo = tTo.hour - time.hour;
            int minuteTo = tTo.minute - time.minute;
            int minuteMinus = tTo.minute - tFrom.minute;
            int minuteSumTime = ((tTo.hour - tFrom.hour) * 60) + minuteMinus;
            this.max = ((tTo.hour * 60) + tTo.minute) - ((tFrom.hour * 60) + tFrom.minute);
            if ((tFrom.hour * 60) + tFrom.minute >= (time.hour * 60) + time.minute || (time.hour * 60) + time.minute >= (tTo.hour * 60) + tTo.minute) {
                this.send = false;
                return 0;
            }
            this.send = true;
            this.idImg = R.drawable.buses_yellow;
            Integer result = this.max - (((tTo.hour * 60) + tTo.minute) - ((time.hour * 60) + time.minute));
            Log.d("TIME_MINUTE", String.valueOf(result));
            if (result >= 0 && result <= this.max) {
                return result;
            }
            this.send = false;
            return result;
        }

        public String statusBus(String time) {
            Time t = new Time();
            int[] timeMas = timeToInt(time);
            t.set(0, timeMas[1], timeMas[0], 0, 0, 0);
            Time tNow = new Time(Time.getCurrentTimezone());
            tNow.setToNow();
            int hour = t.hour - tNow.hour;
            int minute = t.minute - tNow.minute;
            if ((hour < 0 || minute < 0) && (hour <= 0 || minute > 0)) {
                this.idImg = R.drawable.buses_red;
                if (hour != 0) {
                    return "ушел";
                }

                if (minute < 0) {
                    minute = -minute;
                }
                return String.valueOf(minute) + " мин. назад";
            }
            this.idImg = R.drawable.buses_green;
            String str = "отправка через " + (String.valueOf(hour) == "0" ? "" : String.valueOf(hour) + " ч. ");
            if (minute < 0) {
                minute = -minute;
            }
            return String.valueOf(minute) + " мин.";
        }

        public String statusBusV2(String time) {
            Time t = new Time();
            int[] timeMas = timeToInt(time);
            t.set(0, timeMas[1], timeMas[0], 0, 0, 0);
            Time tNow = new Time(Time.getCurrentTimezone());
            tNow.setToNow();
            int hour = t.hour - tNow.hour;
            int result = (hour * 60) + (t.minute - tNow.minute);
            Integer pHour = FormatSecToTime(result).get(0);
            Integer pMinute = FormatSecToTime(result).get(1);
            if (result > 0) {
                String str;
                this.idImg = R.drawable.buses_green;
                StringBuilder append = new StringBuilder().append("отправка через ").append(pHour == 0 ? "" : String.valueOf(pHour) + " ч. ");
                if (pMinute == 0) {
                    str = "";
                } else {
                    str = String.valueOf(pMinute) + " мин.";
                }
                return append.append(str).toString();
            } else if (hour == 0) {
                this.idImg = R.drawable.buses_yellow;
                return "ушел " + String.valueOf(pMinute < 0 ? -pMinute : pMinute) + " мин. назад";
            } else if (result > (-this.max)) {
                this.idImg = R.drawable.buses_yellow;
                return "ушел";
            } else {
                this.idImg = R.drawable.buses_red;
                return "ушел";
            }
        }
    }

    private void loadHistory() {
        if (dbAdapter.hashHistoryRasp()) {
            String reys = this.dbAdapter.recordhistoryRasp();
                if (dbAdapter.isRecordToRaspTable(reys)) {
                    this.busList.clear();
                    List<Bus> lb = new ArrayList();
                    lb = this.dbAdapter.recordRaspTable(reys);
                    for (int i = 0; i < lb.size(); i++) {
                        this.busList.add(lb.get(i));
                    }
                    RefrashTime();

                    this.adapterBus.notifyDataSetChanged();
                    this.searchReys = reys;

                    String[] s = reys.split("_");
                    this.from = s[0];
                    this.to = s[1];
                    this.searchReysReverse = s[1] + "_" + s[0];
                    Log.d("STRINGF", this.searchReys + " " + this.searchReysReverse);
                } else Log.d("loadHistory", "пустая табл");
        } else {
            preview();
            adapterBus.notifyDataSetChanged();
            recycler.setAdapter(adapterBus);
            RefrashTime();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    public void RefrashTime() {
        Timer t = new Timer();
        final Status status = new Status();
        t.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                ScrollingActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        for (int i = 0; i < busList.size(); i++) {
                            busList.get(i).setStatus(status.statusBusV2(busList.get(i).getFromTime()));
                            busList.get(i).setIdImg(status.idImg);
                            busList.get(i).setPosition(status.trackTransport(busList.get(i).getFromTime(), busList.get(i).getToTime()));
                            Log.d("PROVERKA_POS", String.valueOf(status.trackTransport(busList.get(i).getFromTime(), busList.get(i).getToTime())));
                            busList.get(i).setTrack_max(status.max);
                            Log.d("PROVERKA", String.valueOf(status.max));
                            busList.get(i).setTrack_visible(status.send);
                        }
                        adapterBus.notifyDataSetChanged();
                        recycler.setAdapter(adapterBus);
                    }
                });
            }
        }, 0, 5000);
    }

    protected boolean isOnline3() {
        String cs = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(cs);
        if (cm.getActiveNetworkInfo() == null) {
            return false;
        } else {
            return true;
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_thread:
                startActivity(new Intent(this, ActivityThread.class));
                return true;
            case R.id.action_shudle:
                startActivity(new Intent(this, ActivityShudle.class));
                return true;
            case R.id.action_dataRasp:
                startActivity(new Intent(this, ActivityDataRasp.class));
                break;
            case R.id.action_bookmark:
                startActivity(new Intent(this, ActivityBookMark.class));
                break;
            case R.id.action_test:
                startActivity(new Intent(this, TestActivity.class));
                break;
            case R.id.action_test_sql:
                startActivity(new Intent(this, ActivityTestSQL.class));
                break;
            case R.id.action_nearest_settlement:
                startActivity(new Intent(this, ActivityNearestSettlement.class));
                break;
            case R.id.action_nearest_stations:
                startActivity(new Intent(this, ActivityNearestStations.class));
                break;
            case R.id.action_web:
                startActivity(new Intent(this, ActivityWebView.class));
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }

    public static boolean hasConnection(final Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getActiveNetworkInfo();
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public static boolean isOnline2(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    protected void isOnline() {
        String cs = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(cs);
        if (cm.getActiveNetworkInfo() == null) {
            internetConnect = false;
        } else {
            internetConnect = true;
        }
    }

    public List<Integer> FormatSecToTime(Integer time) {
        List<Integer> result = new ArrayList();
        Integer hour = time / 60;
        Integer minute = time - (hour * 60);
        result.add(hour);
        result.add(minute);
        return result;
    }

    public int[] timeToInt(String time) {
        int[] result = new int[0];
        String[] str = time.split(":");
        int chour = Integer.parseInt(str[0]);
        int minute = Integer.parseInt(str[1]);
        return new int[]{chour, minute};
    }

    private void createDialogOptionSearch()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(ScrollingActivity.this);
        View view2 = getLayoutInflater().inflate(R.layout.dialog, null); // Получаем layout по его ID

        List<TypeTransport> transportList = new ArrayList<>();
        transportList.add(new TypeTransport("автобус", R.drawable.buses_red, Yandex.TRANSPORT_TYPE.bus,  0));
        transportList.add(new TypeTransport("поезд", R.drawable.train_red, Yandex.TRANSPORT_TYPE.train, 1));
        transportList.add(new TypeTransport("электричка", R.drawable.surban_red, Yandex.TRANSPORT_TYPE.suburban, 2));

        AdapterTypeTransport adapterTypeTransport = new AdapterTypeTransport(getApplicationContext(), transportList);
        Spinner spinner = (Spinner) findViewById(R.id.typeTransportSpinner);
        spinner.setAdapter(adapterTypeTransport);

        builder.setView(view2);
        builder.setTitle("Настройка поиска")
                .setPositiveButton("готово", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                }).setNegativeButton("отмена", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.dismiss();
            }
        });

        builder.create();
        builder.show();
    }

    private void refreshBusList()
    {

    }
//    private class Download extends AsyncTask<String, Void, Void> {
//        //        private List<String> LfromCode;
////        private List<String> Lfromtitle;
////        private List<String> LthreadNumber;
////        private List<String> LthreadTitle;
////        private List<String> LthreadUid;
////        private List<String> LthreadsArrival;
////        private List<String> LthreadsDays;
////        private List<String> LthreadsDeparture;
////        private List<String> LthreadsDuration;
////        private List<String> LthreadsToTypeTransp;
////        private List<String> LtoCode;
////        private List<String> Ltotitle;
//        private String url = "";
//        private Bus bus;
//        private Integer checkParse = 1;
//        private Document doc;
//        private String[] eventCheck = new String[]{"arrival", "departure"};
//        ;
//        private String from;
//        //private List<String> fromStation;
//        private ProgressDialog pr;
//        private boolean printScreen = true;
//        private String sfromTitle;
//        private String station;
//        private String stoTitle;
//        private String to;
//        private String uid;
//
//        public Download(String uid) {
//            this.checkParse = 2;
//            this.uid = uid;
//            this.url = "https://api.rasp.yandex.net/v1.0/thread/?apikey=86973449-b547-417e-95da-e70f87ef8748&format=xml&uid=" + this.uid + "&show_systems=all";
//        }
//
//        public Download(String from, String to) {
//            this.checkParse = 1;
//            this.from = from;
//            this.to = to;
//            this.url = "https://api.rasp.yandex.net/v1.0/search/?apikey=d09cda7c-7376-4e79-bdf1-7e0e4b3a4f58&format=xml&from=" + this.from + "&to=" + this.to;
//        }
//
//        public Download(String station, int check) {
//
//            this.checkParse = 3;
//            this.station = station;
//            if (check > 1) {
//                check = 1;
//            }
//            if (check < 0) {
//                check = 0;
//            }
//            this.url = "https://api.rasp.yandex.net/v1.0/schedule/?apikey=86973449-b547-417e-95da-e70f87ef8748&format=xml&station=" + this.station + "&event=" + this.eventCheck[check];
//        }
//
//        private void reverse() {
//            this.url = "https://api.rasp.yandex.net/v1.0/search/?apikey=d09cda7c-7376-4e79-bdf1-7e0e4b3a4f58&format=xml&from=" + this.to + "&to=" + this.from + "&lang=ru&[transport_types=bus]";
//        }
//
//        protected void onPreExecute() {
//            pr = new ProgressDialog(ScrollingActivity.this);
//            pr.setTitle("Загрузка рассписания");
//            pr.setMessage("Соединение с сервером...");
//            pr.setIndeterminate(false);
////            pr.show();
//            super.onPreExecute();
//        }
//
//        protected Void doInBackground(String... Url) {
//            try {
//                doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new URL(url).openStream()));
//                doc.getDocumentElement().normalize();
//                doc.getDocumentElement().getTagName();
//                bus = new Bus();
//            } catch (Exception e) {
////                Toast.makeText(ScrollingActivity.this, "error network", Toast.LENGTH_SHORT).show();
//            }
//            return null;
//        }
//
//        protected void onPostExecute(Void aVoid) {
//
//            if (checkParse == 3) {
//                Log.d("thread_parse", doc.getDocumentElement().getElementsByTagName("event").item(0).getTextContent());
//                NodeList stationList = doc.getElementsByTagName("station");
//                for (int s = 0; s < stationList.getLength(); s++) {
//                    Element station = (Element) stationList.item(s);
//                    Log.d("thread_parse", station.getElementsByTagName("code").item(0).getTextContent());
//                    Log.d("thread_parse", station.getElementsByTagName("title").item(0).getTextContent());
//                    Log.d("thread_parse", station.getElementsByTagName("station_type").item(0).getTextContent());
//                    Log.d("thread_parse", station.getElementsByTagName("transport_type").item(0).getTextContent());
//                }
//                NodeList scheduleList = doc.getElementsByTagName("schedule");
//                for (int i = 0; i < scheduleList.getLength(); i++) {
//                    Element schedule = (Element) scheduleList.item(i);
//                    Log.d("thread_parse", schedule.getElementsByTagName("days").item(0).getTextContent());
//                    Log.d("thread_parse", schedule.getElementsByTagName("is_fuzzy").item(0).getTextContent());
//                    Log.d("thread_parse", schedule.getElementsByTagName("arrival_time").item(0).getTextContent());
//                    Log.d("thread_parse", schedule.getElementsByTagName("departure_time").item(0).getTextContent());
//                    NodeList threadList = schedule.getElementsByTagName("thread");
//                    for (int k = 0; k < threadList.getLength(); k++) {
//                        Element thread = (Element) threadList.item(k);
//                        Log.d("thread_parse", thread.getElementsByTagName("uid").item(0).getTextContent());
//                        Log.d("thread_parse", thread.getElementsByTagName("title").item(0).getTextContent());
//                        Log.d("thread_parse", thread.getElementsByTagName("number").item(0).getTextContent());
//                        NodeList carrierList = thread.getElementsByTagName("thread");
//                        for (int f = 0; f < carrierList.getLength(); f++) {
//                            Element carrier = (Element) threadList.item(f);
//                            Log.d("thread_parse", carrier.getElementsByTagName("code").item(0).getTextContent());
//                            Log.d("thread_parse", carrier.getElementsByTagName("title").item(0).getTextContent());
//                            Log.d("thread_parse", "------------carrier------------");
//                        }
//                    }
//                    Log.d("thread_parse", "--------------schedule-------------");
//                }
//                pr.dismiss();
//            }
//            if (checkParse == 2) {
//                Element uid = doc.getDocumentElement();
//                Log.d("thread_parse", uid.getElementsByTagName("uid").item(0).getTextContent());
//                Log.d("thread_parse", uid.getElementsByTagName("title").item(0).getTextContent());
//                Log.d("thread_parse", uid.getElementsByTagName("number").item(0).getTextContent());
//                Log.d("thread_parse", uid.getElementsByTagName("transport_type").item(0).getTextContent());
//                NodeList stopsList = doc.getElementsByTagName("stops");
//                for (int i = 0; i < stopsList.getLength(); i++) {
//                    Element stops = (Element) stopsList.item(i);
//                    Log.d("thread_parse", stops.getElementsByTagName("arrival").item(0).getTextContent());
//                    Log.d("thread_parse", stops.getElementsByTagName("duration").item(0).getTextContent());
//                    Log.d("thread_parse", stops.getElementsByTagName("departure").item(0).getTextContent());
//                    Log.d("thread_parse", stops.getElementsByTagName("stop_time").item(0).getTextContent());
//                    NodeList stationList = stops.getElementsByTagName("station");
//                    for (int k = 0; k < stationList.getLength(); k++) {
//                        Element station = (Element) stationList.item(k);
//                        Log.d("thread_parse", station.getElementsByTagName("code").item(0).getTextContent());
//                        Log.d("thread_parse", station.getElementsByTagName("title").item(0).getTextContent());
//                        Log.d("thread_parse", station.getElementsByTagName("transport_type").item(0).getTextContent());
//                    }
//                }
//                pr.dismiss();
//            }
//            if (checkParse == 1) {
//                bus.clear();
//                NodeList searchList;
//                Element to;
//                NodeList threadsList;
//                Element threads;
//                NodeList toList;
//                int iTo;
//                NodeList fromList;
//                int iFrom;
//                Element from;
//                int j;
//                if (printScreen) {
//                    searchList = doc.getElementsByTagName("search");
//                    for (int k = 0; k < searchList.getLength(); k++) {
//                        to = (Element) searchList.item(k);
//                        Log.d("TOcode: ", to.getElementsByTagName("code").item(0).getChildNodes().item(0).getNodeValue());
//                        Log.d("TOtitle: ", to.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
//                        stoTitle = to.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue();
//                        Log.d("FROMcode: ", to.getElementsByTagName("code").item(1).getChildNodes().item(0).getNodeValue());
//                        Log.d("FROMtitle: ", to.getElementsByTagName("title").item(1).getChildNodes().item(0).getNodeValue());
//                        sfromTitle = to.getElementsByTagName("title").item(1).getChildNodes().item(0).getNodeValue();
//                    }
//                    threadsList = doc.getElementsByTagName("threads");
//                    for (int i = 0; i < threadsList.getLength(); i++) {
//                        threads = (Element) threadsList.item(i);
//                        Log.d("THREADS", threads.getElementsByTagName("days").item(0).getChildNodes().item(0).getNodeValue());
//                        bus.setDays(threads.getElementsByTagName("days").item(0).getChildNodes().item(0).getNodeValue());
//                        Log.d("THREADS", threads.getElementsByTagName("arrival").item(0).getChildNodes().item(0).getNodeValue());
//                        bus.setToTime(threads.getElementsByTagName("arrival").item(0).getChildNodes().item(0).getNodeValue());
//                        Log.d("THREADS", threads.getElementsByTagName("duration").item(0).getChildNodes().item(0).getNodeValue());
//                        bus.setDuration(Format.FormatDurationTotime(threads.getElementsByTagName("duration").item(0).getChildNodes().item(0).getNodeValue()));
//                        Log.d("THREADS", threads.getElementsByTagName("departure").item(0).getChildNodes().item(0).getNodeValue());
//                        bus.setFromTime(threads.getElementsByTagName("departure").item(0).getChildNodes().item(0).getNodeValue());
//                        Log.d("------------", "to------------");
//                        toList = threads.getElementsByTagName("to");
//                        for (iTo = 0; iTo < toList.getLength(); iTo++) {
//                            to = (Element) toList.item(iTo);
//                            Log.d("code: ", to.getElementsByTagName("code").item(0).getChildNodes().item(0).getNodeValue());
//                            Log.d("title: ", to.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
//                            bus.setToStation(to.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
//                        }
//                        Log.d("------------", "from------------");
//                        fromList = threads.getElementsByTagName("from");
//                        for (iFrom = 0; iFrom < fromList.getLength(); iFrom++) {
//                            from = (Element) fromList.item(iFrom);
//                            Log.d("code: ", from.getElementsByTagName("code").item(0).getChildNodes().item(0).getNodeValue());
//                            Log.d("title: ", from.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
//                            bus.setFromStation(from.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
//                        }
//                        NodeList threadList = threads.getElementsByTagName("thread");
//                        for (j = 0; j < threadList.getLength(); j++) {
//                            Element thread = (Element) threadList.item(j);
//                            Log.d("THREAD", thread.getElementsByTagName("uid").item(0).getTextContent());
//                            bus.setUid(thread.getElementsByTagName("uid").item(0).getTextContent());
//                            Log.d("THREAD", thread.getElementsByTagName("title").item(0).getTextContent());
//                            bus.setNameReys(thread.getElementsByTagName("title").item(0).getTextContent());
//                            Log.d("THREAD", thread.getElementsByTagName("number").item(0).getTextContent());
//                            bus.setNumber(thread.getElementsByTagName("number").item(0).getTextContent());
//                            Log.d("THREAD", thread.getElementsByTagName("transport_type").item(0).getTextContent());
//                            bus.setTypeTransport(thread.getElementsByTagName("transport_type").item(0).getTextContent());
//                        }
//                        bus.setId(i);
//                        bus.setIdImg(R.drawable.buses_green);
//                        bus.setStatus("в пути");
//                        dbAdapter.insertUidReysV2(bus.getUid(), bus.getNumber(), bus.getNameReys());
//                        dbAdapter.insertRaspTableV1(searchReys, bus);
//                        busList.add(new Bus(bus.getId(), bus.getFromStation(), bus.getToStation(), bus.getFromTime(), bus.getToTime(), bus.getNumber(), bus.getDuration(), bus.getNameReys(), bus.getDays(), bus.getStatus(), bus.getIdImg()));
//                    }
//                    adapterBus.notifyDataSetChanged();
//                    // Helper.getListViewSize(listView);
//                    pr.dismiss();
//                    return;
//                }
//                searchList = doc.getElementsByTagName("search");
//                for (int k = 0; k < searchList.getLength(); k++) {
//                    to = (Element) searchList.item(k);
//                    Log.d("TOcode: ", to.getElementsByTagName("code").item(0).getChildNodes().item(0).getNodeValue());
//                    Log.d("TOtitle: ", to.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
//                    stoTitle = to.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue();
//                    Log.d("FROMcode: ", to.getElementsByTagName("code").item(1).getChildNodes().item(0).getNodeValue());
//                    Log.d("FROMtitle: ", to.getElementsByTagName("title").item(1).getChildNodes().item(0).getNodeValue());
//                    sfromTitle = to.getElementsByTagName("title").item(1).getChildNodes().item(0).getNodeValue();
//                }
//                threadsList = doc.getElementsByTagName("threads");
//                for (int i = 0; i < threadsList.getLength(); i++) {
//                    threads = (Element) threadsList.item(i);
//                    Log.d("THREADS", threads.getElementsByTagName("days").item(0).getChildNodes().item(0).getNodeValue());
//                    bus.setDays(threads.getElementsByTagName("days").item(0).getChildNodes().item(0).getNodeValue());
//                    Log.d("THREADS", threads.getElementsByTagName("arrival").item(0).getChildNodes().item(0).getNodeValue());
//                    bus.setToTime(threads.getElementsByTagName("arrival").item(0).getChildNodes().item(0).getNodeValue());
//                    Log.d("THREADS", threads.getElementsByTagName("duration").item(0).getChildNodes().item(0).getNodeValue());
//                    bus.setDuration(Format.FormatDurationTotime(threads.getElementsByTagName("duration").item(0).getChildNodes().item(0).getNodeValue()));
//                    Log.d("THREADS", threads.getElementsByTagName("departure").item(0).getChildNodes().item(0).getNodeValue());
//                    bus.setFromTime(threads.getElementsByTagName("departure").item(0).getChildNodes().item(0).getNodeValue());
//                    Log.d("------------", "to------------");
//                    toList = threads.getElementsByTagName("to");
//                    for (iTo = 0; iTo < toList.getLength(); iTo++) {
//                        to = (Element) toList.item(iTo);
//                        Log.d("code: ", to.getElementsByTagName("code").item(0).getChildNodes().item(0).getNodeValue());
//                        Log.d("title: ", to.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
//                        bus.setToStation(to.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
//                    }
//                    Log.d("------------", "from------------");
//                    fromList = threads.getElementsByTagName("from");
//                    for (iFrom = 0; iFrom < fromList.getLength(); iFrom++) {
//                        from = (Element) fromList.item(iFrom);
//                        Log.d("code: ", from.getElementsByTagName("code").item(0).getChildNodes().item(0).getNodeValue());
//                        Log.d("title: ", from.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
//                        bus.setFromStation(from.getElementsByTagName("title").item(0).getChildNodes().item(0).getNodeValue());
//                    }
//                    NodeList threadList = threads.getElementsByTagName("thread");
//                    for (j = 0; j < threadList.getLength(); j++) {
//                        Element thread = (Element) threadList.item(j);
//                        Log.d("THREAD", thread.getElementsByTagName("uid").item(0).getTextContent());
//                        Log.d("THREAD", thread.getElementsByTagName("title").item(0).getTextContent());
//                        bus.setNameReys(thread.getElementsByTagName("title").item(0).getTextContent());
//                        Log.d("THREAD", thread.getElementsByTagName("number").item(0).getTextContent());
//                        bus.setNumber(thread.getElementsByTagName("number").item(0).getTextContent());
//                        Log.d("THREAD", thread.getElementsByTagName("transport_type").item(0).getTextContent());
//                    }
//                    bus.setId(i);
//                    bus.setIdImg(R.drawable.buses_green);
//                    bus.setStatus("в пути");
//                    dbAdapter.insertRaspTableV1(searchReysReverse, bus);
//                }
//                pr.dismiss();
//                Toast.makeText(ScrollingActivity.this, "готово", Toast.LENGTH_SHORT).show();
//            }
//        }
//    }

}
